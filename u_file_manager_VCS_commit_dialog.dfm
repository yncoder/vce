object Commit_Dialog_Form: TCommit_Dialog_Form
  Left = 0
  Top = 0
  Caption = 'Commit dialog'
  ClientHeight = 373
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 137
    Width = 592
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    ExplicitTop = 369
    ExplicitWidth = 1070
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 592
    Height = 137
    Align = alTop
    TabOrder = 0
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 590
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Commit comment'
      TabOrder = 0
    end
    object Commit_Msg_M: TMemo
      Left = 1
      Top = 26
      Width = 590
      Height = 110
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 1
      OnChange = Commit_Msg_MChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 140
    Width = 592
    Height = 212
    Align = alClient
    TabOrder = 1
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 590
      Height = 24
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Files to commit:'
      TabOrder = 0
    end
    object Files_LV: TListView
      Left = 1
      Top = 25
      Width = 590
      Height = 186
      Align = alClient
      Columns = <
        item
          AutoSize = True
          Caption = 'File'
        end
        item
          AutoSize = True
          Caption = 'State'
        end>
      ColumnClick = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      ViewStyle = vsReport
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 352
    Width = 592
    Height = 21
    Align = alBottom
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 85
    Caption = 'ToolBar1'
    ShowCaptions = True
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = OK_A
      AutoSize = True
    end
    object ToolButton3: TToolButton
      Left = 89
      Top = 0
      Width = 64
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsDivider
    end
    object ToolButton2: TToolButton
      Left = 153
      Top = 0
      Action = Cancel_A
      AutoSize = True
    end
  end
  object ActionList1: TActionList
    Left = 472
    Top = 88
    object OK_A: TAction
      Caption = 'Commit changes'
      OnExecute = OK_AExecute
    end
    object Cancel_A: TAction
      Caption = 'Cancel'
      OnExecute = Cancel_AExecute
    end
  end
end
