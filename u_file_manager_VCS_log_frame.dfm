object VCS_Log_Frame: TVCS_Log_Frame
  Left = 0
  Top = 0
  Width = 448
  Height = 331
  Constraints.MinHeight = 240
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 0
    Top = 213
    Width = 448
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 361
    ExplicitWidth = 83
  end
  object LV: TListView
    Left = 0
    Top = 0
    Width = 448
    Height = 213
    Align = alClient
    Columns = <
      item
        AutoSize = True
        Caption = 'n'
      end
      item
        AutoSize = True
        Caption = 'author'
      end
      item
        AutoSize = True
        Caption = 'date'
      end
      item
        AutoSize = True
        Caption = 'comment'
      end>
    ColumnClick = False
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnChange = LVChange
  end
  object Panel1: TPanel
    Left = 0
    Top = 216
    Width = 448
    Height = 115
    Align = alBottom
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 145
      Top = 1
      Height = 113
      Align = alRight
      ExplicitLeft = 304
      ExplicitTop = 16
      ExplicitHeight = 100
    end
    object Panel2: TPanel
      Left = 340
      Top = 1
      Width = 107
      Height = 113
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object SaveAs_B: TButton
        Left = 14
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Save as...'
        TabOrder = 0
        OnClick = SaveAs_BClick
      end
    end
    object File_List_LB: TListBox
      Left = 148
      Top = 1
      Width = 192
      Height = 113
      Align = alRight
      ItemHeight = 13
      TabOrder = 1
      OnClick = File_List_LBClick
      OnExit = File_List_LBExit
    end
    object Comment_M: TMemo
      Left = 1
      Top = 1
      Width = 144
      Height = 113
      Align = alClient
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
    end
  end
  object SaveDialog1: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 544
    Top = 424
  end
end
