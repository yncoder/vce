(* ������� ������ VCS - version control system. ������� *)
unit u_file_manager_VCS_Base;

interface

uses u_file_manager_VCS_const, U_API_Wrapper_Const,
     Classes;

(* ����� ��������� ������ ��� ������� �������? *)     
type T_Work_Folder_Env = ( twfe_Default,            // �� �����
                           twfe_WorkFolder,         // ������� �����
                           twfe_WorkFolder_Up       // ������� ����� - ���� ��� ����� (��� �������� svn checkout �������� )
                         );

(* ������������ �� TComponent ����� ������ ��� fXMLDocument *)
type T_VCS_Wrapper_Base = class( TComponent )
        protected
          fVCS_Project_State : T_VCS_Project_State;
          fProject_ID : integer;
          fAuth_Params : T_Auth_Params;

          fVCS_Busy_Notify : T_VCS_Busy_Notify;

          (* ��������� ��� ��� *)
          fVCS_Folder : string;
          (* ���� � �������, � ����������� *)
          fVCS_URL : string;

          (* ������ �� ������������� ������ ������  *)
          (* ������ ������ �� ������ *)
          fFolder_Files_Array : T_Files_Array;

          (* ������ ������ ����� *)
          fVCS_Folder_Length : integer;
          (* ������� ������������ ����� - ������ ���� *)
          fCurrent_Folder : string;
          (* ������� ������������ ����� - ���� ������������ ����� *)
          fCurrent_Related_Folder : string;

          (* ������ ��������� ���������� ������� � ������� � ������ ��������� ���������� (������������). ������ ������������������ � ����������� *)
          f_const_protocol_list, f_const_local_protocol_list : string;

          (* ������ �� ������ - ������ �������� *)
          fProjects_List : T_Projects_List;

          procedure Clear_Files;

         (* ������������� ������ �� �������� � �������� *)
         function StateName2State( Arg_State_Name : string ) : T_File_VCS_States; virtual; abstract;

         procedure Update_Related_Folder;

         (* �������� ������� �� ��������� ����� *)
         function Scan( Arg_Name : string; var Arg_Files_Array : T_Files_Array; Arg_Folder : boolean ) : boolean; virtual; abstract;
         (* ���������� ������� ��� ������� *)
         function Do_Checkout : boolean; virtual; abstract;
         procedure Clear_Params;
         
         function Check_VCS_URL( Arg_URL : string ) : boolean;

         (* ���������� � ������ ���������� � ������ �� ����, ����� �� ������ ���������������� ���� *)
         function Get_Commit_MSG_File_Name : string;
         procedure Save_Commit_Msg( Arg_MSG : string );
         function Load_Commit_Msg : string;

         procedure After_Create; virtual; abstract;
         function Do_Run_VCS_Tool( Arg : string; Arg_Auth_Need : boolean; Arg_Work_Folder_Env : T_Work_Folder_Env; out Out_Str : string; Arg_Log : boolean = False ) : boolean; virtual; abstract;
         function Run_VCS_Tool( Arg : string; Arg_Auth_Need : boolean; Arg_Work_Folder_Env : T_Work_Folder_Env; out Out_Str : string; Arg_Log : boolean = False ) : boolean;

         procedure Validate_Current_Folder;

         procedure Set_New_Auth_Params( Arg_Auth_Params : T_Auth_Params );
        public
          property Folder_Files_Array : T_Files_Array read fFolder_Files_Array;
          property Current_Folder : string read fCurrent_Folder;
          property Current_Related_Folder : string read fCurrent_Related_Folder;
          property VCS_Project_State : T_VCS_Project_State read fVCS_Project_State;
          property Stored_Commit_Msg : string read Load_Commit_Msg write Save_Commit_Msg;
          property VCS_Folder : string read fVCS_Folder;
          property VCS_URL: string read fVCS_URL;
          property Auth_Params : T_Auth_Params read fAuth_Params write Set_New_Auth_Params;

          (* ���������������� ������
             Arg_Server_Url - ��� �� ������
             Arg_Root - ���� ������ ������. ���� ���� - �� ������� �� ������, ������ ������ � ������ ��������� �������.
          *)
          constructor Create(AOwner: TComponent; Arg_VCS_Busy_Notify : T_VCS_Busy_Notify); reintroduce;

          function Get_Path_Info( Arg_Path : string; Arg_Remote : boolean ; out Out_Repo_Info : T_Repo_Info ) : T_Connect_Results; virtual; abstract;
          function Test_Server_Url( Arg_Server_Url : string; out Out_Repo_Info : T_Repo_Info ) : T_Connect_Results;

          function Check_Working_Copy_Outdated( out out_Server_Rev, out_Working_Copy_Rev : t_revision_id_type ) : T_Connect_Results; virtual; abstract;

          function Set_Params( Arg_Projects_List : T_Projects_List; Arg_Server_Url : string; Arg_ID : integer; Arg_Auth_Params : T_Auth_Params; Arg_Current_Folder : string = ''; Arg_Root : string = '' ) : boolean;

          //procedure Before_Destroy; virtual; abstract;

          function Get_File_FullPath( Arg_File_Idx : integer; Arg_Canonical : boolean = False ): string; virtual;

          function Add_File( Arg_File_IDX : integer ) : boolean; virtual; abstract;

          function Commit : boolean; virtual; abstract;

          (* ������ ���������������� ������, ������� ������� � ������.
             �� ����� � ��������� �����, ���� ���� ���������, �������� *)
          function Get_FileList_BeforeCommit( var out_File_List : T_Commited_Files_Array ) : T_Get_FileList_BeforeCommit_results; virtual; abstract;

          function Get_VCS_History( out Out_Commits_Log_A : T_Commits_Log_A; Arg_Path : string ) : boolean; virtual; abstract;

          function Revert( Arg_File_IDX : integer ) : boolean; virtual; abstract;

          function Resolve( Arg_File_IDX : integer ) : boolean; virtual; abstract;

          (* �������� ����� diff ��� ���������� ����� - ������� �� ������� ������ *)
          function Get_Diff_Text( Arg_File_IDX : integer ) : string; virtual; abstract;

          (* ����������� ������� ����� *)
          procedure Scan_Folder;

          (* ��������� ��������� ������ ����� *)
          function Check_File_State( Arg_File : string; out out_conflicted_file_rev : t_revision_id_type ) : T_File_VCS_States; virtual; abstract;

          (* ���� ���� ��� � ������� - �������� ��� ���� ��� �������� diff *)
          function Get_Pristine_File_Path( Arg_File_IDX : integer ) : string; virtual; abstract;

          function Update : T_Update_Results; virtual; abstract;

          (* ������� ����� �� ������� � ��������� ����� *)
          function Export_File_Revisioned( Arg_Local_Path, Arg_Dest_Path : string; Arg_Rev : t_revision_id_type ) : boolean; virtual; abstract;

          (* ������� �� ����� ������, �������� ������ *)
          function Drill2Folder( Arg_File_IDX : integer ) : boolean;

          (* ������� �� ����� �����, �������� ������ *)
          function Up2Folder : boolean;

          (* ���������, �������� �� ������� ������? *)
          function Up2Folder_avail : boolean;

          destructor Done; virtual;
      end;

implementation

{$WARN UNIT_PLATFORM OFF}

uses FileCTRL, Sysutils,
     U_Const, U_Msg, u_web_utils;

{ T_VCS_Wrapper_Base }

function T_VCS_Wrapper_Base.Check_VCS_URL;
begin
  Result := Check_URL( Arg_URL, f_const_protocol_list );
end;

procedure T_VCS_Wrapper_Base.Clear_Files;
begin
  SetLength( fFolder_Files_Array, 0 );
end;

procedure T_VCS_Wrapper_Base.Clear_Params;
begin
  fProjects_List := nil;
  
  fVCS_Project_State := ps_None;

  Clear_Files;

  fVCS_URL    := '';

  fVCS_Folder := '';

  fCurrent_Folder := '';
  fCurrent_Related_Folder := '';

  fVCS_Folder_Length := 0;

  fProject_ID := 0;

  fAuth_Params.f_username := '';
  fAuth_Params.f_password := '';
end;

function T_VCS_Wrapper_Base.Get_Commit_MSG_File_Name;
begin
  Result := AppData_Path + 'commit_msg_' + IntToStr( fProject_ID ) + '.msg';  
end;

constructor T_VCS_Wrapper_Base.Create;
begin
  inherited Create( AOwner );
  
  fVCS_Busy_Notify := Arg_VCS_Busy_Notify;
  f_const_protocol_list := '';

  After_Create;

  Clear_Params;
end;

destructor T_VCS_Wrapper_Base.Done;
begin
  Clear_Files;
end;

function T_VCS_Wrapper_Base.Drill2Folder;
var Dest_Folder : string;
begin
  Result := False;

  if not fFolder_Files_Array[ Arg_File_IDX ].f_folder_flag
    then
      begin
        Exit;
      end;

  Dest_Folder := fCurrent_Folder + Slash + fFolder_Files_Array[ Arg_File_IDX ].f_file_name;

  if DirectoryExists( Dest_Folder )
    then
      begin
        fCurrent_Folder := Dest_Folder;
        Update_Related_Folder;
        Scan_Folder;
        Result := True;
      end;
end;

function T_VCS_Wrapper_Base.Get_File_FullPath;
begin
  Result := fCurrent_Folder + Slash + Folder_Files_Array[ Arg_File_Idx ].f_file_name;
end;

function T_VCS_Wrapper_Base.Load_Commit_Msg;
var fn, s : string;
    f : TextFile;
begin
  Result := '';
  fn := Get_Commit_MSG_File_Name;

  if FileExists( fn )
    then
      begin
        AssignFile( f, fn );

        Reset( f );

        while not EOF(f) do
          begin
            Readln( f, s );
            if Result <> '' then Result := Result + ret;
            
            Result := Result + s;
          end;        

        CloseFile( f );
      end
    else
      Result := '';
end;

function T_VCS_Wrapper_Base.Run_VCS_Tool;
begin
  if Assigned( fVCS_Busy_Notify )
    then
      fVCS_Busy_Notify( True );

  {$i test.inc}
  {$ifdef testing_msg}
  Arg_Log := True;
  {$endif}
  Result := Do_Run_VCS_Tool( Arg, Arg_Auth_Need, Arg_Work_Folder_Env, Out_Str, Arg_Log );

  if Assigned( fVCS_Busy_Notify )
    then
      fVCS_Busy_Notify( False );
end;

procedure T_VCS_Wrapper_Base.Save_Commit_Msg;
var f : textfile;
    fn : string;
begin
  fn := Get_Commit_MSG_File_Name;
  
  if Arg_MSG <> ''
    then
      begin
        AssignFile( f, fn );
        Rewrite( f );

        Write( f, Arg_MSG );

        CloseFile( f );
      end
    else
      begin
        (* ��������� ������ *)
        if FileExists( fn )
          then
            DeleteFile( fn );
      end;
end;

procedure T_VCS_Wrapper_Base.Scan_Folder;
begin
  Clear_Files;
  Scan( fCurrent_Folder, fFolder_Files_Array, True );
end;

procedure T_VCS_Wrapper_Base.Set_New_Auth_Params;
begin
  fAuth_Params := Arg_Auth_Params;
  if Update <> tur_Fail
    then
      (* ��������� ��������� *)
      fProjects_List.Save_Credentials( fProject_ID, fAuth_Params.f_username, fAuth_Params.f_password );
end;

function T_VCS_Wrapper_Base.Set_Params;
begin
  Result := True;
  
  Clear_Params;
  fProjects_List := Arg_Projects_List;
  fVCS_URL    := Arg_Server_Url;

  fAuth_Params := Arg_Auth_Params;
  if Arg_Root = ''
    then
      begin
        (* �� ������ ������� *)
        fVCS_Project_State := ps_Browse;

        fVCS_Folder := '';

        fCurrent_Folder := '';
        fCurrent_Related_Folder := '';

        fVCS_Folder_Length := 0;
      end
    else
      begin
        fVCS_Project_State := ps_Checkout;
        fProject_ID := Arg_ID;

        fVCS_Folder := Arg_Root;
        (* ���� ���� �������� �������������, �� ���� ��������� ��� � ���������� *)
        if pos( ':\', fVCS_Folder ) = 0
          then
            begin
              (* ��� ������������� ���� *)
              if ( fVCS_Folder[1] = Slash )
                then
                  Delete( fVCS_Folder, 1, 1 );

              fVCS_Folder := AppData_Path + fVCS_Folder;

              {$i test.inc}
              {$ifdef testing_msg}
              ShowMsg( fVCS_Folder );
              {$endif}
            end;

        if Arg_Current_Folder = ''
          then
            begin
              fCurrent_Folder := fVCS_Folder;
              fCurrent_Related_Folder := '';
            end
          else
            begin
              fCurrent_Folder := fVCS_Folder + Slash + Arg_Current_Folder;
              fCurrent_Related_Folder := Arg_Current_Folder;
            end;

        fVCS_Folder_Length := Length( fVCS_Folder );

        (* ������ ���������� ������� *)
        Result := Do_Checkout;

        if Arg_Current_Folder <> ''
          then
            Validate_Current_Folder;
      end;
end;

function T_VCS_Wrapper_Base.Test_Server_Url;
var Out_Str : string;
    //Out_Repo_Info : T_Repo_Info;
begin
  Result := Get_Path_Info( Arg_Server_Url, True, Out_Repo_Info );
end;

function T_VCS_Wrapper_Base.Up2Folder;
var i : integer;
    s : string;

begin
  Result := False;

  if fCurrent_Related_Folder = ''
    then
      Exit;

  s := fCurrent_Folder;

  (* ���������, ���� �� ������ ���� - ����� �������� ��� ����, �� ����� *)
  repeat
    i := LastDelimiter( '\', s );
    if i > 0
      then
        begin
          s := Copy( s, 1, i - 1 );
          if ( DirectoryExists( s ) )
              or
             ( s = fVCS_Folder )
            then
              begin
                fCurrent_Folder := s;
                Update_Related_Folder;
                Scan_Folder;
                Result := True;
              end;
        end;
  until Result;
end;

function T_VCS_Wrapper_Base.Up2Folder_avail: boolean;
begin
  Result := fCurrent_Related_Folder <> '';
end;

procedure T_VCS_Wrapper_Base.Update_Related_Folder;
begin
  fCurrent_Related_Folder := Copy( fCurrent_Folder, fVCS_Folder_Length + 2, length( fCurrent_Folder ) - fVCS_Folder_Length );
end;

procedure T_VCS_Wrapper_Base.Validate_Current_Folder;
begin
  if ( not( DirectoryExists( fCurrent_Folder ) ) )
    then
      Up2Folder;
end;

end.
