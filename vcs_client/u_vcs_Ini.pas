(* Ini- ����. ���������� �������� *)
unit u_vcs_Ini;

interface

uses u_vcs_diff_options_const, IniFiles, Classes, Comctrls, Forms, Graphics;

const Monitor_Interval_Minimum  = 5;
      Monitor_Interval_Default_as_string = '20';
      Monitor_Interval_Maximum = 24 * 60;

(* ������ *)
type TINI_Sections =
     ( is_Test,
       is_Options_Section,
       is_Open_Projects,
       is_Main_Font,
       is_GitHub_Accounts
     );


(* ������ *)
type TINI_Idents   = (
       ii_Test,
       ii_Work_Checkouts_Root,
       ii_Work_Checkouts_ExeData_Root,
       ii_HTTP_Proxy_Host,
       ii_HTTP_Proxy_Port,
       ii_Monitor_Interval,
       ii_External_Diff_Exe,
       ii_External_Diff3_Flag,
       ii_DoNot_Show_StartUp,
       ii_Temp_Folder_Flag,
       ii_Default_Commit_MSG,
       ii_ToolBar_Type,
       ii_Catalog_URL,

       ii_Main_Font_Name,
       ii_Main_Font_Size
 );

const Project_Section_Name = 'Project_rec_';
      Project_Path_ident = 'Current_Folder';
      External_Diff_sec = 'External_Diff';

(* ��� ������� � ������ *)
type T_Toolbar_type = ( tbt_BigImage_Text, tbt_BigImage, tbt_SmallImage_Text, tbt_SmallImage, tbt_Text_Only );

type TIni_Strore = class
       private
         Ini : TIniFile;

         fWork_Checkouts_Root, fHTTP_Proxy_Host : string;

         fHTTP_Proxy_Port : integer;

         fMonitor_Interval : integer;

         fTemp_Folder_Flag : boolean;

         fFont : TFont;

         function Get_Int( INI_Idents   : TINI_Idents
                         ) : integer;
         procedure Set_Int( INI_Idents   : TINI_Idents;
                            Value : integer );

         function Get_Boo( INI_Idents   : TINI_Idents
                         ) : boolean;
         procedure Set_Boo( INI_Idents   : TINI_Idents;
                            Value : boolean );

         function Get_Str( INI_Idents   : TINI_Idents
                         ) : string;
         procedure Set_Str( INI_Idents   : TINI_Idents;
                            Value : string );

         function Get_Work_Checkouts_Root : string;
         procedure Set_Work_Checkouts_Root( Arg_Value : string );

         function Get_HTTP_Proxy_Host : string;
         procedure Set_HTTP_Proxy_Host( Arg_Value : string );

         function Get_HTTP_Proxy_Port : integer;
         procedure Set_HTTP_Proxy_Port( Arg_Value : integer );

         function Get_Monitor_Interval : integer;
         procedure Set_Monitor_Interval( Arg_Value : integer );

         function Get_Temp_Folder_Flag : boolean;
         procedure Set_Temp_Folder_Flag( Arg_Value : boolean );
       public
         f_Diff_Action_arr : T_Diff_Action_arr;

         property Int[ INI_Idents : TINI_Idents ]  : integer read Get_Int write Set_Int;

         property Boo[ INI_Idents : TINI_Idents ]  : boolean read Get_Boo write Set_Boo;

         property Str[ INI_Idents : TINI_Idents ]  : string  read Get_Str write Set_Str;

         property Work_Checkouts_Root : string read Get_Work_Checkouts_Root write Set_Work_Checkouts_Root;

         property HTTP_Proxy_Host : string read Get_HTTP_Proxy_Host write Set_HTTP_Proxy_Host;
         property HTTP_Proxy_Port : integer read Get_HTTP_Proxy_Port write Set_HTTP_Proxy_Port;

         property Monitor_Interval : integer read Get_Monitor_Interval write Set_Monitor_Interval;
         property Temp_Folder_Flag : boolean read Get_Temp_Folder_Flag write Set_Temp_Folder_Flag;
         (* ������-������ �������� ����� �� ini ����� *)
         (* ������ �������� ����� � ini ���� *)
         procedure Save_Form_Sizes( Arg_Form : TForm; Arg_Project_Form : boolean = False; Arg_Current_Path : string = '' );

         (* ������ �������� ����� �� ini ����� *)
         procedure Load_Form_Sizes( Arg_Form : TForm );

         (* �������� �������� �� ��������� *)
         function Get_Def( INI_Idents : TINI_Idents ) : string;

         (* �������� �������� ���� � ini *)
         function Get_Ini_Path : string;

         (* ������� ������ �� ����� ����������� *)
         procedure Del_Section( Arg_INI_Sections : TINI_Sections );

         (* ������� ������ �� ����� ������������� *)
         procedure Del_Section_By_Name( Arg_INI_Section_Name : string );

         function Get_Ini_File : TIniFile;

         function Get_ToolBar_Type : T_Toolbar_type;

         (* ���������� ����� *)
         procedure Set_Font( Arg_Font : TFont );

         procedure Save_Font( Arg_Font : TFont ); (* ��������� ����� � ������ *)
         function Check_Font_Modified( Arg_Font : TFont ) : boolean; (* ���������, ��������� �� ����� � �������� *)
         procedure Load_Font; (* ��������� ����� �� ������� *)

         procedure Save_Project_Opened( Arg_Project_ID : integer );
         procedure Save_Project_Path( Arg_Project_ID : integer; Arg_Current_Folder : string );
         function Is_Project_Opened( Arg_Project_ID : integer ) : boolean;
         function Get_Project_Saved_Current_Folder( Arg_Project_Form_Name : string ) : string;
         procedure Reset_Values;
         procedure Load_Interface_Images;
         procedure Done;

         procedure Diff_Action_arr_Free;
         procedure Diff_Action_arr_Load;
         procedure Diff_Action_arr_Save;
         procedure Diff_Action_arr_Del_And_Save( Arg_IDX : integer );
         function Get_Known_Diff_Tools : string; (* ���������� �� f_Diff_Action_arr *)
         function Get_Known_Diff_Ext : string; (* ������ �� f_Diff_Action_arr *)

         procedure Save_GitHub_Accounts( Arg_Account_SL : TStrings );
         procedure Load_GitHub_Accounts( Arg_Account_SL : TStrings );

         constructor Create;
         destructor Destroy; override;
      end;

var Ini_Strore : TIni_Strore;

(* ������� ���� �������� *)
procedure Ini_Init;

(* ������� ���� �������� *)
procedure Ini_Done;

(* �������� �������� ������ �� ���� *)
function Get_Section_Name( Arg_INI_Sections : TINI_Sections ) : string;

(* �������� �������� �������� �� ���� *)
function Get_Idents_Name( Arg_INI_Idents : TINI_Idents ) : string;

function Calculate_Work_checkouts_Root : string;

(* ��������� INI ���� �� ������ �� ������ *)
function Test_WriteAccess_INI( Arg_INI_FN : string ) : boolean;

implementation

uses Sysutils, StrUtils, U_Const, U_ShellOp, U_Msg, u_vcs_portable_const, u_vcs_ico_const;

(* ������ *)
const INI_Sections_Name : array[TINI_Sections] of string =
        ( 'Test',
          'Options',
          'Open_Projects',
          'Main_Font',
          'GitHub_Accounts'
        );

(* ������ *)

const INI_Idents_Name : array[ TINI_Idents ] of
        record
          INI_Sections : TINI_Sections;
          Idents_Name  : string;
          Def          : string
        end
      =
      (
        ( INI_Sections : is_Test;
          Idents_Name  : 'Test';
          Def          : '' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Work_Checkouts_Root';
          Def          : '' ),
        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Work_Checkouts_ExeData_Root';
          Def          : '' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'HTTP_Proxy_Host';
          Def          : '' ),
        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'HTTP_Proxy_Port';
          Def          : '0' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Monitor_Interval';
          Def          : Monitor_Interval_Default_as_string ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'External_Diff_Exe';
          Def          : '' ),
        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'External_Diff3_Flag';
          Def          : '' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'DoNot_Show_StartUp';
          Def          : '' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Temp_Folder';
          Def          : '1' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Default_Commit_MSG';
          Def          : '' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'ToolBar_Type';
          Def          : '0' ),

        ( INI_Sections : is_Options_Section;
          Idents_Name  : 'Catalog_URL';
          Def          : '' ),

        ( INI_Sections : is_Main_Font;
          Idents_Name  : 'Font_Name';
          Def          : 'Tahoma' ),
        ( INI_Sections : is_Main_Font;
          Idents_Name  : 'Font_Size';
          Def          : '8' )
      );

(* ��������������� ��� ������ �� ini *)
function DeEscape_For_Ini( Arg_S : string ) : string;
begin
  Result := ReplaceText( Arg_S, '|', '=' );
  Result := ReplaceText( Arg_S, '^', '[' );
end;

(* ������������ ��� ���������� � ini *)
function Escape_For_Ini( Arg_S : string ) : string;
begin
  Result := ReplaceText( Arg_S, '=', '|' );
  Result := ReplaceText( Arg_S, '[', '^' );
end;

(* ������� ���� �������� *)
procedure Ini_Init;
begin
  Ini_Strore := TIni_Strore.Create;

  Refresh_Temp_Path( True );
end;

(* ������� ���� �������� *)
procedure Ini_Done;
begin
  Ini_Strore.Free;
end;

function TIni_Strore.Check_Font_Modified;
begin
  result := ( Arg_Font.Name <> fFont.Name ) or ( Arg_Font.Size <> fFont.Size );
end;

constructor TIni_Strore.Create;
begin
  Ini := TIniFile.Create( Ini_File_Path );
  
  fFont := nil;

  Reset_Values;
  Load_Interface_Images;

  Diff_Action_arr_Load;
end;

procedure TIni_Strore.Del_Section;
begin
  Ini.EraseSection( INI_Sections_Name[ Arg_INI_Sections ] );
end;

procedure TIni_Strore.Del_Section_By_Name;
begin
  Ini.EraseSection( Arg_INI_Section_Name );
end;

destructor TIni_Strore.Destroy;
begin
  if Assigned( fFont )
    then
      fFont.Free;
      
  Ini.Free;
end;

procedure TIni_Strore.Diff_Action_arr_Del_And_Save;
var i : integer;
    l : integer;
begin
  l := length( f_Diff_Action_arr );

  if l < 1
    then
      Exit; (* �������� ��� �������� ������������ *)

  Ini.DeleteKey( External_Diff_sec, Escape_For_Ini( f_Diff_Action_arr[ Arg_IDX ].f_extention ) );

  for i := Arg_IDX to l - 2 do
    begin
      f_Diff_Action_arr[ i ] := f_Diff_Action_arr[ i + 1 ];
    end;
  SetLength( f_Diff_Action_arr, l - 1 );
end;

procedure TIni_Strore.Diff_Action_arr_Free;
begin
  SetLength( f_Diff_Action_arr, 0 );
end;

procedure TIni_Strore.Diff_Action_arr_Load;
var i : integer;
    sl : tstringlist;
    ext : string;
begin
  Diff_Action_arr_Free;

  (* �������� *)
  sl := tstringlist.Create;
  Ini.ReadSection( External_Diff_sec, SL );

  SetLength( f_Diff_Action_arr, SL.Count );
  for I := 0 to sl.Count - 1 do
    begin
      ext := Sl[ i ];
      f_Diff_Action_arr[i].f_extention := DeEscape_For_Ini( ext );
      f_Diff_Action_arr[i].f_external_tool_path := DeEscape_For_Ini( Ini.ReadString( External_Diff_sec, ext, '' ) );
    end;

  SL.Free;
end;

procedure TIni_Strore.Diff_Action_arr_Save;
var i : integer;
begin
  Del_Section_By_Name( External_Diff_sec );

  for i := 0 to high( f_Diff_Action_arr ) do
    Ini.WriteString( External_Diff_sec, Escape_For_Ini( f_Diff_Action_arr[i].f_extention ), Escape_For_Ini( f_Diff_Action_arr[i].f_external_tool_path ) );
end;

procedure TIni_Strore.Done;
begin
  // �� �����������, �.�. �������� ������� ����������.
  // � ���� �� ����� ������ �������� ����������� �� �������� ����������, � �������� 
  // Interface_Image_List.Free;
end;

function TIni_Strore.Get_Boo;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Ini.ReadBool( INI_Sections_Name[INI_Sections], Idents_Name, ( Def = '1' ) );
    end;
end;

function TIni_Strore.Get_Ini_File: TIniFile;
begin
  Result := Ini;
end;

function TIni_Strore.Get_Ini_Path: string;
begin
  Result := '';
  if Assigned( Ini )
    then
      Result := Ini.FileName;
end;

function TIni_Strore.Get_Int;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Ini.ReadInteger( INI_Sections_Name[INI_Sections], Idents_Name, StrToInt( Def ) );
    end;
end;

function TIni_Strore.Get_Known_Diff_Ext: string;
var i : integer;
begin
  Result := '';

  for i := 0 to high( f_Diff_Action_arr ) do
    Result := Result + Ext_List_Delimiter + f_Diff_Action_arr[i].f_extention + Ext_List_Delimiter;
end;

function TIni_Strore.Get_Known_Diff_Tools;
var i : integer;
begin
  Result := '';

  for i := 0 to high( f_Diff_Action_arr ) do
    if pos( f_Diff_Action_arr[i].f_external_tool_path, Result ) = 0
      then
        begin
          if Result <> '' then Result := Result + ret;
          Result := Result + f_Diff_Action_arr[i].f_external_tool_path;
        end;
end;

function TIni_Strore.Get_Monitor_Interval: integer;
begin
  if fMonitor_Interval <> Monitor_Interval_Minimum
    then
      Result := fMonitor_Interval
    else
      begin
        Result := Int[ ii_Monitor_Interval ];
        fMonitor_Interval := Result;
      end;
end;

function TIni_Strore.Get_Str;
var s : string;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      s := Ini.ReadString( INI_Sections_Name[INI_Sections], Idents_Name, Def );

      Result := s;
    end;
end;

function TIni_Strore.Get_Temp_Folder_Flag;
begin
  {if fTemp_Folder_Flag
    then
      Result := fTemp_Folder_Flag
    else   }
      begin
        {$message warn '����������� ������ ini �� ����������'}

        Result := Boo[ ii_Temp_Folder_Flag ];
        fTemp_Folder_Flag := Result;
      end;
end;

function TIni_Strore.Get_ToolBar_Type;
var i : integer;
begin
  (* ��� ������ � ������ ������� *)
  i := Ini_Strore.Int[ ii_ToolBar_Type ];
  if ( i < integer( low ( T_Toolbar_type ) ) )
     and
     ( i > integer( high ( T_Toolbar_type ) ) )
    then
      Result := tbt_BigImage_Text
    else
      Result := T_Toolbar_type( i );
end;

function TIni_Strore.Get_Work_Checkouts_Root;
begin
  if fWork_Checkouts_Root <> ''
    then
      Result := fWork_Checkouts_Root
    else
      begin
        Result := Str[ ii_Work_Checkouts_Root ];
        fWork_Checkouts_Root := Result;
      end;
end;

function TIni_Strore.Is_Project_Opened;
begin
  Result := Ini.ValueExists( INI_Sections_Name[ is_Open_Projects ], IntToStr( Arg_Project_ID ) );
end;

function TIni_Strore.Get_Project_Saved_Current_Folder;
begin
  Result := Ini.ReadString( Arg_Project_Form_Name, Project_Path_ident, '' );
end;

procedure TIni_Strore.Set_Str;
var s : string;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      s := Value;

      Ini.WriteString( INI_Sections_Name[INI_Sections], Idents_Name, s );
    end;
end;

procedure TIni_Strore.Set_Temp_Folder_Flag;
begin
  if Boo[ ii_Temp_Folder_Flag ] <> Arg_Value
    then
      begin
        Boo[ ii_Temp_Folder_Flag ] := Arg_Value;
        fTemp_Folder_Flag := Arg_Value;
        Refresh_Temp_Path( True );
      end;
end;

procedure TIni_Strore.Set_Work_Checkouts_Root;
begin
  Str[ ii_Work_Checkouts_Root ] := Arg_Value;
  fWork_Checkouts_Root := Arg_Value;  
end;

procedure TIni_Strore.Set_Font;
begin
  Load_Font;
  Arg_Font.Name := fFont.Name;
  Arg_Font.Size := fFont.Size;
end;

procedure TIni_Strore.Save_GitHub_Accounts;
var s : string;
begin
  Ini.EraseSection( INI_Sections_Name[ is_GitHub_Accounts ] );
  for s in Arg_Account_SL do
    Ini.WriteString( INI_Sections_Name[ is_GitHub_Accounts ], s, '' );
end;

procedure TIni_Strore.Load_Font;
begin
  if not Assigned( fFont )
    then
      begin
        fFont := TFont.Create;
        fFont.Name := Str[ ii_Main_Font_Name ];
        fFont.Size := int[ ii_Main_Font_Size ];
      end;
end;

procedure TIni_Strore.Load_Form_Sizes;
var Max : boolean;
    Section : string;
    Top : integer;
begin
  Section := Arg_Form.Name;
  Max := ( Arg_Form.WindowState = wsMaximized );

  (* ��������� �� ��������� �������� ������� ��������. ����� ����������! *)
  with Ini do
    begin
      if not SectionExists( Section )
        then
          Exit;
          
      Max := ReadBool( Section, 'Maximized', Max );

      (* ���� ������� "���������� �����", �� ������� ����� �� ���������� *)
      if Max
        then
          Arg_Form.WindowState := wsMaximized
        else
          begin
            Top := ReadInteger( Section, 'Top', 0 );
            if Top = 0
              then
                begin
                  Arg_Form.width  := Arg_Form.Constraints.MinWidth;
                  Arg_Form.height := Arg_Form.Constraints.MinHeight;
                end
              else
                begin
                  Arg_Form.top := ReadInteger( Section, 'Top', Arg_Form.Top);
                  Arg_Form.left := ReadInteger( Section, 'Left', Arg_Form.Left);

                  if ( Arg_Form.BorderStyle = bsSizeable )
                    then
                      begin
                        Arg_Form.width  := ReadInteger( Section, 'Width', Arg_Form.Constraints.MinWidth );
                        Arg_Form.height := ReadInteger( Section, 'Height', Arg_Form.Constraints.MinHeight );
                      end;
                end;
          end;
    end;
end;

procedure TIni_Strore.Load_Interface_Images;
begin
  Load_Icon_Res;
end;

procedure TIni_Strore.Reset_Values;
begin
  fWork_Checkouts_Root := '';
  fHTTP_Proxy_Host := '';
  fHTTP_Proxy_Port := 0;
  fMonitor_Interval := Monitor_Interval_Minimum;
  fTemp_Folder_Flag := True;
end;

procedure TIni_Strore.Save_Font;
begin
  Str[ ii_Main_Font_Name ] := Arg_Font.Name;
  Int[ ii_Main_Font_Size ] := Arg_Font.Size;
  fFont.Name := Arg_Font.Name;
  fFont.Size := Arg_Font.Size;
end;

procedure TIni_Strore.Save_Form_Sizes;
var Max : boolean;
    Section : string;
begin
  try
    //ShowMsg( Arg_Form.Name );
    Section := Arg_Form.Name;
    Max := ( Arg_Form.WindowState = wsMaximized );

    with Ini do
      begin
        WriteInteger( Section, 'Top', Arg_Form.Top );
        WriteInteger( Section, 'Left', Arg_Form.Left );
        WriteInteger( Section, 'Width', Arg_Form.Width );
        WriteInteger( Section, 'Height', Arg_Form.Height );
        WriteBool( Section, 'Maximized', Max );

        if Arg_Project_Form
          then
            WriteString( Section, Project_Path_ident, Arg_Current_Path );
      end;
  except
    Show_Err( cap( cap_Cant_Write_On_Disk ) );
  end;
end;

procedure TIni_Strore.Save_Project_Opened;
begin
  Ini.WriteString( INI_Sections_Name[ is_Open_Projects ], IntToStr( Arg_Project_ID ), '' );
end;

procedure TIni_Strore.Save_Project_Path;
begin
  Ini.WriteString( INI_Sections_Name[ is_Open_Projects ], IntToStr( Arg_Project_ID ), Arg_Current_Folder );
end;

procedure TIni_Strore.Set_Boo;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Ini.WriteBool( INI_Sections_Name[INI_Sections], Idents_Name, Value );
    end;
end;

procedure TIni_Strore.Set_HTTP_Proxy_Host;
begin
  Str[ ii_HTTP_Proxy_Host ] := Arg_Value;
  fHTTP_Proxy_Host := Arg_Value;
end;

procedure TIni_Strore.Set_HTTP_Proxy_Port;
begin
  Int[ ii_HTTP_Proxy_Port ] := Arg_Value;
  fHTTP_Proxy_Port := Arg_Value;
end;

procedure TIni_Strore.Set_Int;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Ini.WriteInteger( INI_Sections_Name[INI_Sections], Idents_Name, Value );
    end;
end;

procedure TIni_Strore.Set_Monitor_Interval(Arg_Value: integer);
begin
  Int[ ii_Monitor_Interval ] := Arg_Value;
  fMonitor_Interval := Arg_Value;
end;

function TIni_Strore.Get_Def;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Def;
    end;
end;

procedure TIni_Strore.Load_GitHub_Accounts;
begin
  Ini.ReadSection( INI_Sections_Name[ is_GitHub_Accounts ], Arg_Account_SL );
end;

function TIni_Strore.Get_HTTP_Proxy_Host;
begin
  if fHTTP_Proxy_Host <> ''
    then
      Result := fHTTP_Proxy_Host
    else
      begin
        Result := Str[ ii_HTTP_Proxy_Host ];
        fHTTP_Proxy_Host := Result;
      end;
end;

function TIni_Strore.Get_HTTP_Proxy_Port;
begin
  if fHTTP_Proxy_Port <> 0
    then
      Result := fHTTP_Proxy_Port
    else
      begin
        Result := Int[ ii_HTTP_Proxy_Port ];
        fHTTP_Proxy_Port := Result;
      end;
end;

(* �������� �������� ������ �� ���� *)
function Get_Section_Name;
begin
  Result := INI_Sections_Name[ Arg_INI_Sections ];
end;

(* �������� �������� �������� �� ���� *)
function Get_Idents_Name;
begin
  Result := INI_Idents_Name[ Arg_INI_Idents ].Idents_Name;
end;

function Calculate_Work_checkouts_Root;
begin
  if Ini_Strore.Boo[ ii_Work_Checkouts_ExeData_Root ]
    then
      Result := AppData_Path
    else
      begin
        Result := Ini_Strore.Work_Checkouts_Root;
        if Result = ''
          then
            Result := Document_Folder;
      end;
end;

function Test_WriteAccess_INI;
var test_ini : TIniFile;
    i : integer;
begin
  Result := False;
  test_ini := TIniFile.Create( Arg_INI_FN );

  i := Random( MaxInt - 1 );
  try
    test_ini.WriteInteger( INI_Sections_Name[ INI_Idents_Name[ ii_Test ].INI_Sections ], INI_Idents_Name[ ii_Test ].Idents_Name, i );
    Result := True;
  except
    Show_Err( format( cap( cap_Cant_Write_To_Portable_INI ), [ Arg_INI_FN ] ) );
  end;

  test_ini.Free;
end;

end.
