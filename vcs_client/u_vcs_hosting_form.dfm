object VCS_Hosting_Form: TVCS_Hosting_Form
  Left = 0
  Top = 0
  Caption = 'VCS_Hosting_Form'
  ClientHeight = 352
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Repo_ListView: TListView
    Left = 0
    Top = 41
    Width = 613
    Height = 311
    Align = alClient
    Columns = <
      item
        AutoSize = True
        Caption = 'Name'
      end
      item
        AutoSize = True
        Caption = 'URL'
      end
      item
        AutoSize = True
        Caption = 'Fork'
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = Repo_ListViewClick
    OnDblClick = Repo_ListViewDblClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 613
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 85
      Height = 13
      Caption = 'GitHub user name'
    end
    object GH_User_CB: TComboBox
      Left = 112
      Top = 11
      Width = 225
      Height = 21
      ItemHeight = 0
      TabOrder = 0
      OnChange = GH_User_CBChange
    end
    object Fetch_B: TButton
      Left = 352
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Fetch'
      TabOrder = 1
      OnClick = Fetch_BClick
    end
    object Ok_B: TButton
      Left = 448
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Ok'
      TabOrder = 2
      OnClick = Ok_BClick
    end
    object Cancel_B: TButton
      Left = 529
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 3
    end
  end
end
