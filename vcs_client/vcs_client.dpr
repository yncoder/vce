program vcs_client;

uses
  Forms,
  SysUtils,
  Windows,
  vcs_client_main in 'vcs_client_main.pas' {VCS_Client_Project_List_F},
  u_file_manager_frame in '..\u_file_manager_frame.pas' {File_Manager_Frame: TFrame},
  U_Msg in '..\U_Msg.pas',
  U_Const in '..\U_Const.pas',
  u_file_manager_VCS in '..\u_file_manager_VCS.pas',
  u_file_manager_VCS_Base in '..\u_file_manager_VCS_Base.pas',
  U_ShellOp in '..\U_ShellOp.pas',
  u_file_manager_VCS_const in '..\u_file_manager_VCS_const.pas',
  U_ProjectView in '..\U_ProjectView.pas' {Project_Form},
  U_API_Wrapper_Const in '..\U_API_Wrapper_Const.pas',
  u_file_manager_VCS_log in '..\u_file_manager_VCS_log.pas' {VCS_Log_F},
  u_file_manager_VCS_commit_dialog in '..\u_file_manager_VCS_commit_dialog.pas' {Commit_Dialog_Form},
  u_vcs_Ini in 'u_vcs_Ini.pas',
  vcs_options_form in 'vcs_options_form.pas' {Options_F},
  u_common_forms in '..\u_common_forms.pas',
  vcs_resolver_form in 'vcs_resolver_form.pas' {VCS_Resolver_f},
  u_vcs_resolver_one_file_frame in 'u_vcs_resolver_one_file_frame.pas' {vcs_resolver_one_file_frame: TFrame},
  vcs_resolver_const in 'vcs_resolver_const.pas',
  u_file_manager_VCS_log_frame in '..\u_file_manager_VCS_log_frame.pas' {VCS_Log_Frame: TFrame},
  fisFileNotification in '..\libs\fisFileNotification\fisFileNotification.pas',
  u_vcs_about in 'u_vcs_about.pas' {AboutBox},
  U_Ver in '..\U_Ver.pas',
  U_URLs in '..\U_URLs.pas',
  u_vcs_diff_wrapper in 'u_vcs_diff_wrapper.pas',
  vcs_diff_frame in 'vcs_diff_frame.pas' {View_diff_frame: TFrame},
  vcs_diff_form in 'vcs_diff_form.pas' {Diff_F},
  vcs_password_form in 'vcs_password_form.pas' {Credential_F},
  U_CheckUpdate in '..\U_CheckUpdate.pas',
  vcs_view_image_frame in 'vcs_view_image_frame.pas' {View_Image_Frame: TFrame},
  u_image_utils in 'u_image_utils.pas',
  u_vcs_start_up_master_f in 'u_vcs_start_up_master_f.pas' {StartUp_Master_Form},
  u_ms_word_compare in '..\formats\word\u_ms_word_compare.pas',
  u_ms_ole_common in '..\formats\ms_common\u_ms_ole_common.pas',
  u_multi_page_const in '..\formats\multi_page\u_multi_page_const.pas',
  visio_converter in '..\formats\visio\visio_converter.pas',
  vcs_diff_pagelist_frame in 'vcs_diff_pagelist_frame.pas' {Diff_Pagelist_Frame: TFrame},
  vcs_view_image_const in 'vcs_view_image_const.pas',
  u_vcs_portable_const in 'u_vcs_portable_const.pas',
  u_vcs_ico_const in 'u_vcs_ico_const.pas',
  u_vcs_repo_catalog in 'u_vcs_repo_catalog.pas',
  u_web_utils in '..\u_web_utils.pas',
  vcs_options_form_diff_edit in 'vcs_options_form_diff_edit.pas' {Option_Edit_Diff_F},
  u_vcs_diff_options_const in 'u_vcs_diff_options_const.pas',
  kompas_converter in '..\formats\kompas\kompas_converter.pas',
  KsTLB in '..\formats\kompas\KsTLB.pas',
  U_VCS_Hosters in '..\hosts\U_VCS_Hosters.pas',
  superobject in '..\libs\superobject.pas',
  u_vcs_hosting_form in 'u_vcs_hosting_form.pas' {VCS_Hosting_Form},
  U_Msg_Questions in '..\U_Msg_Questions.pas',
  u_vcs_diff_wrapper_previewer in 'u_vcs_diff_wrapper_previewer.pas';

{$R *.res}

var hPrevWindow : HWND;
    form_name : string;

    hMutex : integer;

begin
  (* �������� - �� ��� �� ��������� ��������� ������� �����? *)

  hMutex:=CreateMutex( nil, TRUE, 'VCSClient7c4wqf8nwa9m2Mutex' );
  (* ������ �������� - ������ ��� ������ *)
  if GetLastError <> 0
    then
      begin
        (* ������� ����� ���� ����� ����������� ����������. �������� ������ ����� ������ ��������� *)
        form_name := TVCS_Client_Project_List_F.ClassName;

        (* ���� �� ������ ����. ������ �������� - caption - ����������, ������ �� ���� ����� ��� *)
        hPrevWindow := FindWindow( pchar( form_name ), nil );
        if hPrevWindow <> 0
          then
            begin
              ShowWindow( hPrevWindow, SW_SHOWNORMAL );
              ShowWindow( hPrevWindow, SW_RESTORE );
              SetForegroundWindow( hPrevWindow );
            end;
        (* ���� ��������� ��� �������, �� �������� �� ����� ���� ������ �� ����,
           ������ ������� ������� ���������. ��������, ���������� ��������� ��� ��
           �����������������, ��� ��� ����� ��������� (������ ������ ��������) *)
        Exit;
      end
    else
      begin
        if ( Prepare_SVN_bins ) and ( Prepare_Image_bins ) and ( Prepare_Readme_Path ) and ( Prepare_Ico_Path )
          then
            begin
              {$message warn '���� �������������� rand �����'}
              Randomize;

              Prepare_Paths;

              {$i test.inc}
              {$ifdef program_start_fail}
              Write_Log( 'Test Load Fail', True );
              raise Exception.Create('Test Load Fail');
              {$endif}

              Get_Version_From_Exe;

              (* ������� ���� �������� *)
              Ini_Init;

              Application.Initialize;
              Application.Title := program_title;

              Application.CreateForm(TVCS_Client_Project_List_F, VCS_Client_Project_List_F);

              Application.Run;

              (* ������� ���� �������� *)
              Ini_Done;
            end
          else
            ShowMsg( 'Failed to load application! Please, reinstall!' );

        ReleaseMutex( hMutex );
      end;
end.
