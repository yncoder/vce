unit vcs_view_image_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, StdCtrls, Controls, Forms,
  Dialogs, ExtCtrls,
  vcs_diff_pagelist_frame, vcs_view_image_const;

(* ���������, ������ ��� ��������� �������� � ����������� ���������� *)
type
  TMyScrollBox = class(TScrollBox)
  private
    FOnScroll: TNotifyEvent;
    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
  protected
    procedure Scroll; virtual;
  published
    property OnScroll: TNotifyEvent read FOnScroll write FOnScroll;
  end;

type
  TView_Image_Frame = class(TFrame)
    procedure Image1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Image1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ScrollBox1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ScrollBox1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
  private
    { Private declarations }
    fPage_List : TDiff_Pagelist_Frame;
    fFile_Name : string;
    File_Caption_E : TEdit;
    f_View_Mode : T_View_Mode;
    Spot : TPoint;
    procedure Set_View_Mode( Arg_View_Mode : T_View_Mode );
  public
    Image : TImage;
    ScrollBox : TMyScrollBox;

    { Public declarations }
    property View_Mode : T_View_Mode read f_View_Mode write Set_View_Mode;
    procedure Init( Arg_File_Name,            // ���� ��� �����������
                    Arg_File_Caption          // ���� ����, �� ������������ ������ ����� �����. ��������, ���� ����� � pristine, � ����� �� ���� ����� ������
                    : string;                 //
                    Arg_Detect_Base_FileName : string; // ���� ���� ����� � �������, ��� �����������, �� ���� �������� ������� ��� ����� ��� ������� ���� �� ����������
                    Arg_Scroll_Event : TNotifyEvent // ������������� ��������
                    );
    procedure Create_PageList( out Out_Diff_Pagelist_Frame : TDiff_Pagelist_Frame );
    (* ����� ���� ��� ����������� *)
    procedure Change_File( Arg_File_Name : string );
    function Export_Image( Arg_Dest_Filename : string ) : boolean;
    function Get_FileName : string;
  end;

implementation

uses GraphicEx;

{$R *.dfm}

procedure TMyScrollBox.WMHScroll(var Message: TWMHScroll);
begin
  inherited;
  Scroll;
end;

procedure TMyScrollBox.WMVScroll(var Message: TWMVScroll);
begin
  inherited;
  Scroll;
end;

procedure TMyScrollBox.Scroll;
begin
  if Assigned(FOnScroll) then
    FOnScroll(Self);
end;

{ TView_Image_Frame }

procedure TView_Image_Frame.Change_File;
begin
  fFile_Name := Arg_File_Name;
  Image.Picture.LoadFromFile( fFile_Name );
end;

procedure TView_Image_Frame.Create_PageList;
begin
  fPage_List := TDiff_Pagelist_Frame.Create( Self );
  fPage_List.Parent := Self;
  fPage_List.Align := alBottom;
  Out_Diff_Pagelist_Frame := fPage_List;
end;

function TView_Image_Frame.Export_Image;
begin
  (* ������������ ���� ��� ����, ����� ��� ������ ����������� *)
  Result := CopyFile( pansichar( fFile_Name ), pansichar( Arg_Dest_Filename ), False );
end;

function TView_Image_Frame.Get_FileName: string;
begin
  Result := fFile_Name;
end;

procedure TView_Image_Frame.Image1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if Source is TImage
    then
      begin
        if Sender is TImage
          then
            begin
              ScrollBox.HorzScrollBar.Position := ScrollBox.HorzScrollBar.Position + ( Spot.X - X );
              ScrollBox.VertScrollBar.Position := ScrollBox.VertScrollBar.Position + ( Spot.Y - Y );
              ScrollBox.Scroll;
            end;
      end;
end;

procedure TView_Image_Frame.Image1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  accept := ( Source is TImage );
end;

procedure TView_Image_Frame.Image1MouseDown;
begin
  (* ������, ��� �������� ����� ���� � ��� ������ �� ����� �������, � ������������� �� ����
     http://stackoverflow.com/questions/32644879/check-is-tscrollbox-scrollbars-are-actually-visible *)
  if ( Button = mbLeft ) and ( ( ScrollBox.HorzScrollBar.IsScrollBarVisible ) or ( ScrollBox.VertScrollBar.IsScrollBarVisible ) )
    then
      begin
        Spot.X := X;
        Spot.Y := Y;
        Image.BeginDrag( True );
      end;
end;

procedure TView_Image_Frame.Image1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var parent_cntrl : TWinControl;
begin
  (* ��� ���-�� ����� ���� � ������ � ������������ ��������� �� �������� ���� *)
  parent_cntrl := parent.Parent;
  while ( not ( parent_cntrl is tform ) ) and ( parent_cntrl <> nil ) do
    begin
      parent_cntrl := parent_cntrl.Parent;
    end;

  if ( Assigned( parent_cntrl ) ) and ( parent_cntrl as TForm ).active
    then
      begin
        if not ScrollBox.Focused
          then
            ScrollBox.SetFocus;
      end;
end;

procedure TView_Image_Frame.Init;
var GraphicExClass: TGraphicExGraphicClass; // �������
    GraphicClass : TGraphicClass;
    Graphic: TGraphic;
begin
  Visible := False;
  fFile_Name := Arg_File_Name;
  
  (* ������ ����� *)
  if not Assigned( Image )
    then
      begin
        (* ������� �������� *)
        File_Caption_E := TEdit.Create( Self );
        with File_Caption_E do
          begin
            Align := alBottom;
            Parent := Self;
            ReadOnly := True;
            Color := clBtnFace;
            if Arg_File_Caption <> ''
              then
                Text := Arg_File_Caption
              else
                Text := Arg_File_Name;
          end;

        if Assigned( fPage_List )
          then
            fPage_List.Top := 1;
            
        ScrollBox := TMyScrollBox.Create( Self );
        with ScrollBox do
          begin
            Parent := Self;
            Align := alClient;

            if Assigned( Arg_Scroll_Event )
              then
                OnScroll := Arg_Scroll_Event;
          end;

        (* ���������� ����� *)
        Image := TImage.Create( ScrollBox );
        with Image do
          begin
            Parent := ScrollBox;
            Proportional := true;
            View_Mode := vm_Original_Size;

            (* ����������� �������������� ����� ������ *)
            OnDragDrop := Image1DragDrop;
            OnDragOver := Image1DragOver;
            OnMouseDown := Image1MouseDown;
            OnMouseMove := Image1MouseMove;
          end;

        ScrollBox.OnDragDrop := ScrollBox1DragDrop;
        ScrollBox.OnDragOver := ScrollBox1DragOver;
      end;

  (* ���������� �������� �������� *)
  if Arg_Detect_Base_FileName <> ''
    then
      begin
        // determine true file type from content rather than extension
        GraphicExClass := FileFormatList.GraphicFromContent( Arg_File_Name );
        if GraphicExClass = nil
          then
            begin
              (* ���������� ��� �� �������� �� ����� - �������� ���������� ������������ ����� � �������� ������ ������� ���� ������������� *)
              GraphicClass := FileFormatList.GraphicFromExtension( ExtractFileExt( Arg_Detect_Base_FileName ) );
              Graphic := GraphicClass.Create;
            end
          else
            begin
              // GraphicFromContent always returns TGraphicExGraphicClass
              Graphic := GraphicExClass.Create;
            end;

        if Graphic = nil
          then
            begin
              //ShowMessage( 'nil' );
            end
          else
            begin
              Graphic.LoadFromFile(Arg_File_Name);
              Image.Picture.Graphic := Graphic;
            end;
      end
    else
      Image.Picture.LoadFromFile( Arg_File_Name );

  Visible := True;      
end;

procedure TView_Image_Frame.ScrollBox1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if Source is TImage
    then
      begin
        if Sender is TImage
          then
            begin
              ScrollBox.HorzScrollBar.Position := ScrollBox.HorzScrollBar.Position + ( Spot.X - X );
              ScrollBox.VertScrollBar.Position := ScrollBox.VertScrollBar.Position + ( Spot.Y - Y );
              ScrollBox.Scroll;
            end;
      end;
end;

procedure TView_Image_Frame.ScrollBox1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  accept := ( Source is TImage );
end;

procedure TView_Image_Frame.Set_View_Mode;
begin
  (* ����� ��������� - ������������ ������ ���� ������� � ������ *)
  if Arg_View_Mode <> f_View_Mode
    then
      begin
        if Assigned( Image )
          then
            begin
              if Arg_View_Mode = vm_Fit
                then
                  begin
                    Image.AutoSize := False;
                    Image.Align := alClient;
                    Image.Stretch := true;
                  end
                else
                  begin
                    Image.AutoSize := True;
                    Image.Align := alNone;
                    Image.Stretch := False;
                  end;
            end;

        f_View_Mode := Arg_View_Mode;
      end;
end;

end.
