unit vcs_diff_frame_const;

interface

(* ��� ��� ����� ����������� ����������� ��������� ������ *)
type T_Compare_Files_Cash = record
       private
        ffile_cash : array of
                      record
                        left_page, right_page : integer;
                        file_name : string;
                        trim_px : integer; (* ���� �������� ���� � �������� *)
                       end;
        procedure Set_Nil;
        (* -1 ���� �� ������� *)
        function Get( Arg_Trim_px : integer ; Arg_left_page , Arg_right_page : integer ) : integer;
        procedure Show_List;
       public
        procedure Init;
        procedure Save_Compared_File( Arg_FN : string; Arg_Trim_px : integer ; const arg_left_page : integer = 0; const arg_right_page : integer = 0 );
        function Get_Compared_File( Arg_Trim_px : integer; const arg_left_page : integer = 0; const arg_right_page : integer = 0 ) : string;
        procedure Done;
      end;

implementation

uses dialogs, sysutils, U_Msg;

{ T_Compare_Files_Cash }

procedure T_Compare_Files_Cash.Done;
begin
  Set_Nil;
end;

function T_Compare_Files_Cash.Get;
var i : integer;
begin
  Result := -1;

  for i := 0 to high( ffile_cash ) do
    with ffile_cash[i] do
      begin
        if ( trim_px = Arg_Trim_px )
           and
           ( left_page = arg_left_page )
           and
           ( right_page = arg_right_page )
          then
            begin
              result := i;
              break;
            end;
    end;
end;

function T_Compare_Files_Cash.Get_Compared_File;
var i : integer;
begin
  Result := '';

  i := Get( Arg_Trim_px, arg_left_page, arg_right_page );
  if ( i >= 0 ) and ( FileExists( ffile_cash[i].file_name ) )
    then
      result := ffile_cash[i].file_name;
end;

procedure T_Compare_Files_Cash.Init;
begin
  Set_Nil;
end;

procedure T_Compare_Files_Cash.Show_List;
var i : integer;
    s : string;
begin
  s := '';
  for i := 0 to high( ffile_cash ) do
    with ffile_cash[i] do
      s := s + file_name + format( ' %d %d %d ', [ trim_px, left_page, right_page ] ) + ret;

  ShowMessage( s );
end;

procedure T_Compare_Files_Cash.Save_Compared_File;
var compare_exist : boolean;
    i : integer;
begin
  i := Get( Arg_Trim_px, arg_left_page, arg_right_page );

  if i = -1
    then
      begin
        i := length( ffile_cash );
        SetLength( ffile_cash, i + 1 );
        ffile_cash[ i ].left_page := arg_left_page;
        ffile_cash[ i ].right_page := arg_right_page;
        ffile_cash[ i ].trim_px := Arg_Trim_px;
        ffile_cash[ i ].file_name := Arg_FN;
      end;

  //Show_List;
end;

procedure T_Compare_Files_Cash.Set_Nil;
begin
  SetLength( ffile_cash, 0 );
end;

end.
