object Options_F: TOptions_F
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Options'
  ClientHeight = 513
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 540
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 472
    Width = 592
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Save_B: TButton
      Left = 48
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 0
      OnClick = Save_BClick
    end
    object Close_B: TButton
      Left = 400
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 1
      OnClick = Close_BClick
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 592
    Height = 472
    ActivePage = View_TSh
    Align = alClient
    TabOrder = 1
    object Common_TabSheet: TTabSheet
      Caption = 'Common'
      object Work_Copy_Root_GroupBox: TGroupBox
        Left = 0
        Top = 0
        Width = 584
        Height = 135
        Align = alTop
        Caption = 'Working copy root'
        TabOrder = 0
        DesignSize = (
          584
          135)
        object Work_Copy_Root_L: TLabel
          Left = 16
          Top = 48
          Width = 96
          Height = 13
          Caption = 'Work_Copy_Root_L'
        end
        object Work_Copy_Root_Description_L: TLabel
          Left = 16
          Top = 91
          Width = 155
          Height = 13
          Caption = 'Work_Copy_Root_Description_L'
        end
        object Work_Copy_Root_Check_L: TLabel
          Left = 16
          Top = 110
          Width = 131
          Height = 13
          Caption = 'Work_Copy_Root_Check_L'
        end
        object Work_Copy_Root_E: TEdit
          Left = 16
          Top = 65
          Width = 401
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
        end
        object Select_Work_Copy_Root_B: TButton
          Left = 423
          Top = 63
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Select'
          TabOrder = 2
          OnClick = Select_Work_Copy_Root_BClick
        end
        object Checkout_In_Exe_Folder_CB: TCheckBox
          Left = 16
          Top = 25
          Width = 361
          Height = 17
          Caption = 
            'Place all checkouts to executable Data path (useful for portable' +
            ')'
          TabOrder = 0
          OnClick = Checkout_In_Exe_Folder_CBClick
        end
        object Clear_Work_Copy_Root_B: TButton
          Left = 506
          Top = 63
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Clear'
          TabOrder = 3
          OnClick = Clear_Work_Copy_Root_BClick
        end
      end
      object Proxy_GroupBox: TGroupBox
        Left = 0
        Top = 196
        Width = 584
        Height = 71
        Align = alTop
        Caption = 'HTTP(s) proxy'
        TabOrder = 2
        DesignSize = (
          584
          71)
        object Proxy_Host_L: TLabel
          Left = 16
          Top = 16
          Width = 52
          Height = 13
          Caption = 'Proxy host'
        end
        object Proxy_port_L: TLabel
          Left = 456
          Top = 16
          Width = 51
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Proxy port'
          ExplicitLeft = 598
        end
        object Proxy_Host_E: TEdit
          Left = 16
          Top = 35
          Width = 427
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnChange = Proxy_Host_EChange
        end
        object Proxy_port_E: TSpinEdit
          Left = 456
          Top = 35
          Width = 121
          Height = 22
          Anchors = [akTop, akRight]
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnChange = Proxy_port_EChange
          OnExit = Proxy_port_EExit
        end
      end
      object Monitor_repositories_GB: TGroupBox
        Left = 0
        Top = 267
        Width = 584
        Height = 54
        Align = alTop
        Caption = 'Monitor repositories interval'
        TabOrder = 3
        object Label1: TLabel
          Left = 16
          Top = 25
          Width = 60
          Height = 13
          Caption = 'Check every'
        end
        object Label111: TLabel
          Left = 167
          Top = 25
          Width = 37
          Height = 13
          Caption = 'minutes'
        end
        object Monitor_repositories_interval_E: TSpinEdit
          Left = 82
          Top = 19
          Width = 79
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = Monitor_repositories_interval_EChange
          OnExit = Monitor_repositories_interval_EExit
        end
      end
      object Show_master_on_start_up_GB: TGroupBox
        Left = 0
        Top = 377
        Width = 584
        Height = 67
        Align = alClient
        Caption = 'Start up master'
        TabOrder = 5
        object Show_master_on_start_up_CB: TCheckBox
          Left = 16
          Top = 24
          Width = 383
          Height = 17
          Caption = 'Show master on start up'
          TabOrder = 0
          OnClick = Show_master_on_start_up_CBClick
        end
      end
      object TempFile_Path_RadioGroup: TRadioGroup
        Left = 0
        Top = 135
        Width = 584
        Height = 61
        Align = alTop
        Caption = 'Temporary files path'
        Items.Strings = (
          'System path (default)'
          
            'Application executable path (if no access then system default wi' +
            'll be used)')
        TabOrder = 1
        OnClick = TempFile_Path_RadioGroupClick
      end
      object Commit_MSG_GB: TGroupBox
        Left = 0
        Top = 321
        Width = 584
        Height = 56
        Align = alTop
        Caption = 'Default comment for new commit'
        TabOrder = 4
        DesignSize = (
          584
          56)
        object Default_Commit_MSG_E: TEdit
          Left = 16
          Top = 27
          Width = 553
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnChange = Default_Commit_MSG_EChange
        end
      end
    end
    object Diff_TabSheet: TTabSheet
      Caption = 'Diff'
      ImageIndex = 1
      object Diff_GB: TGroupBox
        Left = 0
        Top = 0
        Width = 584
        Height = 444
        Align = alClient
        Caption = 'External diff utils'
        TabOrder = 0
        object Ext_Diff_LV: TListView
          Left = 2
          Top = 15
          Width = 580
          Height = 273
          Align = alClient
          Columns = <
            item
              AutoSize = True
            end
            item
              AutoSize = True
            end>
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          SortType = stText
          TabOrder = 0
          ViewStyle = vsReport
          OnChange = Ext_Diff_LVChange
          OnDblClick = Ext_Diff_LVDblClick
        end
        object Default_Diff_GB: TGroupBox
          Left = 2
          Top = 336
          Width = 580
          Height = 106
          Align = alBottom
          Caption = 'Default diff'
          TabOrder = 1
          DesignSize = (
            580
            106)
          object Diff_GB_Info_L: TLabel
            Left = 8
            Top = 77
            Width = 401
            Height = 13
            Caption = 
              'Select the default external diff tool for unsupported file forma' +
              'ts, not specified in list'
          end
          object Default_Diff_Exe_E: TEdit
            Left = 8
            Top = 27
            Width = 329
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 0
            OnChange = Default_Diff_Exe_EChange
          end
          object Default_Diff_Exe_Select_B: TButton
            Left = 343
            Top = 23
            Width = 75
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Select'
            TabOrder = 1
            OnClick = Default_Diff_Exe_Select_BClick
          end
          object Default_Diff_Exe_Clear_B: TButton
            Left = 424
            Top = 23
            Width = 75
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Clear'
            TabOrder = 2
            OnClick = Default_Diff_Exe_Clear_BClick
          end
          object Default_Diff_Exe_Test_B: TButton
            Left = 507
            Top = 23
            Width = 63
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Test'
            TabOrder = 3
            OnClick = Default_Diff_Exe_Test_BClick
          end
          object DIff3_CB: TCheckBox
            Left = 8
            Top = 54
            Width = 97
            Height = 17
            Caption = 'Is diff3?'
            TabOrder = 4
            OnClick = DIff3_CBClick
          end
        end
        object Panel1: TPanel
          Left = 2
          Top = 288
          Width = 580
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Add_Diff_Tool_B: TButton
            Left = 5
            Top = 15
            Width = 135
            Height = 25
            Caption = 'Add diff tool'
            TabOrder = 0
            OnClick = Add_Diff_Tool_BClick
          end
          object Edit_Diff_Tool_B: TButton
            Left = 148
            Top = 15
            Width = 159
            Height = 25
            Caption = 'Edit diff tool'
            TabOrder = 1
            OnClick = Ext_Diff_LVDblClick
          end
          object Del_Diff_Tool_B: TButton
            Left = 325
            Top = 17
            Width = 144
            Height = 25
            Caption = 'Del diff tool'
            TabOrder = 2
            OnClick = Del_Diff_Tool_BClick
          end
        end
      end
    end
    object Catalog_TS: TTabSheet
      Caption = 'Catalog'
      ImageIndex = 2
      object Catalog_URL_GB: TGroupBox
        Left = 0
        Top = 0
        Width = 584
        Height = 105
        Align = alTop
        Caption = 'Catalog URL'
        TabOrder = 0
        DesignSize = (
          584
          105)
        object Catalog_URL_Check_L: TLabel
          Left = 8
          Top = 72
          Width = 6
          Height = 13
          Caption = '*'
        end
        object Catalog_URL_E: TEdit
          Left = 8
          Top = 32
          Width = 561
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnChange = Catalog_URL_EChange
        end
      end
    end
    object View_TSh: TTabSheet
      Caption = 'View'
      ImageIndex = 2
      object ToolBar_Type_RG: TRadioGroup
        Left = 0
        Top = 0
        Width = 584
        Height = 153
        Align = alTop
        Caption = 'Tool bar images and captions'
        Items.Strings = (
          'Big images and text'
          'Big images'
          'Small images and text'
          'Small images'
          'Text only')
        TabOrder = 0
        OnClick = ToolBar_Type_RGClick
      end
      object Font_GB: TGroupBox
        Left = 0
        Top = 153
        Width = 584
        Height = 168
        Align = alTop
        Caption = 'Fonts (not for all windows)'
        TabOrder = 1
        object Select_Font_B: TButton
          Left = 477
          Top = 56
          Width = 75
          Height = 25
          Caption = 'Select font'
          TabOrder = 0
          OnClick = Select_Font_BClick
        end
        object Font_Example_M: TMemo
          Left = 16
          Top = 40
          Width = 417
          Height = 89
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
        end
      end
    end
    object Information_TSh: TTabSheet
      Caption = 'Information'
      ImageIndex = 2
      DesignSize = (
        584
        444)
      object Ini_Path_L: TLabel
        Left = 3
        Top = 3
        Width = 122
        Height = 13
        Caption = 'Path to configuration file:'
      end
      object Log_Path_L: TLabel
        Left = 3
        Top = 49
        Width = 73
        Height = 13
        Caption = 'Path to log file:'
      end
      object Ini_Path_E: TEdit
        Left = 3
        Top = 22
        Width = 468
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
      end
      object Log_Path_E: TEdit
        Left = 3
        Top = 68
        Width = 468
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
      end
      object Ini_Path_B: TButton
        Left = 477
        Top = 20
        Width = 104
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Open in explorer'
        TabOrder = 1
        OnClick = Ini_Path_BClick
      end
      object Log_Path_B: TButton
        Left = 477
        Top = 66
        Width = 104
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Open in explorer'
        TabOrder = 3
        OnClick = Log_Path_BClick
      end
    end
  end
  object FontDialog1: TFontDialog
    OnShow = FontDialog1Show
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = []
    Left = 488
    Top = 280
  end
end
