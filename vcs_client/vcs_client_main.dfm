object VCS_Client_Project_List_F: TVCS_Client_Project_List_F
  Left = 0
  Top = 0
  Caption = 'VCS Client Projects list'
  ClientHeight = 373
  ClientWidth = 600
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 600
    Height = 23
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 66
    Caption = 'ToolBar1'
    Flat = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = Add_Project_A
      AutoSize = True
    end
    object ToolButton2: TToolButton
      Left = 67
      Top = 2
      Action = View_Project_A
      AutoSize = True
    end
    object ToolButton7: TToolButton
      Left = 137
      Top = 2
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 145
      Top = 2
      Action = Options_A
      AutoSize = True
    end
    object ToolButton4: TToolButton
      Left = 193
      Top = 2
      Action = About_A
      AutoSize = True
    end
    object ToolButton6: TToolButton
      Left = 233
      Top = 2
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 241
      Top = 2
      Action = Close_A
      AutoSize = True
    end
  end
  object Projects_PC: TPageControl
    Left = 0
    Top = 44
    Width = 600
    Height = 329
    ActivePage = My_Projects_TS
    Align = alClient
    TabOrder = 1
    OnChange = Projects_PCChange
    object My_Projects_TS: TTabSheet
      Caption = 'My projects'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Projects_LV: TListView
        Left = 0
        Top = 0
        Width = 592
        Height = 301
        Align = alClient
        Columns = <
          item
            AutoSize = True
            Caption = 'Local path'
          end
          item
            AutoSize = True
            Caption = 'Server url'
          end>
        ColumnClick = False
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnChange = Projects_LVChange
        OnDblClick = View_Project_AExecute
      end
    end
    object Catalog_TS: TTabSheet
      Caption = 'Catalog'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Catalog_LV: TListView
        Left = 0
        Top = 0
        Width = 592
        Height = 301
        Align = alClient
        Columns = <
          item
            AutoSize = True
            Caption = 'Name'
          end
          item
            AutoSize = True
            Caption = 'Server Url'
          end
          item
            AutoSize = True
            Caption = 'Description'
          end
          item
            AutoSize = True
            Caption = 'Local path'
          end>
        ColumnClick = False
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnChange = Projects_LVChange
        OnDblClick = Catalog_LVDblClick
      end
    end
  end
  object Filter_E: TEdit
    Left = 0
    Top = 23
    Width = 600
    Height = 21
    Hint = 'Filter'
    Align = alTop
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnChange = Filter_EChange
  end
  object ActionList1: TActionList
    Left = 488
    Top = 88
    object Add_Project_A: TAction
      Category = 'Visible'
      Caption = 'Add project'
      OnExecute = Add_Project_AExecute
    end
    object View_Project_A: TAction
      Category = 'Visible'
      Caption = 'View project'
      OnExecute = View_Project_AExecute
    end
    object Close_A: TAction
      Category = 'Visible'
      Caption = 'Close'
      OnExecute = Close_AExecute
    end
    object Options_A: TAction
      Category = 'Visible'
      Caption = 'Options'
      OnExecute = Options_AExecute
    end
    object Create_Demo_Repo_And_Add_A: TAction
      Caption = 'Create_Demo_Repo_And_Add_A'
      OnExecute = Create_Demo_Repo_And_Add_AExecute
    end
    object Connect2demo_github_A: TAction
      Caption = 'Connect2demo_github_A'
      OnExecute = Connect2demo_github_AExecute
    end
    object About_A: TAction
      Category = 'Visible'
      Caption = 'About'
      OnExecute = About_AExecute
    end
  end
  object Timer1: TTimer
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 472
    Top = 200
  end
  object XPManifest1: TXPManifest
    Left = 432
    Top = 112
  end
end
