unit u_vcs_ico_const;

(* ������� ������ *)

interface

uses ComCtrls, ImgList;

(* ������ ��������� ������ *)
var Small_FileState_ImageList : TCustomImageList;
    (* ���� ��� ��� ������ ��������� ���������. ����� ������ ��� ����������� *)
    Small_FileState_ImageList_Done : boolean = False;

    (* ������� ���������� *)
    Small_Interface_Image_List, Big_Interface_Image_List : TCustomImageList;
    Interface_Image_List_Done : boolean = False;

type T_Intarface_Img_Ico = ( iii_Options, iii_About, iii_Add_New_Checkout, iii_Open_Checkout, iii_Close, iii_Open_in_Explorer, iii_Edit_Project, iii_View_Log, iii_Revert,
iii_Diff, iii_Update, iii_Commit, iii_Stage, iii_Resolve );

const action_ico_folder = 'actions\';

const Intarface_Img_Ico : array[ T_Intarface_Img_Ico ] of string =
  ( action_ico_folder + 'gear.ico',
    action_ico_folder + 'gohome.ico',
    action_ico_folder + 'edit_add.ico',
    action_ico_folder + '1rightarrow.ico',
    action_ico_folder + 'fileclose.ico',
    action_ico_folder + 'folder_open.ico',
    action_ico_folder + 'password.ico',
    action_ico_folder + 'view_calendar_time_spent.ico',
    action_ico_folder + 'revert.ico',
    action_ico_folder + 'document_preview.ico',
    action_ico_folder + 'down.ico',
    action_ico_folder + 'up.ico',
    action_ico_folder + 'flag.ico',
    'conflicted.ico'    (* ��������� � ������� ��������� �������� *)
  );


function Load_Icon_Res : boolean;

(* Arg_Always_Show_Captions - ������������ ����� ��������� *)
procedure Set_ToolBar( Arg_ToolBar : TToolBar; Arg_Always_Show_Captions : boolean = False );

implementation

uses Graphics, SysUtils, Forms,

     dialogs,

     u_const, u_file_manager_VCS_const, u_vcs_Ini;

function Load_Icon_Res;
var temp_ico : ticon;
    VCS_State : T_File_VCS_States;
    Intarface_Img_Icon : T_Intarface_Img_Ico;
    s : string;

begin
  Result := False;
  temp_ico := nil;
  Small_FileState_ImageList := TCustomImageList.Create( application );
  //Small_FileState_ImageList.Width := 32;
  //Small_FileState_ImageList.Height := 32;

  Small_Interface_Image_List := TCustomImageList.Create( Application );
  with Small_Interface_Image_List do
    begin
      Width := 16;
      Height := 16;
    end;
  Big_Interface_Image_List := TCustomImageList.Create( Application );
  with Big_Interface_Image_List do
    begin
      Width := 32;
      Height := 32;
    end;

  try
    temp_ico := TIcon.Create;

    for VCS_State := low( T_File_VCS_States ) to high( T_File_VCS_States ) do
      begin
        s := ico_full_path + T_File_VCS_State_Titles[ VCS_State ] + '.ico';
        if FileExists(s)
          then
            begin
              temp_ico.LoadFromFile( s );
              Small_FileState_ImageList.AddIcon( temp_ico );
            end
          else
            break;
      end;
    Small_FileState_ImageList_Done := Small_FileState_ImageList.count = ( integer( high( T_File_VCS_States ) ) + 1 );

    for Intarface_Img_Icon := low( T_Intarface_Img_Ico ) to High( T_Intarface_Img_Ico ) do
      begin
        s := ico_full_path + Intarface_Img_Ico[ Intarface_Img_Icon ];
        if FileExists(s)
          then
            begin
              temp_ico.LoadFromFile( s );
              Small_Interface_Image_List.AddIcon( temp_ico );
              Big_Interface_Image_List.AddIcon( temp_ico );
            end
          else
            break;
      end;

    Interface_Image_List_Done := Small_Interface_Image_List.Count = ( integer( High( T_Intarface_Img_Ico ) ) + 1 );

  finally
    if Assigned( temp_ico )
      then
        temp_ico.Free;
  end;
end;

procedure Set_ToolBar;
var toolbar_type : T_Toolbar_type;
    i : integer;
begin
  if Interface_Image_List_Done
    then
      toolbar_type := Ini_Strore.Get_ToolBar_Type
    else
      toolbar_type := tbt_BigImage_Text;
  with Arg_ToolBar do
    begin
      case toolbar_type of
        tbt_BigImage_Text   : begin ShowCaptions := True  or Arg_Always_Show_Captions; Images := Big_Interface_Image_List;                                       end;
        tbt_BigImage        : begin ShowCaptions := False or Arg_Always_Show_Captions; Images := Big_Interface_Image_List;   ButtonHeight := ButtonHeight div 2; (* ��� - ����� �� ����������� ������ ������� ����� ������� ������ *)  end;
        tbt_SmallImage_Text : begin ShowCaptions := True  or Arg_Always_Show_Captions; Images := Small_Interface_Image_List;                                     end;
        tbt_SmallImage      : begin ShowCaptions := False or Arg_Always_Show_Captions; Images := Small_Interface_Image_List; ButtonHeight := ButtonHeight div 2; end;
        tbt_Text_Only       : begin ShowCaptions := True  or Arg_Always_Show_Captions; Images := nil;                                                            end;
       end;

      for i := 0 to ButtonCount - 1 do
        Buttons[ i ].AutoSize := True;
    end;
end;

end.
