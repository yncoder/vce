unit u_vcs_diff_wrapper_previewer;

interface

uses u_file_manager_VCS_Base;

procedure Prepare_No_Diff_MSG( Arg_Right_File : string; Arg_VCS_Wrapper_Base : T_VCS_Wrapper_Base; Arg_File_IDX : integer );

procedure Show_No_Diff( Arg_Diff_Text : string = '' );

implementation

uses u_vcs_diff_wrapper, U_Msg_Questions, u_msg, Controls, u_vcs_ico_const;

procedure Prepare_No_Diff_MSG;
begin
  Show_No_Diff( Arg_Right_File + ret +  Arg_VCS_Wrapper_Base.Get_Diff_Text( Arg_File_IDX ) );
end;

procedure Show_No_Diff;
var BL : array[1..2] of T_Button_Data;
    mr : TModalResult;
begin
  (* � �������� ������� *)
  BL[2].fCaption := cap( cap_Open_Diff_Options_Confirm );
  BL[2].fResult := mrYes;
  BL[2].fIntarface_Img_Ico := iii_Options;

  BL[1].fCaption := 'Close';
  BL[1].fResult := mrNo;
  BL[1].fIntarface_Img_Ico := iii_Close;

  mr := Show_Form_With_Buttons( cap( cap_No_Diff_Tool_Configured ), Arg_Diff_Text, BL );
  if mr = mrYes
    then
      v_Show_Diff_Options_Event;
end;

end.
