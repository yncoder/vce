object vcs_resolver_one_file_frame: Tvcs_resolver_one_file_frame
  Left = 0
  Top = 0
  Width = 451
  Height = 57
  Align = alTop
  TabOrder = 0
  DesignSize = (
    451
    57)
  object File_L: TLabel
    Left = 3
    Top = 3
    Width = 27
    Height = 13
    Caption = 'File_L'
  end
  object File_E: TEdit
    Left = 3
    Top = 22
    Width = 260
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 0
    OnEnter = File_EEnter
  end
  object Execute_B: TButton
    Left = 75
    Top = 3
    Width = 106
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Execute'
    TabOrder = 1
  end
  object Show_in_Explorer_B: TButton
    Left = 269
    Top = 20
    Width = 97
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Show in Explorer'
    TabOrder = 2
    OnClick = Show_in_Explorer_BClick
  end
  object Use_This_File_B: TButton
    Left = 372
    Top = 20
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Use this file'
    ModalResult = 1
    TabOrder = 3
    OnClick = Use_This_File_BClick
  end
end
