unit vcs_diff_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, vcs_view_image_frame, u_vcs_diff_wrapper, u_multi_page_const,
  vcs_diff_pagelist_frame, vcs_view_image_const, vcs_diff_frame_const, ActnList, ComCtrls, ToolWin,
  Spin;

type T_Close_Action = procedure of object;

{$i test.inc}

type
  TView_diff_frame = class(TFrame)
    GridPanel1: TGridPanel;
    View_Image_Frame1: TView_Image_Frame;
    View_Image_Frame2: TView_Image_Frame;
    View_Image_Frame3: TView_Image_Frame;
    ActionList1: TActionList;
    Fit_A: TAction;
    Compare_A: TAction;
    ToolBar1: TToolBar;
    Fit_ToolButton: TToolButton;
    Compare_ToolButton: TToolButton;
    ToolButton3: TToolButton;
    ToolButton1: TToolButton;
    Sync_Pages_A: TAction;
    ToolButton2: TToolButton;
    Export_Compared_Img_A: TAction;
    ToolButton4: TToolButton;
    SaveDialog1: TSaveDialog;
    Execute_Compared_Img_A: TAction;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    Close_A: TAction;
    About_A: TAction;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Trim_TB: TToolButton;
    Trim_Compared_A: TAction;
    Shave_E: TSpinEdit;
    procedure FrameResize(Sender: TObject);
    procedure Compare_AExecute(Sender: TObject);
    procedure Fit_AExecute(Sender: TObject);
    procedure Sync_Pages_AExecute(Sender: TObject);
    procedure Export_Compared_Img_AExecute(Sender: TObject);
    procedure Execute_Compared_Img_AExecute(Sender: TObject);
    procedure Close_AExecute(Sender: TObject);
    procedure About_AExecute(Sender: TObject);
    procedure Trim_Compared_AExecute(Sender: TObject);
    procedure Shave_EExit(Sender: TObject);
  private
    { Private declarations }
    fLeft_File, fRight_File : string;
    (* ������ �� ����� ��� ���������� �������� ���������
       ���������������� ������, ��� ������ fMultipage ���������� �� fLeft_File � fRight_File *)
    fLeft4Compare_File, fRight4Compare_File : string;
    fImage_Convert_Format : T_Convertable_Formats;
    fMultipage : T_Multipage_Formats;
    {$message warn '���� ��� �������� � case record'}
    fMultipage_Wrapper : T_Base_Multipage_Wrapper;
    (* ������ ��� ������ Multipage *)
    fMultipage_left_Doc_IDX, fMultipage_right_Doc_IDX : integer;
    (* ������ ��� ������ Multipage *)
    fMultipage_left_Page_IDX, fMultipage_right_Page_IDX : integer;

    (* ����� ���������, ����� ��� ���� ���� � ���������� *)
    fView_Mode : T_View_Mode;

    (* �������� ����� ����������� �����-��������� *)
    fCompare_Files_Cash : T_Compare_Files_Cash;

    Diff_Pagelist_Frame1: TDiff_Pagelist_Frame;
    Diff_Pagelist_Frame2: TDiff_Pagelist_Frame;

    fClose_Action : T_Close_Action;

    procedure Set_Command_Enabled( Arg_Disable_All : boolean = False );
    procedure Set_Trim_Enabled;
    procedure Set_Compare_Enabled;
    procedure Show_Compare( Arg_FileName : string );
    procedure Hide_Compare;
    procedure Image_Scroll(Sender: TObject);
    procedure Set_GridPanel( Arg_Compare_Visible : boolean );

    procedure Compare_Image;
    procedure Refresh_Compared;
    (* ��� ��� �� ��������� 2 �����, �� �������� ���������� 2 ������ ����������� � ���� *)
    function Convert_Image( var Arg_FN_1, Arg_FN_2 : string ) : boolean;
    procedure Close_Frame;
    function Show_Page_for_Multipage( var arg_left_file, arg_right_file : string; Arg_Left_Page, Arg_Right_Page : integer ) : boolean;
    {$message warn '���� �������� Left_Page_Changed � Right_Page_Changed, ��������, ������� �� ��������� � ���� ������ ������ � ������ 2� ��������� '}
    procedure Left_Page_Changed( const Arg_Page_IDX : integer );
    procedure Right_Page_Changed( const Arg_Page_IDX : integer );
    function Export_Page( Arg_Doc_Idx : integer; Arg_Base_Name : string; Arg_Page : integer;
                           out Out_Result_FileName : string;
                           const Arg_Diff_Pagelist_Frame : TDiff_Pagelist_Frame = nil ) : boolean;
  public
    { Public declarations }
    property View_Mode : T_View_Mode read fView_Mode;
    constructor Create( AOwner: TComponent; Arg_Left_File, Arg_Left_File_Caption, Arg_Right_File, Arg_Right_File_Caption : string;
                        Arg_Close_Action : T_Close_Action;
                        Arg_Image_Convert : T_Convertable_Formats = cf_None;
                        Arg_Multipage : T_Multipage_Formats = mf_None ); reintroduce; virtual;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

uses U_ShellOp, U_Const, U_Msg, u_image_utils, visio_converter, kompas_converter

     {$include directives.inc}
     {$ifdef CAD_Diff_App}
     , U_About_CAD_Diff
     {$endif}
     ;

{ TView_diff_frame }

procedure TView_diff_frame.About_AExecute(Sender: TObject);
begin
  {$ifdef CAD_Diff_App}
  Show_About;
  {$endif}
end;

procedure TView_diff_frame.Close_AExecute(Sender: TObject);
begin
  if Assigned( fClose_Action )
    then
      fClose_Action;
end;

procedure TView_diff_frame.Close_Frame;
begin
  if fMultipage <> mf_None
    then
      begin
        if Assigned( fMultipage_Wrapper )
          then
            begin
              fMultipage_Wrapper.Free;
              fMultipage_Wrapper := Nil;
            end;
      end;

  fView_Mode := vm_None;
  Set_Command_Enabled( True );
end;

procedure TView_diff_frame.Compare_AExecute(Sender: TObject);
begin
  (* ���� ��������� ���������� - ������ *)
  if View_Image_Frame3.Visible
    then
      begin
        Hide_Compare;
      end
    else
      begin
        Refresh_Compared;
      end;
end;

procedure TView_diff_frame.Compare_Image;
var f1, f2, dest_file : string;

    shave_trim_cmd,
    Program_name : String;
    trim_px : integer;

    Result_Str, Error_Str: string;

    file_prefix : string;

    new_generated : boolean;
begin
  new_generated := false;

  if Trim_Compared_A.Checked
    then
      begin
        (* �������� ���� � Shave_E onExit *)
        trim_px := Shave_E.value;

        shave_trim_cmd := ' -shave ' + IntToStr( trim_px ) +  'x' + IntToStr( trim_px ) +  ' -trim ';
      end
    else
      begin
        shave_trim_cmd := ' ';
        trim_px := 0;
      end;

  (* ����������� - �����, ��� ������ ���������� compare? *)
  if fMultipage <> mf_None
    then
      dest_file := fCompare_Files_Cash.Get_Compared_File( trim_px, fMultipage_left_Page_IDX, fMultipage_right_Page_IDX )
    else
      dest_file := fCompare_Files_Cash.Get_Compared_File( trim_px );

  if dest_file = ''
    then
      begin
        f1 := Escape_Command_Line( fLeft4Compare_File );
        f2 := Escape_Command_Line( fRight4Compare_File );
        (* ��������� ������������ � png *)
        Randomize;
        dest_file := Get_Rnd_Temp_File_Name( True ) + ExtractFileName( f1 ) + '.compared.png';

        if FileExists( dest_file )
          then
            DeleteFile( dest_file );

        (* ��������� ������� � ������, ���� ��� - ������ ����� ���������� *)
        case fImage_Convert_Format of
          cf_None : begin
                      file_prefix := ExtractFileExt( f2 );
                      if ( file_prefix <> '' ) and ( file_prefix[1] = '.' )
                        then
                          Delete( file_prefix, 1, 1 );
                    end;
          else file_prefix := imagemagick_format_prefix[fImage_Convert_Format];
         end;
        if file_prefix <> ''
          then
            file_prefix := file_prefix + ':';

        (* imagemagick convert ����� ������ � compare *)
        Program_name := '@"' + Image_Convert_exe + '" ' + shave_trim_cmd + '-compare ' + file_prefix + '"' + f1 + '" ' + file_prefix + '"' + f2 + '" "' + dest_file + '"';

        {$ifdef testing_msg} ShowMessage( Program_name ); {$endif}

        {$message warn '������, ��� �������� �������� �������. �� ������������, �� �� ������ ��� ��������. ����� �������� ���� highlight. �����, ���� �������������'}
        Execute_To_String_Filed( Program_name, '', '', Result_Str, Error_Str,
                                 {$ifdef testing_msg}True{$else}False{$endif}
                               );
        new_generated := true;
      end;

  (* ��� ���������� ���������� �� ����� - ����� ��� ��������� ���� *)
  if FileExists( dest_file )
    then
      begin
        if new_generated
          then
            begin
              if fMultipage <> mf_None
                then
                  fCompare_Files_Cash.Save_Compared_File( dest_file, trim_px, fMultipage_left_Page_IDX, fMultipage_right_Page_IDX )
                else
                  fCompare_Files_Cash.Save_Compared_File( dest_file, trim_px );
            end;

        Show_Compare( dest_file );
      end
    else
      ShowMsg( cap( cap_Image_Compare_Fail ) );
end;

function TView_diff_frame.Convert_Image;

(* ����������� ��������� ������ ��� ����������� - ��� ������ ����� *)
function Build_String( var Arg_FN : string; out Out_Dest_FN : string ) : string;
begin
  Arg_FN := Escape_Command_Line( Arg_FN );

  (* ��������� ������������ � png *)
  Out_Dest_FN := Get_Rnd_Temp_File_Name( True ) + ExtractFileName( Arg_FN ) + '.converted.png';

  if FileExists( Out_Dest_FN )
    then
      DeleteFile( Out_Dest_FN );

  Result := '@"' + Image_Convert_exe + '" ' + imagemagick_format_prefix[fImage_Convert_Format] + ':"' + Arg_FN + '" "' + Out_Dest_FN + '"';
end;

var f1, dest_file_1,
    f2, dest_file_2 : string;

    cmd_1, cmd_2 : string;

    Program_name : String;
    Result_Str, Error_Str: string;
begin
  Result := False;

  Randomize;

  f1 := Arg_FN_1;
  f2 := Arg_FN_2;

  cmd_1 := Build_String( f1, dest_file_1 );
  cmd_2 := Build_String( f2, dest_file_2 );

  (* http://stackoverflow.com/questions/33305416/how-to-define-the-input-image-format-for-imagemagick-convert *)
  Program_name := cmd_1 + ret + ret + cmd_2;

  {$ifdef testing_image_convert_fail} Program_name := ' fail ' + Program_name;
  {$endif}

  (* ���� �������������� 2 ����� - ����� �������� � ���� ������ *)
  Execute_To_String_Filed( Program_name, '', '', Result_Str, Error_Str,
                           {$ifdef testing_image_convert_fail} true {$else} false {$endif}
                         );
  (* ��� ���������� ���������� �� ����� - ����� ��� ��������� ���� *)

  if FileExists( dest_file_1 ) and FileExists( dest_file_2 )
    then
      begin
        Result := True;
        Arg_FN_1 := dest_file_1;
        Arg_FN_2 := dest_file_2;
      end
    else
      ShowMsg( cap( cap_Image_Convert_Fail ) );
end;

constructor TView_diff_frame.Create;

function Is_Multipage_Wrapper_File_Open_Correct( Arg_FN : string; var Arg_Doc_IDX : integer ) : boolean;
var error_msg : string;
begin
  Result := False;

  case fMultipage_Wrapper.Open_Invisible_File( Arg_FN, Arg_Doc_IDX, error_msg ) of
    tor_Ok : Result := True;
    tor_File_Not_Found : ShowMsg( cap( cap_File_Not_Found ) + ret + ret + Arg_FN + ret + ret + error_msg );
    tor_Ole_Error_Open_Doc : ShowMsg( cap( cap_Cant_Open_File ) + ret + ret + Arg_FN + ret + ret + error_msg );
   end;
end;

var left_file, right_file : string;
begin
  inherited Create( AOwner );

  {$ifdef CAD_Diff_App}
  About_A.Visible := True;
  {$else}
  About_A.Visible := False;
  {$endif}

  fCompare_Files_Cash.Init;

  Parent := AOwner as TWinControl;

  if Assigned( Arg_Close_Action )
    then
      begin
        fClose_Action := Arg_Close_Action;
        Close_A.Visible := True;
      end
    else
      begin
        fClose_Action := nil;
        Close_A.Visible := False;
      end;

  fMultipage_left_Doc_IDX  := -1;
  fMultipage_right_Doc_IDX := -1;
  fMultipage_left_Page_IDX := -1;
  fMultipage_right_Page_IDX := -1;

  fLeft_File  := Arg_Left_File;
  fRight_File := Arg_Right_File;

  fImage_Convert_Format := Arg_Image_Convert;
  fMultipage := Arg_Multipage;

  fView_Mode := vm_Original_Size;

  left_file  := fLeft_File;
  right_file := fRight_File;

  fLeft4Compare_File  := fLeft_File;
  fRight4Compare_File := fRight_File;

  (* ��� ��� ���������� ������������, �� ��� ����� ��� ������ *)
  if Arg_Right_File_Caption = ''
    then
      Arg_Right_File_Caption := fRight_File;

  (* ������, ��� ��������� �������������� *)
  if fImage_Convert_Format <> cf_None
    then
      begin
        (* � ������� ���������� �������� �������� ����� *)
        if not Convert_Image( right_file, left_file )
          then
            begin
              Close_Frame;
              Exit;
            end;
      end
    else
      begin
        if fMultipage <> mf_None
          then
            begin
              (* �������� ��������� ����������������� *)
              case fMultipage of
                mf_MSVisio : begin
                               fMultipage_Wrapper := T_Visio_Multipage_Wrapper.Create;
                               {$message warn '�� ����, ����� ������ ��� callback-�' }
                               if fMultipage_Wrapper.Init_Wrapper( nil, nil )
                                 then
                                   begin
                                     (* ������� ������� ����� - ���� ���� ���� � ����� - ������ �� ���� *)
                                     if not(
                                         ( Is_Multipage_Wrapper_File_Open_Correct( fLeft_File, fMultipage_left_Doc_IDX ) )
                                           and
                                         ( Is_Multipage_Wrapper_File_Open_Correct( fRight_File, fMultipage_right_Doc_IDX ) )
                                         and
                                         (* ���������� ������ ������� *)
                                         {$message warn '����� ���� �� 0,0, � �� ���������������, ��� �� ����� ���������� ��� ���������'}
                                         ( Show_Page_for_Multipage( left_file, right_file, 0, 0 ) )
                                        )
                                       then
                                         begin
                                          Close_Frame;
                                          Exit;
                                         end;
                                   end
                                 else
                                   begin
                                     ShowMsg( cap( cap_MSVisio_Not_Found ) );
                                     Close_Frame;
                                     Exit;
                                   end;
                             end;
                mf_Kompas : begin
                               fMultipage_Wrapper := T_Kompas_Multipage_Wrapper.Create;
                               {$message warn '�� ����, ����� ������ ��� callback-�' }
                               if fMultipage_Wrapper.Init_Wrapper( nil, nil )
                                 then
                                   begin
                                     (* ������� ������� ����� - ���� ���� ���� � ����� - ������ �� ���� *)
                                     if not(
                                         ( Is_Multipage_Wrapper_File_Open_Correct( fLeft_File, fMultipage_left_Doc_IDX ) )
                                           and
                                         ( Is_Multipage_Wrapper_File_Open_Correct( fRight_File, fMultipage_right_Doc_IDX ) )
                                         and
                                         (* ���������� ������ ������� *)
                                         {$message warn '����� ���� �� 0,0, � �� ���������������, ��� �� ����� ���������� ��� ���������'}
                                         ( Show_Page_for_Multipage( left_file, right_file, 0, 0 ) )
                                        )
                                       then
                                         begin
                                          Close_Frame;
                                          Exit;
                                         end;
                                   end
                                 else
                                   begin
                                     ShowMsg( cap( cap_Kompas_Not_Found ) );
                                     Close_Frame;
                                     Exit;
                                   end;
                            end;
              end;
            end;
      end;

  View_Image_Frame1.Init( left_file, Arg_Left_File_Caption, Arg_Right_File, Image_Scroll );
  View_Image_Frame2.Init( right_file, Arg_Right_File_Caption, '', Image_Scroll );

  Set_Command_Enabled;
  Set_Trim_Enabled;

  Set_GridPanel( False );
end;

destructor TView_diff_frame.Destroy;
begin
  Close_Frame;

  fCompare_Files_Cash.Done;

  inherited;
end;

procedure TView_diff_frame.Fit_AExecute(Sender: TObject);
begin
  if fView_Mode = vm_Original_Size
    then
      fView_Mode := vm_Fit
    else
      fView_Mode := vm_Original_Size;

  View_Image_Frame1.View_Mode := fView_Mode;
  View_Image_Frame2.View_Mode := fView_Mode;

  if View_Image_Frame3.Visible
    then
      View_Image_Frame3.View_Mode := fView_Mode;

  Fit_ToolButton.Down := ( fView_Mode = vm_Fit );
end;

procedure TView_diff_frame.FrameResize(Sender: TObject);
begin
  (* � ����������� GridPanel ���� �������� � �������� - ������ �� ���������. �������� ������ �� http://stackoverflow.com/questions/862146/gridpanel-does-not-adjust-at-first-resize
     � �����, ��� ��� *)
  with GridPanel1.ControlCollection do
    begin
      BeginUpdate;
      EndUpdate;
    end;
end;

procedure TView_diff_frame.Hide_Compare;
begin
  View_Image_Frame3.Visible := False;
  Set_GridPanel( False );
  Set_Command_Enabled;
end;

procedure TView_diff_frame.Image_Scroll(Sender: TObject);

procedure Sync_Scrolls( Arg_Master_S, Arg_Slave_S : TMyScrollBox );
begin
  if Assigned( Arg_Master_S ) and Assigned( Arg_Slave_S )
    then
      begin
        if Arg_Slave_S.VertScrollBar.Position <> Arg_Master_S.VertScrollBar.Position
          then
            Arg_Slave_S.VertScrollBar.Position := Arg_Master_S.VertScrollBar.Position;

        if Arg_Slave_S.HorzScrollBar.Position <> Arg_Master_S.HorzScrollBar.Position
          then
            Arg_Slave_S.HorzScrollBar.Position := Arg_Master_S.HorzScrollBar.Position;
      end;
end;

var Master_S, Slave1_S, Slave2_S : TMyScrollBox;
begin
  (* ������������� �������� *)
  {$message '������ �����������. ��� ��� ��������? �����, � ���� �������? '}

  if ( Sender as TMyScrollBox ).Parent = View_Image_Frame1
    then
      begin
        Master_S := View_Image_Frame1.ScrollBox;
        Slave1_S  := View_Image_Frame2.ScrollBox;
        Slave2_S  := View_Image_Frame3.ScrollBox;
      end
    else
      begin
        if ( Sender as TMyScrollBox ).Parent = View_Image_Frame2
          then
            begin
              Master_S := View_Image_Frame2.ScrollBox;
              Slave1_S  := View_Image_Frame1.ScrollBox;
              Slave2_S  := View_Image_Frame3.ScrollBox;
            end
          else
            begin
              Master_S := View_Image_Frame3.ScrollBox;
              Slave1_S  := View_Image_Frame1.ScrollBox;
              Slave2_S  := View_Image_Frame2.ScrollBox;
            end;
      end;

  Sync_Scrolls( Master_S, Slave1_S );
  Sync_Scrolls( Master_S, Slave2_S );
end;

procedure TView_diff_frame.Left_Page_Changed;
var left_file : string;
    page_id : integer;
begin
  (* �������� �������� *)
  if Export_Page( fMultipage_left_Doc_IDX, fLeft_File + '_l', Arg_Page_IDX, left_file )
    then
      begin
        View_Image_Frame1.Change_File( left_file );

        fLeft4Compare_File  := left_file;

        fMultipage_left_Page_IDX := Arg_Page_IDX;

        if Sync_Pages_A.Checked
          then
            begin
              (* ���������������� �������� *)
              page_id := fMultipage_Wrapper.Get_Page_ID( fMultipage_left_Doc_IDX, Arg_Page_IDX );
              if page_id >= 0
                then
                  Diff_Pagelist_Frame2.Show_Page_by_ID( page_id );
            end;

        if View_Image_Frame3.Visible
          then
            Refresh_Compared
          else
            Set_Compare_Enabled;
      end;
end;

procedure TView_diff_frame.Refresh_Compared;
begin
  (* ����������/�������� ��������� *)
  Set_Command_Enabled( True );

  Compare_Image;

  Set_Command_Enabled;
end;

procedure TView_diff_frame.Right_Page_Changed;
var right_file : string;
    page_id : integer;
begin
  (* �������� �������� *)
  if Export_Page( fMultipage_right_Doc_IDX, fLeft_File + '_r', Arg_Page_IDX, right_file )
    then
      begin
        View_Image_Frame2.Change_File( right_file );

        fRight4Compare_File  := right_file;
        fMultipage_right_Page_IDX := Arg_Page_IDX;

        if Sync_Pages_A.Checked
          then
            begin
              (* ���������������� �������� *)
              page_id := fMultipage_Wrapper.Get_Page_ID( fMultipage_right_Doc_IDX, Arg_Page_IDX );
              if page_id >= 0
                then
                  Diff_Pagelist_Frame1.Show_Page_by_ID( page_id );
            end;

        if View_Image_Frame3.Visible
          then
            Refresh_Compared
          else
            Set_Compare_Enabled;
      end;
end;

procedure TView_diff_frame.Set_Command_Enabled;
begin
  Fit_A.Enabled := not Arg_Disable_All;

  if Arg_Disable_All
    then
      begin
        Compare_A.Enabled := False;
        Trim_Compared_A.Enabled := False;
        Shave_E.Enabled := False;
        Sync_Pages_A.Enabled := False;
        Exit;
      end;

   (* ������������� ������� - �������� ��� ��������������� ���������� *)
   Sync_Pages_A.Enabled := fMultipage <> mf_None;

   Set_Compare_Enabled;

   Execute_Compared_Img_A.Enabled := View_Image_Frame3.Visible;
   Export_Compared_Img_A.Enabled := View_Image_Frame3.Visible;
end;

procedure TView_diff_frame.Set_Compare_Enabled;
begin
  Compare_ToolButton.Down := View_Image_Frame3.Visible;
  Trim_Compared_A.Enabled := True;
  Shave_E.Enabled := True;

  (* ���� ���� ��� ���������� *)
  if ( ( View_Image_Frame1.View_Mode <> vm_None ) and ( View_Image_Frame2.View_Mode <> vm_None ) )
    then
      begin
        Compare_A.Enabled := true;
        (* ���� ������� �������� �� ��������� - �������� �������������� ����� �� ������ *)
        if ( View_Image_Frame1.Image.Picture.Width = View_Image_Frame2.Image.Picture.Width )
           and
           ( View_Image_Frame1.Image.Picture.Height = View_Image_Frame2.Image.Picture.Height )
          then
            Compare_A.Caption := cap( cap_Image_Compare )
          else
            Compare_A.Caption := cap( cap_Image_Compare_Size_Is_Different );
      end
    else
      Compare_A.Enabled := False;
end;

procedure TView_diff_frame.Set_GridPanel;
begin
  (* ��������� ����� *)
  GridPanel1.RowCollection.BeginUpdate;
  if Arg_Compare_Visible
    then
      begin
        GridPanel1.RowCollection[ 0 ].Value := 50;
        GridPanel1.RowCollection[ 1 ].Value := 50;
      end
    else
      begin
        GridPanel1.RowCollection[ 0 ].Value := 100;
        GridPanel1.RowCollection[ 1 ].Value := 0;
      end;

  GridPanel1.RowCollection.EndUpdate;
end;

procedure TView_diff_frame.Set_Trim_Enabled;
var min_size : integer;
begin
  with View_Image_Frame1.Image.Picture do
    if Width > Height
      then
        min_size := Height
      else
        min_size := Width;

  (* ����������� �� ������ ������� - ������ ��� �������� ���������� ����������� �������� *)
  min_size := ( min_size div 2 ) - 1;
  if min_size < 2 then min_size := 1;  

  Shave_E.MaxValue := min_size;
end;

procedure TView_diff_frame.Shave_EExit(Sender: TObject);
var i : integer;
begin
  try
    strtoint( Shave_E.Text );
    if Shave_E.Value < 0
      then
        Shave_E.Value := 0;
  except
    Shave_E.Value := 0;
  end;
end;

procedure TView_diff_frame.Show_Compare;
begin
  {$ifdef testing_msg} ShowMessage( 'Compared file ' + ret + Arg_FileName ); {$endif}

  if View_Image_Frame3.Visible
    then
      View_Image_Frame3.Change_File( Arg_FileName )
    else
      begin
        View_Image_Frame3.Init( Arg_FileName, 'Compared image', '', Image_Scroll );
        View_Image_Frame3.View_Mode := fView_Mode;
        View_Image_Frame3.Visible := True;
        Set_GridPanel( True );

        Image_Scroll( View_Image_Frame3.ScrollBox );
      end;
end;

function TView_diff_frame.Show_Page_for_Multipage;
begin
  Result := False;

  View_Image_Frame1.Create_PageList( Diff_Pagelist_Frame1 );
  View_Image_Frame2.Create_PageList( Diff_Pagelist_Frame2 );

  (* ������ ����� ��� ������ ��������, ������ � temp *)
  if ( Export_Page( fMultipage_left_Doc_IDX, fLeft_File + '_l', Arg_Left_Page, arg_left_file, Diff_Pagelist_Frame1 ) )
     and
     ( Export_Page( fMultipage_right_Doc_IDX, fLeft_File + '_r', Arg_Right_Page, arg_right_file, Diff_Pagelist_Frame2 ) )
    then
      begin
        (* �������� ������ �� �����, ������� ����� �������� ��������� *)
        fLeft4Compare_File  := arg_left_file;
        fRight4Compare_File := arg_right_file;

        (* ������������� ����������� ��� ����� ������� *)
        Diff_Pagelist_Frame1.OnPage_Change := Left_Page_Changed;
        Diff_Pagelist_Frame2.OnPage_Change := Right_Page_Changed;

        fMultipage_left_Page_IDX := Arg_Left_Page;
        fMultipage_right_Page_IDX := Arg_Right_Page;
        Result := True;
      end;
end;

procedure TView_diff_frame.Sync_Pages_AExecute(Sender: TObject);
begin
  (* ����������� ����� ���������������� �������� *)
  Sync_Pages_A.Checked := not Sync_Pages_A.Checked;
end;

procedure TView_diff_frame.Trim_Compared_AExecute(Sender: TObject);
begin
  Trim_Compared_A.Checked := not Trim_Compared_A.Checked;

  if View_Image_Frame3.Visible
    then
      begin
        Refresh_Compared;
      end;
end;

procedure TView_diff_frame.Execute_Compared_Img_AExecute(Sender: TObject);
begin
  if ( View_Image_Frame3.Visible )
    then
      StartFile( View_Image_Frame3.Get_FileName );
end;

procedure TView_diff_frame.Export_Compared_Img_AExecute(Sender: TObject);
var fn : string;
begin
  SaveDialog1.DefaultExt := 'png';
  SaveDialog1.Filter := 'PNG|*.png';
  SaveDialog1.FileName := fRight_File + '_compared.png';
  if ( View_Image_Frame3.Visible ) and ( SaveDialog1.Execute )
    then
      begin
        fn := SaveDialog1.FileName;
        if View_Image_Frame3.Export_Image( fn )
          then
            Show_in_Explorer( fn );
      end;
end;

function TView_diff_frame.Export_Page;
var Page_List: T_Page_List_A;
    error_msg : string;
begin
  Result := False;

  (* ������ ����� ��� ���������, ������ � temp *)
  if fMultipage_Wrapper.Export_File( Arg_Doc_Idx, Arg_Base_Name, True, error_msg, Arg_Page )
    then
      begin
        fMultipage_Wrapper.Get_Pages( Arg_Doc_Idx, Page_List );

        if Assigned( Arg_Diff_Pagelist_Frame )
          then
            Arg_Diff_Pagelist_Frame.Init_Pagelist( Page_List );

        Out_Result_FileName := Page_List[Arg_Page].Saved2File;

        Result := True;
      end
    else
      begin
        ShowMsg( cap( cap_MSVisio_Export_Fail ) + ret + error_msg );
      end;
end;

end.
