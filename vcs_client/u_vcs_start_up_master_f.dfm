object StartUp_Master_Form: TStartUp_Master_Form
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Startup master'
  ClientHeight = 458
  ClientWidth = 447
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  DesignSize = (
    447
    458)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 273
    Height = 13
    Caption = 'Welcome to Version Control for engineers sturtup master'
  end
  object GitHub_L: TLabel
    Left = 24
    Top = 208
    Width = 415
    Height = 62
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '*'
    WordWrap = True
  end
  object Goto_Options_B: TButton
    Left = 24
    Top = 87
    Width = 353
    Height = 33
    Caption = 'Set up Options'
    TabOrder = 1
    OnClick = Goto_Options_BClick
  end
  object Connect2demo_github_B: TButton
    Left = 24
    Top = 284
    Width = 353
    Height = 33
    Caption = 'Add demo github project'
    TabOrder = 3
    OnClick = Connect2demo_github_BClick
  end
  object Do_Not_Show_Again_CB: TCheckBox
    Left = 8
    Top = 374
    Width = 280
    Height = 17
    Caption = 'Do not show master on start up'
    TabOrder = 5
  end
  object Close_B: TButton
    Left = 326
    Top = 368
    Width = 91
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Close'
    TabOrder = 6
    OnClick = Close_BClick
  end
  object Open_Web_Site_B: TButton
    Left = 24
    Top = 337
    Width = 353
    Height = 25
    Caption = 'Open web site'
    TabOrder = 4
    OnClick = Open_Web_Site_BClick
  end
  object Where2Host_Page_B: TButton
    Left = 24
    Top = 143
    Width = 353
    Height = 33
    Caption = 
      'Article - Where you can host your projects for free (online help' +
      ')'
    TabOrder = 2
    OnClick = Where2Host_Page_BClick
  end
  object Add_New_Checkout_B: TButton
    Left = 24
    Top = 41
    Width = 353
    Height = 33
    Caption = 'Add new project'
    TabOrder = 0
    OnClick = Add_New_Checkout_BClick
  end
end
