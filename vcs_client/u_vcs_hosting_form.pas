unit u_vcs_hosting_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls;

type
  TVCS_Hosting_Form = class(TForm)
    Repo_ListView: TListView;
    Panel1: TPanel;
    Label1: TLabel;
    GH_User_CB: TComboBox;
    Fetch_B: TButton;
    Ok_B: TButton;
    Cancel_B: TButton;
    procedure Fetch_BClick(Sender: TObject);
    procedure GH_User_CBChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Repo_ListViewDblClick(Sender: TObject);
    procedure Ok_BClick(Sender: TObject);
    procedure Repo_ListViewClick(Sender: TObject);
  private
    { Private declarations }
    procedure Enable_Interface( Arg_Enable : boolean );
    function Some_Repo_Selected : boolean;
  public
    { Public declarations }
  end;

function Show_Hosted_Repo : string;

implementation

{$R *.dfm}

uses U_VCS_Hosters, u_vcs_Ini, U_Msg;

function Show_Hosted_Repo;
begin
  Result := '';
  with TVCS_Hosting_Form.Create( Application ) do
    begin
      if ShowModal = mrOk
        then
          begin
            if Some_Repo_Selected
              then
                begin
                  Result := Repo_ListView.Items[ Repo_ListView.ItemIndex ].SubItems[0];
                end;
          end;
      Free;
    end;
end;

procedure TVCS_Hosting_Form.Enable_Interface(Arg_Enable: boolean);
begin
  GH_User_CB.Enabled := Arg_Enable;
  Fetch_B.Enabled := ( Arg_Enable ) and ( GH_User_CB.Text <> '' );
  Ok_B.Enabled := ( Arg_Enable ) and Some_Repo_Selected;
  Cancel_B.Enabled := Arg_Enable;
  Repo_ListView.Enabled := Arg_Enable;
end;

procedure TVCS_Hosting_Form.Fetch_BClick(Sender: TObject);
var Hosted_Repo_A : T_Hosted_Repo_A;
    r : T_Hosted_Repo_Rec;
    user_name : string;
begin
  user_name := GH_User_CB.Text;
  if user_name = '' then Exit;
  (* ���� ����� - �������� � ������ *)
  if GH_User_CB.Items.IndexOf( user_name ) < 0
    then
      begin
        GH_User_CB.Items.Add( user_name );
        Ini_Strore.Save_GitHub_Accounts( GH_User_CB.Items );
      end;

  Enable_Interface( False );

  SetLength( Hosted_Repo_A, 0 );

  Repo_ListView.Items.Clear;

  case Get_GitHub_User_Repos( user_name, Hosted_Repo_A ) of
    ghrt_Ok :
      begin
        for r in Hosted_Repo_A do
          begin
            with Repo_ListView.Items.Add do
              begin
                Caption := r.fName;
                SubItems.Add( r.fSVN_Url );
                if r.fFork then SubItems.Add( 'Fork' );
              end;
          end;
      end;
    ghrt_NoSuchUser : ShowMsg( format( cap( cap_No_GitHubUser ), [user_name] ) );
    else Show_Err( cap( cap_No_Internet_Connection ) );
   end;

  SetLength( Hosted_Repo_A, 0 );
  Enable_Interface( True );
end;

procedure TVCS_Hosting_Form.FormCreate(Sender: TObject);
begin
  Ini_Strore.Load_GitHub_Accounts( GH_User_CB.Items );
  if GH_User_CB.Items.Count > 0
    then
      GH_User_CB.ItemIndex := 0;

  Enable_Interface( True );
end;

procedure TVCS_Hosting_Form.GH_User_CBChange(Sender: TObject);
begin
  Enable_Interface( True );
end;

procedure TVCS_Hosting_Form.Ok_BClick(Sender: TObject);
begin
  if Some_Repo_Selected
    then
      ModalResult := mrOk;
end;

procedure TVCS_Hosting_Form.Repo_ListViewClick(Sender: TObject);
begin
  Enable_Interface( True );
end;

procedure TVCS_Hosting_Form.Repo_ListViewDblClick(Sender: TObject);
begin
  Ok_B.Click;
end;

function TVCS_Hosting_Form.Some_Repo_Selected: boolean;
begin
  Result := Repo_ListView.ItemIndex >= 0;
end;

end.
