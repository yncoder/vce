unit vcs_resolver_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  vcs_resolver_const, u_file_manager_VCS_Base, u_file_manager_VCS_const, u_file_manager_VCS_log_frame, u_vcs_diff_options_const,
  ComCtrls;

type
  TVCS_Resolver_f = class(TForm)
    Panel1: TPanel;
    Cancel_B: TButton;
    Diff_B: TButton;
    Diff3_B: TButton;
    PageControl1: TPageControl;
    Conflict_Info_TS: TTabSheet;
    Log_P: TPanel;
    VCS_Log_Frame1: TVCS_Log_Frame;
    Panel3: TPanel;
    File_Log_L: TLabel;
    Internal_Image_Viewer_TS: TTabSheet;
    Diff_Options_B: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Diff_BClick(Sender: TObject);
    procedure Diff3_BClick(Sender: TObject);
    procedure Diff_Options_BClick(Sender: TObject);
  private
    { Private declarations }
    f_VCS_Wrapper : T_VCS_Wrapper_Base;
    f_File_IDX : integer;

    fConflicted_File, fYour_File, fBase_Revision_File, fNew_Revision_File : string;
    f_Diff_Action_desc : T_Diff_Action_desc;

    procedure Set_Resolve_Decision( Arg_Resolve_Decision : type_Resolve_Decision );
    procedure Highlight_Revision_In_Log( Arg_Revision_id : t_revision_id_type );

    procedure Set_Diff_Enable;

    procedure do_Run_External_Diff( Arg_File1 , Arg_File2 : string; Arg_File3 : string = '' );

  public
    { Public declarations }
    f_Resolve_Decision : type_Resolve_Decision;
  end;

function Show_Resolver( Arg_VCS_Wrapper : T_VCS_Wrapper_Base;
                        Arg_File_IDX : integer;
                        Arg_Conflicted_File, Arg_Your_File, Arg_Base_Revision_File, Arg_New_Revision_File : string;
                        Arg_Base_Revision_id, Arg_New_Revision_id : t_revision_id_type ) : type_Resolve_Decision;

implementation

uses u_vcs_resolver_one_file_frame, u_vcs_Ini, u_vcs_diff_wrapper, U_Msg,
  vcs_diff_frame, vcs_view_image_const, vcs_options_form,
  u_vcs_diff_wrapper_previewer;

{$R *.dfm}

procedure Show_Options_Diff;
begin
  v_Show_Diff_Options_Event;
end;

function Show_Resolver;
var VCS_Resolver_f : TVCS_Resolver_f;
    Commits_Log_A : T_Commits_Log_A;
begin
  VCS_Resolver_f := TVCS_Resolver_f.Create( Application );
  with VCS_Resolver_f do
    begin
      f_VCS_Wrapper := Arg_VCS_Wrapper;
      f_File_IDX := Arg_File_IDX;

      Ini_Strore.Set_Font( Font );
      Ini_Strore.Set_Font( VCS_Log_Frame1.Font );

      PageControl1.ActivePage := Conflict_Info_TS;
      Internal_Image_Viewer_TS.Visible := False;
      Internal_Image_Viewer_TS.TabVisible := False;

      (* ���� Arg_Your_File ����, ������ ������ �� ����. *)
      if Arg_Your_File <> ''
        then
          begin
            Tvcs_resolver_one_file_frame.Create( Conflict_Info_TS, 'your', revision_unknown, 'Your file', Arg_Your_File, True, Set_Resolve_Decision, trd_Your, nil );
            Tvcs_resolver_one_file_frame.Create( Conflict_Info_TS, 'conflicted', revision_unknown, 'Conflicted file (was text merged)', Arg_Conflicted_File, False, Set_Resolve_Decision, trd_Conflicted_Merged, nil );
            fYour_File          := Arg_Your_File;
            fConflicted_File    := Arg_Conflicted_File;
          end
        else
          begin
            Tvcs_resolver_one_file_frame.Create( Conflict_Info_TS, 'conflicted', revision_unknown, 'Conflicted file (yours)', Arg_Conflicted_File, False, Set_Resolve_Decision, trd_Conflicted_Yours, nil );
            fYour_File          := Arg_Conflicted_File;
            fConflicted_File    := Arg_Conflicted_File;
          end;

      Tvcs_resolver_one_file_frame.Create( Conflict_Info_TS, 'their', Arg_New_Revision_id, 'Their file', Arg_New_Revision_File, True, Set_Resolve_Decision, trd_Their, Highlight_Revision_In_Log );
      fNew_Revision_File  := Arg_New_Revision_File;

      Tvcs_resolver_one_file_frame.Create( Conflict_Info_TS, 'base', Arg_Base_Revision_id, 'Base revision file', Arg_Base_Revision_File, True, Set_Resolve_Decision, trd_None, Highlight_Revision_In_Log );
      fBase_Revision_File := Arg_Base_Revision_File;

      SetLength( Commits_Log_A, 0 );
      if Arg_VCS_Wrapper.Get_VCS_History( Commits_Log_A, Arg_Conflicted_File )
        then
          begin
            File_Log_L.Caption := 'History of conflicted file: ' + Arg_Conflicted_File;
            VCS_Log_Frame1.Init_Data( Commits_Log_A, Arg_VCS_Wrapper );
            Log_P.Align := alClient;
          end
        else
          VCS_Log_Frame1.Visible := False;

      Set_Diff_Enable;

      if ShowModal = mrOk
        then
          Result := f_Resolve_Decision
        else
          Result := trd_Cancel;
    end;
  SetLength( Commits_Log_A, 0 );
end;

procedure TVCS_Resolver_f.Diff3_BClick(Sender: TObject);
begin
  if f_Diff_Action_desc.f_external_tool_path <> ''
    then
      do_Run_External_Diff( fYour_File, fNew_Revision_File, fBase_Revision_File );
end;

procedure TVCS_Resolver_f.Diff_BClick(Sender: TObject);

procedure Show_Internal_Image_Viewer( Arg_Image_Convert : T_Convertable_Formats = cf_None; Arg_Multipage_Formats : T_Multipage_Formats = mf_None );
var left_file, right_file : string;
begin
  if ( not Internal_Image_Viewer_TS.Visible ) and ( not Internal_Image_Viewer_TS.TabVisible )
    then
      begin
        left_file  := fNew_Revision_File;
        right_file := fConflicted_File;
        Protect_File( left_file, ExtractFileName( right_file ) );

        with TView_diff_frame.Create( Internal_Image_Viewer_TS, left_file, 'Their', right_file, 'Your', nil, Arg_Image_Convert, Arg_Multipage_Formats ) do
          begin
            if View_Mode <> vm_None
              then
                begin
                  Align := alClient;

                  Internal_Image_Viewer_TS.Visible := True;
                  Internal_Image_Viewer_TS.TabVisible := True;
                  PageControl1.ActivePage := Internal_Image_Viewer_TS;
                  (* ����� ������� - ����������� ��������� ����� diff *)
                  Diff_B.Enabled := False;
                end
              else
                begin
                  (* ��� ���� � ���������� diff *)
                  (* ���� ���������� �����, ��� ��� �� ����� ��������� �� ����� *)
                  Free;
                end;
          end;
       end;
end;

var Convertable_Formats : T_Convertable_Formats;
    Multipage_Formats : T_Multipage_Formats;
begin
  case Check_Diff_Viewer_Kind( fConflicted_File, Convertable_Formats, Multipage_Formats, f_Diff_Action_desc ) of
   dvk_MSWord : Run_MSWord_Compare( fYour_File, fNew_Revision_File );
   dvk_Multipage_Convert : Show_Internal_Image_Viewer( cf_None, Multipage_Formats );
   dvk_Internal_Image : Show_Internal_Image_Viewer;
   {$message warn '���� ��� ���� �������� � ���� ������ � Set_Diff_Enable'  }
   dvk_Internal_Image_Convert : Show_Internal_Image_Viewer( Convertable_Formats );
   dvk_External : do_Run_External_Diff( fYour_File, fNew_Revision_File );
   dvk_None : Prepare_No_Diff_MSG( fConflicted_File, f_VCS_Wrapper, f_File_IDX ); (* ������ �� ��������� *)
  end;
end;

procedure TVCS_Resolver_f.Diff_Options_BClick(Sender: TObject);
begin
  v_Show_Diff_Options_Event;
  Set_Diff_Enable;
end;

procedure TVCS_Resolver_f.do_Run_External_Diff;
begin
  case Run_External_Diff( f_Diff_Action_desc, Arg_File1, '', Arg_File2, '', Arg_File3 ) of
                      //rediff_Ok
                      rediff_NotConfigured : Show_No_Diff ; (* ��� �������������� �����������, ���� ��������� ������������ *)
                      rediff_NotFound : Show_Diff_Not_Found( f_Diff_Action_desc.f_external_tool_path );
                      rediff_NoDiff3 : ;
                      rediff_Fail : (* Show_Err( 'Fail to start ' + Diff_Action_desc.f_external_tool_path ) ��� ��� ���� ��������� *) ;
                    end;

end;

procedure TVCS_Resolver_f.FormClose;
begin
  Ini_Strore.Save_Form_Sizes( Self );
  Action := caFree;
end;

procedure TVCS_Resolver_f.FormCreate(Sender: TObject);
begin
  Ini_Strore.Load_Form_Sizes( Self );
end;

procedure TVCS_Resolver_f.Highlight_Revision_In_Log;
begin
  if Arg_Revision_id > revision_unknown
    then
      begin
        VCS_Log_Frame1.Highlight_Revision( Arg_Revision_id );
      end;
end;

procedure TVCS_Resolver_f.Set_Diff_Enable;
var flag_Diff_Enabled, flag_Diff3_Enabled, flag_Diff_Options_Visible : boolean;
    diff_options_caption : string;
    Convertable_Formats : T_Convertable_Formats;
    Multipage_Formats : T_Multipage_Formats;
begin
  flag_Diff_Enabled := False;
  flag_Diff3_Enabled := False;
  flag_Diff_Options_Visible := False;
  diff_options_caption := 'diff options'; (* � ������ ����� ��� �� ������� ��������� *)

  case Check_Diff_Viewer_Kind( fConflicted_File, Convertable_Formats, Multipage_Formats, f_Diff_Action_desc ) of
    dvk_MSWord : flag_Diff_Enabled := True;
    dvk_Multipage_Convert : flag_Diff_Enabled := True;
    dvk_Internal_Image : flag_Diff_Enabled := True;
    dvk_Internal_Image_Convert : flag_Diff_Enabled := True;
    dvk_External : begin
                     flag_Diff_Enabled := True;
                     if f_Diff_Action_desc.f_diff3_flag
                       then
                         begin
                           flag_Diff3_Enabled := True;
                         end
                       else
                         begin
                          diff_options_caption := cap( cap_No_Diff3_Tool_Configured ) + ' ' + cap( cap_Use_Options );
                          flag_Diff_Options_Visible := True;
                         end;
                   end;
    dvk_None : begin
                 flag_Diff_Enabled := True; (* ����� �������� ���������� ����� diff *)
                 diff_options_caption := cap( cap_No_Diff_Tool_Configured );
                 flag_Diff_Options_Visible := True;
               end;
   end;

  Diff_B.Enabled := flag_Diff_Enabled;
  Diff3_B.Enabled := flag_Diff3_Enabled;
  Diff_Options_B.Visible := flag_Diff_Options_Visible;
  if flag_Diff_Options_Visible
    then
      Diff_Options_B.Caption := diff_options_caption;
end;

procedure TVCS_Resolver_f.Set_Resolve_Decision;
begin
  f_Resolve_Decision := Arg_Resolve_Decision;
  ModalResult := mrOk;
end;

end.
