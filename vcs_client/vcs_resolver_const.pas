unit vcs_resolver_const;

interface

uses u_file_manager_VCS_const;

type type_Resolve_Decision = ( trd_None, trd_Cancel, trd_Your, trd_Their, trd_Conflicted_Yours, trd_Conflicted_Merged );

type T_Resolve_Decision_callback = procedure ( Arg_Resolve_Decision : type_Resolve_Decision ) of object;

type T_Highlight_Revision_In_Log_callback = procedure ( Arg_Revision_id : t_revision_id_type ) of object;

const Resolve_Decision_Params : array[ type_Resolve_Decision ] of
  record
    msg_warning : string

  end =
(
( msg_warning: ''),
( msg_warning: ''),
( msg_warning: 'Use your version of file?'),
( msg_warning: 'Using server version of file, you will discard all your local changes. Use their version?'),
( msg_warning: 'Use your version of file?' ),
( msg_warning: 'Do you resolve the conflict in merged text, and removes the merge marks in text?' )
);


(*
  ���������� - ������ ���� ��������� �� Tvcs_resolver_one_file_frame, �������� ������ � ���������������� (��������, "��������� �� ������� - �� ��������� ���� ��������� ���������")
����� � ���������� ���������� ��������
- ���������� ���� �������� � ��������� � diff, ��� ��� ������ ��� (��� ����������)
*)

implementation

end.
