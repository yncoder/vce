unit u_image_utils;

interface

function Prepare_Image_bins : boolean;

var Image_Convert_exe : string;

implementation

uses U_Const, SysUtils;

function Prepare_Image_bins;
begin
  (* ��������� ���� � ���������� ���������� ��������� ����������� *)
  Image_Convert_exe := Appl_Path + 'imagemagick\convert.exe';

  Result := FileExists( Image_Convert_exe );
end;

end.
