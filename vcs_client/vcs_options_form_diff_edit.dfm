object Option_Edit_Diff_F: TOption_Edit_Diff_F
  Left = 0
  Top = 0
  Caption = 'Edit external diff config'
  ClientHeight = 173
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MaxHeight = 200
  Constraints.MinHeight = 200
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    592
    173)
  PixelsPerInch = 96
  TextHeight = 13
  object File_Ext_L: TLabel
    Left = 24
    Top = 8
    Width = 202
    Height = 13
    Caption = 'File extension (without dots, any register)'
  end
  object External_Diff_Path_L: TLabel
    Left = 24
    Top = 69
    Width = 84
    Height = 13
    Caption = 'External diff path'
  end
  object Check_Extension_L: TLabel
    Left = 168
    Top = 30
    Width = 6
    Height = 13
    Caption = '*'
  end
  object File_Ext_E: TEdit
    Left = 24
    Top = 27
    Width = 121
    Height = 21
    TabOrder = 0
    OnChange = File_Ext_EChange
  end
  object Ok_B: TButton
    Left = 208
    Top = 128
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 5
  end
  object Cancel_B: TButton
    Left = 328
    Top = 128
    Width = 123
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object Select_B: TButton
    Left = 314
    Top = 86
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Select'
    TabOrder = 2
    OnClick = Select_BClick
  end
  object Clear_B: TButton
    Left = 395
    Top = 86
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Clear'
    TabOrder = 3
    OnClick = Clear_BClick
  end
  object Test_Text_B: TButton
    Left = 476
    Top = 86
    Width = 105
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Test with text'
    TabOrder = 4
    OnClick = Test_Text_BClick
  end
  object External_Diff_Path_CB: TComboBox
    Left = 24
    Top = 88
    Width = 275
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 1
    OnChange = External_Diff_Path_CBChange
  end
end
