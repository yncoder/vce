object View_diff_frame: TView_diff_frame
  Left = 0
  Top = 0
  Width = 1074
  Height = 708
  TabOrder = 0
  OnResize = FrameResize
  object GridPanel1: TGridPanel
    Left = 0
    Top = 25
    Width = 1074
    Height = 683
    Align = alClient
    ColumnCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = View_Image_Frame1
        Row = 0
      end
      item
        Column = 1
        Control = View_Image_Frame2
        Row = 0
      end
      item
        Column = 0
        Control = View_Image_Frame3
        Row = 1
      end>
    RowCollection = <
      item
        Value = 54.545454545454540000
      end
      item
        Value = 45.454545454545450000
      end>
    TabOrder = 0
    inline View_Image_Frame1: TView_Image_Frame
      Left = 1
      Top = 1
      Width = 536
      Height = 371
      Align = alClient
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 536
    end
    inline View_Image_Frame2: TView_Image_Frame
      Left = 537
      Top = 1
      Width = 536
      Height = 371
      Align = alClient
      TabOrder = 1
      ExplicitLeft = 537
      ExplicitTop = 1
      ExplicitWidth = 536
    end
    inline View_Image_Frame3: TView_Image_Frame
      Left = 1
      Top = 372
      Width = 536
      Height = 310
      Align = alClient
      TabOrder = 2
      Visible = False
      ExplicitLeft = 1
      ExplicitTop = 372
      ExplicitWidth = 536
      ExplicitHeight = 310
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1074
    Height = 25
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 127
    Caption = 'ToolBar1'
    EdgeBorders = [ebTop]
    Flat = False
    ShowCaptions = True
    TabOrder = 1
    object Fit_ToolButton: TToolButton
      Left = 0
      Top = 2
      Action = Fit_A
      AutoSize = True
    end
    object ToolButton3: TToolButton
      Left = 75
      Top = 2
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object Compare_ToolButton: TToolButton
      Left = 83
      Top = 2
      Action = Compare_A
      AutoSize = True
    end
    object Trim_TB: TToolButton
      Left = 150
      Top = 2
      Action = Trim_Compared_A
    end
    object Shave_E: TSpinEdit
      Left = 277
      Top = 2
      Width = 52
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 15
      OnExit = Shave_EExit
    end
    object ToolButton1: TToolButton
      Left = 329
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 337
      Top = 2
      Action = Sync_Pages_A
      AutoSize = True
    end
    object ToolButton4: TToolButton
      Left = 403
      Top = 2
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 411
      Top = 2
      Action = Execute_Compared_Img_A
    end
    object ToolButton6: TToolButton
      Left = 538
      Top = 2
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 546
      Top = 2
      Action = Export_Compared_Img_A
    end
    object ToolButton8: TToolButton
      Left = 673
      Top = 2
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 681
      Top = 2
      Action = About_A
    end
    object ToolButton11: TToolButton
      Left = 808
      Top = 2
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 816
      Top = 2
      Action = Close_A
    end
  end
  object ActionList1: TActionList
    Left = 544
    Top = 240
    object Fit_A: TAction
      Caption = 'Fit to window'
      OnExecute = Fit_AExecute
    end
    object Compare_A: TAction
      Caption = 'Compare_A'
      OnExecute = Compare_AExecute
    end
    object Sync_Pages_A: TAction
      Caption = 'Sync pages'
      Checked = True
      OnExecute = Sync_Pages_AExecute
    end
    object Export_Compared_Img_A: TAction
      Caption = 'Export compared image'
      OnExecute = Export_Compared_Img_AExecute
    end
    object Execute_Compared_Img_A: TAction
      Caption = 'Execute compared image'
      OnExecute = Execute_Compared_Img_AExecute
    end
    object About_A: TAction
      Caption = 'About'
      OnExecute = About_AExecute
    end
    object Close_A: TAction
      Caption = 'Close'
      OnExecute = Close_AExecute
    end
    object Trim_Compared_A: TAction
      Caption = 'Trim compared'
      OnExecute = Trim_Compared_AExecute
    end
  end
  object SaveDialog1: TSaveDialog
    FilterIndex = 0
    Left = 624
    Top = 72
  end
end
