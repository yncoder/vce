unit vcs_options_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  u_common_forms, StdCtrls, ExtCtrls, Spin, ComCtrls;

type
  TOptions_F = class(T_My_Form)
    Panel2: TPanel;
    Save_B: TButton;
    Close_B: TButton;
    PageControl1: TPageControl;
    Common_TabSheet: TTabSheet;
    Diff_TabSheet: TTabSheet;
    Information_TSh: TTabSheet;
    Work_Copy_Root_GroupBox: TGroupBox;
    Work_Copy_Root_L: TLabel;
    Work_Copy_Root_Description_L: TLabel;
    Work_Copy_Root_Check_L: TLabel;
    Work_Copy_Root_E: TEdit;
    Select_Work_Copy_Root_B: TButton;
    Proxy_GroupBox: TGroupBox;
    Proxy_Host_L: TLabel;
    Proxy_port_L: TLabel;
    Proxy_Host_E: TEdit;
    Proxy_port_E: TSpinEdit;
    Monitor_repositories_GB: TGroupBox;
    Label1: TLabel;
    Label111: TLabel;
    Monitor_repositories_interval_E: TSpinEdit;
    Show_master_on_start_up_GB: TGroupBox;
    Show_master_on_start_up_CB: TCheckBox;
    Diff_GB: TGroupBox;
    Ini_Path_L: TLabel;
    Ini_Path_E: TEdit;
    TempFile_Path_RadioGroup: TRadioGroup;
    Commit_MSG_GB: TGroupBox;
    Default_Commit_MSG_E: TEdit;
    Log_Path_L: TLabel;
    Log_Path_E: TEdit;
    Ini_Path_B: TButton;
    Log_Path_B: TButton;
    Checkout_In_Exe_Folder_CB: TCheckBox;
    Clear_Work_Copy_Root_B: TButton;
    View_TSh: TTabSheet;
    ToolBar_Type_RG: TRadioGroup;
    Catalog_TS: TTabSheet;
    Catalog_URL_GB: TGroupBox;
    Catalog_URL_E: TEdit;
    Catalog_URL_Check_L: TLabel;
    Ext_Diff_LV: TListView;
    Default_Diff_GB: TGroupBox;
    Panel1: TPanel;
    Add_Diff_Tool_B: TButton;
    Edit_Diff_Tool_B: TButton;
    Del_Diff_Tool_B: TButton;
    Default_Diff_Exe_E: TEdit;
    Default_Diff_Exe_Select_B: TButton;
    Default_Diff_Exe_Clear_B: TButton;
    Default_Diff_Exe_Test_B: TButton;
    DIff3_CB: TCheckBox;
    Diff_GB_Info_L: TLabel;
    Font_GB: TGroupBox;
    Select_Font_B: TButton;
    FontDialog1: TFontDialog;
    Font_Example_M: TMemo;
    procedure FormDestroy(Sender: TObject);
    procedure Select_Work_Copy_Root_BClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Close_BClick(Sender: TObject);
    procedure Save_BClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Proxy_Host_EChange(Sender: TObject);
    procedure Proxy_port_EChange(Sender: TObject);
    procedure Proxy_port_EExit(Sender: TObject);
    procedure Monitor_repositories_interval_EChange(Sender: TObject);
    procedure Monitor_repositories_interval_EExit(Sender: TObject);
    procedure Default_Diff_Exe_Select_BClick(Sender: TObject);
    procedure Default_Diff_Exe_Clear_BClick(Sender: TObject);
    procedure Default_Diff_Exe_Test_BClick(Sender: TObject);
    procedure Default_Diff_Exe_EChange(Sender: TObject);
    procedure DIff3_CBClick(Sender: TObject);
    procedure Show_master_on_start_up_CBClick(Sender: TObject);
    procedure TempFile_Path_RadioGroupClick(Sender: TObject);
    procedure Default_Commit_MSG_EChange(Sender: TObject);
    procedure Ini_Path_BClick(Sender: TObject);
    procedure Log_Path_BClick(Sender: TObject);
    procedure Checkout_In_Exe_Folder_CBClick(Sender: TObject);
    procedure Clear_Work_Copy_Root_BClick(Sender: TObject);
    procedure ToolBar_Type_RGClick(Sender: TObject);
    procedure Catalog_URL_EChange(Sender: TObject);
    procedure Ext_Diff_LVDblClick(Sender: TObject);
    procedure Add_Diff_Tool_BClick(Sender: TObject);
    procedure Ext_Diff_LVChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure Del_Diff_Tool_BClick(Sender: TObject);
    procedure Select_Font_BClick(Sender: TObject);
    procedure FontDialog1Show(Sender: TObject);
  private
    { Private declarations }
    fModified : boolean;
    procedure Set_Modified( Arg_Value : boolean );
    property Modified : boolean read fModified write Set_Modified;
    procedure Save_Options;
    procedure Check_Work_Copy_Root;
    procedure Set_DiffExe_Commands_Enabled;
    procedure Set_Work_Copy_Root_Commands_Enabled;
    procedure Fill_Diff_List;
    procedure Set_Edit_Diff_Tool_Enabled;
    procedure Show_Font_Example;
  public
    { Public declarations }
  end;

type T_Options_Default_Page = ( odp_Common, odp_Diff );

procedure Show_Options( Arg_Options_Default_Page : T_Options_Default_Page );

implementation

uses Dlgs, U_ShellOp, U_Msg, u_vcs_Ini, u_vcs_diff_wrapper, U_Const,
  vcs_client_main, u_web_utils, vcs_options_form_diff_edit,
  u_vcs_diff_options_const;

{$R *.dfm}

var Options_F : TOptions_F;

procedure Show_Options;
begin
  if not Assigned( Options_F )
    then
      begin
         Options_F := TOptions_F.Create( Application );
         Ini_Strore.Load_Form_Sizes( Options_F );
         with Options_F do
           case Arg_Options_Default_Page of
             odp_Common : PageControl1.ActivePage := Common_TabSheet;
             odp_Diff   : PageControl1.ActivePage := Diff_TabSheet;
             else PageControl1.ActivePage := Common_TabSheet;
            end;
         Options_F.ShowModal;
      end
    else
       Options_F.FocusIt;  
end;

procedure TOptions_F.Default_Commit_MSG_EChange(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.Default_Diff_Exe_Clear_BClick(Sender: TObject);
begin
  Default_Diff_Exe_E.Clear;
  DIff3_CB.Checked := False;
  Modified := True;
end;

procedure TOptions_F.Default_Diff_Exe_EChange(Sender: TObject);
begin
  Set_DiffExe_Commands_Enabled;
end;

procedure TOptions_F.Default_Diff_Exe_Select_BClick(Sender: TObject);
var s : string;
begin
  s := Select_Util_OpenDialog( Default_Diff_Exe_E.Text );
  if s <> ''
    then
      begin
        Default_Diff_Exe_E.Text := s;
        Modified := True;
      end;
end;

procedure TOptions_F.Default_Diff_Exe_Test_BClick(Sender: TObject);
begin
  if Default_Diff_Exe_E.Text <> ''
    then
      Test_External_Diff( Default_Diff_Exe_E.Text );
end;

procedure TOptions_F.Del_Diff_Tool_BClick(Sender: TObject);
var i : integer;
    sel : TListItem;
begin
  sel := Ext_Diff_LV.Selected;

  if not Assigned( sel )
    then
      Exit;

  i := integer( sel.Data );

  if ( i >= 0 ) and ( Show_Q( cap( cap_Delete_Diff_Tool ) + ret + sel.Caption, False ) )
    then
      begin
        (* ������� *)
        Ini_Strore.Diff_Action_arr_Del_And_Save( i );

        Fill_Diff_List;
      end;
end;

procedure TOptions_F.DIff3_CBClick(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.Ext_Diff_LVChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  Set_Edit_Diff_Tool_Enabled;
end;

procedure TOptions_F.Ext_Diff_LVDblClick(Sender: TObject);
var i : integer;
    Diff_Action_desc : T_Diff_Action_desc;
    sel : TListItem;
begin
  sel := Ext_Diff_LV.Selected;

  if not Assigned( sel )
    then
      Exit;

  i := integer( sel.Data );

  if i >= 0
    then
      begin
        Diff_Action_desc := Ini_Strore.f_Diff_Action_arr[i];

        if Show_Diff_Option_Edit( Diff_Action_desc, Ini_Strore.Get_Known_Diff_Tools, Ini_Strore.Get_Known_Diff_Ext )
          then
            begin
              Ini_Strore.f_Diff_Action_arr[i] := Diff_Action_desc;
              Ini_Strore.Diff_Action_arr_Save;

              Fill_Diff_List;
            end;
      end;
end;

procedure TOptions_F.Add_Diff_Tool_BClick(Sender: TObject);
var i : integer;
    Diff_Action_desc : T_Diff_Action_desc;
begin
  if Show_Diff_Option_Edit( Diff_Action_desc, Ini_Strore.Get_Known_Diff_Tools, Ini_Strore.Get_Known_Diff_Ext )
    then
      begin
        i := length( Ini_Strore.f_Diff_Action_arr );
        SetLength( Ini_Strore.f_Diff_Action_arr, i + 1 );
        Ini_Strore.f_Diff_Action_arr[i] := Diff_Action_desc;
        Ini_Strore.Diff_Action_arr_Save;

        Fill_Diff_List;
      end;
end;

procedure TOptions_F.Catalog_URL_EChange(Sender: TObject);
var url : string;
begin
  url := Catalog_URL_E.Text;
  if ( url = '' ) or ( Check_URL( url ) )
    then
      begin
        Modified := True;
        Catalog_URL_Check_L.Caption := '*'; 
      end
    else
      begin
        Modified := False; (* �� ���� ��������� �������� ��� *)
        Catalog_URL_Check_L.Caption := cap( cap_Wrong_Catalog_URL );
      end;
end;

procedure TOptions_F.Checkout_In_Exe_Folder_CBClick(Sender: TObject);
begin
  if Checkout_In_Exe_Folder_CB.Checked
    then
      begin
        {$message warn '��� �� ���������' }

        if Test_Appl_Path_Data_Folder_Write_Access = ''
          then
            with Checkout_In_Exe_Folder_CB do
              begin
                Checked := False;
                Enabled := False;
            end;
      end;

  Modified := True;
  Set_Work_Copy_Root_Commands_Enabled;
  Check_Work_Copy_Root;
end;

procedure TOptions_F.Check_Work_Copy_Root;
var s : string;
    msg : string;
begin
  (* ��������� ���� � ����� ��� ������� *)
  msg := '';

  if Checkout_In_Exe_Folder_CB.Checked
    then
      begin
        msg := cap( cap_Work_Checkout_Exe_Data_Folder );
      end
    else
      begin
        s := Work_Copy_Root_E.Text;
        if s = ''
          then
            msg := cap( cap_Work_Checkout_Default_Folder )
          else
            begin
              if Drive_Exist( s )
                then
                else
                  msg := cap( cap_Error_Drive_doesnt_exist );
            end;
      end;

  Work_Copy_Root_Check_L.Caption := msg;  
end;

procedure TOptions_F.Clear_Work_Copy_Root_BClick(Sender: TObject);
begin
  if Work_Copy_Root_E.Text <> ''
    then
      begin
        Work_Copy_Root_E.Clear;
        Modified := True;
        Check_Work_Copy_Root;
        Set_Work_Copy_Root_Commands_Enabled;
      end;
end;

procedure TOptions_F.Close_BClick(Sender: TObject);
begin
  Close;
end;

procedure TOptions_F.Fill_Diff_List;
var i : integer;
begin
  Ext_Diff_LV.Items.BeginUpdate;
  Ext_Diff_LV.Items.Clear;

  for i := 0 to high( Ini_Strore.f_Diff_Action_arr ) do
    begin
      with Ext_Diff_LV.Items.Add do
        begin
          Caption := Ini_Strore.f_Diff_Action_arr[i].f_extention;
          Data := pointer( i );
          SubItems.Add( Ini_Strore.f_Diff_Action_arr[i].f_external_tool_path );
        end;
    end;

  Set_Edit_Diff_Tool_Enabled;
  
  Ext_Diff_LV.Items.EndUpdate;
end;

procedure TOptions_F.FontDialog1Show(Sender: TObject);
begin
  (* http://stackoverflow.com/questions/37281815/how-to-disable-a-style-list-in-tfontdialog *)
  EnableWindow( GetDlgItem( FontDialog1.Handle, cmb2 ), False);
  EnableWindow( GetDlgItem( FontDialog1.Handle, cmb5 ), False);

//  ShowWindow(GetDlgItem(FontDialog1.Handle, cmb2), SW_HIDE);
//  ShowWindow(GetDlgItem(FontDialog1.Handle, cmb5), SW_HIDE);

end;

procedure TOptions_F.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Modified
    then
      begin
        case Show_YNC( 'Save options before exit?' ) of
          id_Yes : begin Save_Options; Action := caFree; end;
          ID_NO : Action := caFree;
          ID_CANCEL : Action := caNone;
         end;
      end
    else
      Action := caFree;

  if Action = caFree
    then
      Ini_Strore.Save_Form_Sizes( Self );     
end;

procedure TOptions_F.FormCreate(Sender: TObject);
var log_path : string;
begin
  Work_Copy_Root_L.Caption := cap( cap_Select_Work_Checkout_Default_Folder );
  Work_Copy_Root_Description_L.Caption := cap( cap_Select_Work_Checkout_Default_Folder_Description );

  Work_Copy_Root_E.Text := Ini_Strore.Work_Checkouts_Root;
  Checkout_In_Exe_Folder_CB.Checked := Ini_Strore.Boo[ ii_Work_Checkouts_ExeData_Root ];
  Set_Work_Copy_Root_Commands_Enabled;

  Proxy_Host_E.Text := Ini_Strore.HTTP_Proxy_Host;
  Proxy_port_E.Value := Ini_Strore.HTTP_Proxy_Port;

  Monitor_repositories_interval_E.MinValue := Monitor_Interval_Minimum;
  Monitor_repositories_interval_E.MaxValue := Monitor_Interval_Maximum;

  Monitor_repositories_interval_E.Value := Ini_Strore.Monitor_Interval;

  Default_Diff_Exe_E.Text := Ini_Strore.Str[ ii_External_Diff_Exe ];
  Diff3_CB.Checked        := Ini_Strore.Boo[ ii_External_Diff3_Flag ];

  if Ini_Strore.Temp_Folder_Flag
    then
      TempFile_Path_RadioGroup.ItemIndex := 0
    else
      TempFile_Path_RadioGroup.ItemIndex := 1;

  ToolBar_Type_RG.ItemIndex := integer( Ini_Strore.Get_ToolBar_Type );

  Default_Commit_MSG_E.Text := Ini_Strore.Str[ ii_Default_Commit_MSG ];

  (* ���������� �������-������ *)
  Show_master_on_start_up_CB.Checked := not Ini_Strore.Boo[ ii_DoNot_Show_StartUp ];

  Catalog_URL_E.Text := Ini_Strore.Str[ ii_Catalog_URL ];

  Set_DiffExe_Commands_Enabled;

  Modified := False;
  Check_Work_Copy_Root;

  Ini_Path_E.Text := Ini_Strore.Get_Ini_Path;
  log_path := Get_Log_FN;
  if FileExists( log_path )
    then
      begin
        Log_Path_E.Text := log_path;
        Log_Path_B.Enabled := True;
      end
    else
      begin
        Log_Path_E.Text := '';
        Log_Path_B.Enabled := False;
      end;

  Ini_Strore.Set_Font( Font_Example_M.Font );
  Show_Font_Example;

  Fill_Diff_List;
end;

procedure TOptions_F.FormDestroy(Sender: TObject);
begin
  Options_F := nil;
end;

procedure TOptions_F.Ini_Path_BClick(Sender: TObject);
begin
  Show_in_Explorer( Ini_Path_E.Text );
end;

procedure TOptions_F.Log_Path_BClick(Sender: TObject);
begin
  if Log_Path_E.Text <> ''
    then
      Show_in_Explorer( Log_Path_E.Text );
end;

procedure TOptions_F.Monitor_repositories_interval_EChange(
  Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.Monitor_repositories_interval_EExit(Sender: TObject);
begin
  if Monitor_repositories_interval_E.Text = '' then Monitor_repositories_interval_E.Value := 0;
end;

procedure TOptions_F.Proxy_Host_EChange(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.Proxy_port_EChange(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.Proxy_port_EExit(Sender: TObject);
begin
  if Proxy_port_E.Text = '' then Proxy_port_E.Value := 0;
end;

procedure TOptions_F.Save_BClick(Sender: TObject);
begin
  Save_Options;
end;

procedure TOptions_F.Save_Options;
var port, monitor_interval : integer;
    catalog_url : string;
begin
  if Modified
    then
      begin
        if Checkout_In_Exe_Folder_CB.Checked <> Ini_Strore.Boo[ ii_Work_Checkouts_ExeData_Root ]
          then
            Ini_Strore.Boo[ ii_Work_Checkouts_ExeData_Root ] := Checkout_In_Exe_Folder_CB.Checked;

        if Work_Copy_Root_E.Text <> Ini_Strore.Work_Checkouts_Root
          then
            Ini_Strore.Work_Checkouts_Root := Work_Copy_Root_E.Text;

        if Proxy_Host_E.Text <> Ini_Strore.HTTP_Proxy_Host
          then
            Ini_Strore.HTTP_Proxy_Host := Proxy_Host_E.Text;

        port := 0;
        try
          if Proxy_Port_E.Text <> ''
            then
              port := Proxy_Port_E.Value;
        except
        end;

        if port <> Ini_Strore.HTTP_Proxy_Port
          then
            Ini_Strore.HTTP_Proxy_Port := port;

        monitor_interval := 0;
        try
          if Monitor_repositories_interval_E.Text <> ''
            then
              monitor_interval := Monitor_repositories_interval_E.Value;
        except
        end;
        if monitor_interval <> Ini_Strore.Monitor_Interval
          then
            begin
              Ini_Strore.Monitor_Interval := monitor_interval;
              (* �������� ��������� ������ *)
              VCS_Client_Project_List_F.Set_Timer;
            end;

        if Default_Diff_Exe_E.Text <> Ini_Strore.Str[ ii_External_Diff_Exe ]
          then
            Ini_Strore.Str[ ii_External_Diff_Exe ] := Default_Diff_Exe_E.Text;

        if Diff3_CB.Checked <> Ini_Strore.Boo[ ii_External_Diff3_Flag ]
          then
            Ini_Strore.Boo[ ii_External_Diff3_Flag ] := Diff3_CB.Checked;

        (* temp ����� *)
        if ( TempFile_Path_RadioGroup.ItemIndex = 1 ) and Ini_Strore.Temp_Folder_Flag
          then
            Ini_Strore.Temp_Folder_Flag := False
          else
            begin
              if ( TempFile_Path_RadioGroup.ItemIndex = 0 ) and not Ini_Strore.Temp_Folder_Flag
                then
                  Ini_Strore.Temp_Folder_Flag := True;
            end;

        if ToolBar_Type_RG.ItemIndex <> integer( Ini_Strore.Get_ToolBar_Type )
          then
            begin
              Ini_Strore.Int[ ii_ToolBar_Type ] := ToolBar_Type_RG.ItemIndex;
              VCS_Client_Project_List_F.Refresh_Toolbars;
            end;

        (* ������ �������� - �.�. ���� � ��� ����� �������� - DoNot_Show_StartUp *)
        if Show_master_on_start_up_CB.Checked = Ini_Strore.Boo[ ii_DoNot_Show_StartUp ]
          then
            Ini_Strore.Boo[ ii_DoNot_Show_StartUp ] := not Show_master_on_start_up_CB.Checked;

        (* ����������� ��������� ��� �������� *)
        if Default_Commit_MSG_E.Text <> Ini_Strore.Str[ ii_Default_Commit_MSG ]
          then
            Ini_Strore.Str[ ii_Default_Commit_MSG ] := Default_Commit_MSG_E.Text;

        catalog_url := Catalog_URL_E.Text;
        (* ���� � �������� *)
        if ( catalog_url <> Ini_Strore.Str[ ii_Catalog_URL ] )
            and
           ( ( catalog_url = '' ) or Check_URL( catalog_url ) )
          then
            begin
              Ini_Strore.Str[ ii_Catalog_URL ] := Catalog_URL_E.Text;
              (* ���������� ������� *)
              VCS_Client_Project_List_F.Set_Catalog( True );
            end;

        if Ini_Strore.Check_Font_Modified( Font_Example_M.Font )
          then
            begin
              Ini_Strore.Save_Font( Font_Example_M.Font );
              VCS_Client_Project_List_F.Refresh_Fonts;
            end;

        Modified := False;
      end;
end;

procedure TOptions_F.Select_Font_BClick(Sender: TObject);
begin
  FontDialog1.Font := Font_Example_M.Font;
  if FontDialog1.Execute
    then
      begin
        Font_Example_M.Font := FontDialog1.Font;
        Show_Font_Example;
        Modified := True;
      end;
end;

procedure TOptions_F.Select_Work_Copy_Root_BClick(Sender: TObject);
var Directory: String;
begin
  if Select_Directory( cap_Select_Work_Checkout_Default_Folder,
                   Work_Copy_Root_E.Text,
                   false,
                   Directory)
    then
      begin
        if Work_Copy_Root_E.Text <> Directory
          then
            begin
              Work_Copy_Root_E.Text := Directory;
              Modified := True;
              Check_Work_Copy_Root;
            end;
      end;
end;

procedure TOptions_F.Set_DiffExe_Commands_Enabled;
var en : boolean;
begin
  en := Default_Diff_Exe_E.Text <> '';
  Default_Diff_Exe_Clear_B.Enabled := en;
  Default_Diff_Exe_Test_B.Enabled := en;
  Diff3_CB.Enabled := en;
end;

procedure TOptions_F.Set_Edit_Diff_Tool_Enabled;
var e : boolean;
begin
  e := Ext_Diff_LV.SelCount > 0;
  Edit_Diff_Tool_B.Enabled := e;
  Del_Diff_Tool_B.Enabled := e;
end;

procedure TOptions_F.Set_Modified(Arg_Value: boolean);
begin
  fModified := Arg_Value;
  Save_B.Enabled := fModified;
end;

procedure TOptions_F.Set_Work_Copy_Root_Commands_Enabled;
begin
  Select_Work_Copy_Root_B.Enabled      := not Checkout_In_Exe_Folder_CB.Checked;
  Work_Copy_Root_E.Enabled             := not Checkout_In_Exe_Folder_CB.Checked;
  Clear_Work_Copy_Root_B.Enabled       := ( not Checkout_In_Exe_Folder_CB.Checked ) and ( Work_Copy_Root_E.Text <> '' );
  Work_Copy_Root_Description_L.Enabled := not Checkout_In_Exe_Folder_CB.Checked;
end;

procedure TOptions_F.Show_Font_Example;
begin
  Font_Example_M.Lines.Text := Font_Example_M.Font.Name + ret + IntToStr( Font_Example_M.Font.Size );
end;

procedure TOptions_F.Show_master_on_start_up_CBClick(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.TempFile_Path_RadioGroupClick(Sender: TObject);
begin
  Modified := True;
end;

procedure TOptions_F.ToolBar_Type_RGClick(Sender: TObject);
begin
  Modified := True;
end;

end.
