unit vcs_client_main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  U_API_Wrapper_Const, ActnList, XPMan, ActnMan, ToolWin,
  u_vcs_repo_catalog;

type
  TVCS_Client_Project_List_F = class(TForm)
    ActionList1: TActionList;
    Add_Project_A: TAction;
    View_Project_A: TAction;
    Close_A: TAction;
    Options_A: TAction;
    Timer1: TTimer;
    Create_Demo_Repo_And_Add_A: TAction;
    Connect2demo_github_A: TAction;
    XPManifest1: TXPManifest;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    About_A: TAction;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    Projects_PC: TPageControl;
    My_Projects_TS: TTabSheet;
    Catalog_TS: TTabSheet;
    Projects_LV: TListView;
    Catalog_LV: TListView;
    Filter_E: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Add_Project_AExecute(Sender: TObject);
    procedure View_Project_AExecute(Sender: TObject);
    procedure Close_AExecute(Sender: TObject);
    procedure Options_AExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Create_Demo_Repo_And_Add_AExecute(Sender: TObject);
    procedure Connect2demo_github_AExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Projects_LVChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure About_AExecute(Sender: TObject);
    procedure Filter_EChange(Sender: TObject);
    procedure Projects_PCChange(Sender: TObject);
    procedure Catalog_LVDblClick(Sender: TObject);
  private
    { Private declarations }
    fList_Updating_Flag : boolean;
    fProjects_List : T_Projects_List;
    fFirst_Show : boolean;
    (* ������ �������� *)
    f_Catalog : T_Catalog;
    procedure do_Refresh_List;
    procedure do_View_Project( Arg_Project_Rec : T_Project_Record );
    procedure Open_Previous_Projects;
    procedure Open_Project( Arg_ID : integer );
    procedure Set_Enabled;
    procedure Open_Or_Add_URL( Arg_URL : string );
    procedure Set_Font;
  public
    { Public declarations }
    procedure Set_Timer;
    procedure Set_Catalog( Arg_Refresh : boolean );
    procedure Refresh_Toolbars;
    procedure Refresh_Fonts;
  end;

var
  VCS_Client_Project_List_F: TVCS_Client_Project_List_F;

implementation

uses U_ProjectView, u_vcs_Ini, vcs_options_form, u_vcs_about,
  u_vcs_start_up_master_f, U_URLs, u_vcs_portable_const, U_ShellOp,
  u_vcs_ico_const, u_vcs_diff_wrapper;

{$R *.dfm}

{$i test.inc}

procedure Show_Options_Diff;
begin
  Show_Options( odp_Diff );
end;

procedure TVCS_Client_Project_List_F.Add_Project_AExecute(Sender: TObject);
var i : integer;
    s : string;
begin
  if Projects_PC.ActivePageIndex = 0
    then
      Add_New_Project( fProjects_List, do_Refresh_List )
    else
      begin
        i := Catalog_LV.ItemIndex;
        if i >= 0
          then
            begin
              s := f_Catalog.Get_Server_URL( integer( Catalog_LV.Items[i].Data ) );
              Add_New_Project( fProjects_List, do_Refresh_List, s );
            end
          else
            Add_New_Project( fProjects_List, do_Refresh_List );
      end;
end;

procedure TVCS_Client_Project_List_F.About_AExecute(Sender: TObject);
begin
  Run_About_Singleton;
end;

procedure TVCS_Client_Project_List_F.Catalog_LVDblClick(Sender: TObject);
var i : integer;
    s : string;
    data : pointer;
begin
  i := Catalog_LV.ItemIndex;
  if ( i >= 0 )
    then
      begin
        data := Catalog_LV.Items[i].Data;
        s := f_Catalog.Get_Server_URL( integer( Data ) ); (* ��������� Assigned(Data) ������������, �.�. 0 ��� � ���� Nil *)
        if s <> ''
          then
            Open_Or_Add_URL( s );
      end;
end;

procedure TVCS_Client_Project_List_F.Close_AExecute(Sender: TObject);
begin
  Close;
end;

procedure TVCS_Client_Project_List_F.Connect2demo_github_AExecute;
begin
  Open_Or_Add_URL( url_github_demo );
end;

procedure TVCS_Client_Project_List_F.Create_Demo_Repo_And_Add_AExecute(
  Sender: TObject);
begin
  {$message '��� ������������ ?  ������ � ����� ����� � ��������� ��� ��� ���� svn admin ? ����� ����� �� ����� ������������ '}
end;

procedure TVCS_Client_Project_List_F.Filter_EChange(Sender: TObject);
begin
  do_Refresh_List;
end;

procedure TVCS_Client_Project_List_F.FormActivate(Sender: TObject);
begin
  if fFirst_Show
    then
      begin
        fFirst_Show := False;
        if not Ini_Strore.Boo[ ii_DoNot_Show_StartUp ]
          then
            Show_StartUp_Master( Add_Project_A, Options_A, Connect2demo_github_A );

        Open_Previous_Projects;
      end;
end;

procedure TVCS_Client_Project_List_F.FormClose(Sender: TObject;
  var Action: TCloseAction);
var project_id, i : integer;
    Project_Form : TProject_Form; 
begin
  Ini_Strore.Del_Section( is_Open_Projects );

  for i := 0 to Screen.FormCount - 1 do
    begin
      if Screen.Forms[ i ] is TProject_Form
        then
          begin
            Project_Form := ( Screen.Forms[ i ] as TProject_Form );
            project_id := Project_Form.Project_ID;
            if project_id > 0
              then
                begin
                  Ini_Strore.Save_Project_Opened( project_id );
                  Project_Form.Prepare_Close;
                end;
          end;
    end;

  Ini_Strore.Save_Form_Sizes( Self );
end;

procedure TVCS_Client_Project_List_F.FormCreate(Sender: TObject);
var i : integer;
begin
  {$ifdef testing_msg}showmessage( 'This is a test version!' );{$endif}

  if Portable_Mode
    then
      Caption := Caption + ' (portable mode)';

  fFirst_Show := True;
  Set_Font;

  Ini_Strore.Load_Form_Sizes( Self );

  fProjects_List := T_Projects_List.Create;
  fProjects_List.Load;

  fList_Updating_Flag := False;

  if Interface_Image_List_Done
    then
      begin
        Options_A.ImageIndex := integer( iii_Options );
        About_A.ImageIndex := integer( iii_About );
        Add_Project_A.ImageIndex := integer( iii_Add_New_Checkout );
        View_Project_A.ImageIndex := integer( iii_Open_Checkout );
        Close_A.ImageIndex := integer( iii_Close );
        //ActionList1.Images := Ini_Strore.Interface_Image_List;
      end;

  Set_ToolBar( ToolBar1 );

  (* ���������� ����� ��� ������ *)
  for i := 0 to ActionList1.ActionCount - 1 do
    with ( ActionList1.Actions[i] as TAction ) do
      Hint := Caption;

  f_Catalog := T_Catalog.Create;

  Set_Catalog( False );

  do_Refresh_List;

  Set_Timer;

  Projects_PC.ActivePageIndex := 0;

  v_Show_Diff_Options_Event := Show_Options_Diff;
end;

procedure TVCS_Client_Project_List_F.FormDestroy(Sender: TObject);
begin
  f_Catalog.Free;

  fProjects_List.Free;

  (* ��������� ��������� ����� *)
  {$ifndef skip_temp_del} Clear_Temp_Folder; {$endif}
end;

procedure TVCS_Client_Project_List_F.Open_Or_Add_URL(Arg_URL: string);
var i : integer;
begin
  (* ���������, �����, ��� ���� *)
  i := fProjects_List.Get_Project_ID_By_Server_Url( Arg_URL );
  if i = -1
    then
      Add_New_Project( fProjects_List, do_Refresh_List, Arg_URL )
    else
      Open_Project( i );
end;

procedure TVCS_Client_Project_List_F.Open_Previous_Projects;
var i : Integer;
    Project_Rec : T_Project_Record;
begin
  for i := 0 to fProjects_List.Project_Array_Count - 1 do
    begin
      Project_Rec := fProjects_List.Get_Project( i );
      if Ini_Strore.Is_Project_Opened( Project_Rec.id )
        then
          do_View_Project( Project_Rec );
    end;
end;

procedure TVCS_Client_Project_List_F.Open_Project;
begin
  do_View_Project( fProjects_List.Get_Project_by_ID( Arg_ID ) );
end;

procedure TVCS_Client_Project_List_F.Options_AExecute(Sender: TObject);
begin
  Show_Options( odp_Common );
end;

procedure TVCS_Client_Project_List_F.Projects_LVChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
begin
  (* ���� ���������� ������ *)
  if fList_Updating_Flag
    then
      Exit;

  Set_Enabled;
end;

procedure TVCS_Client_Project_List_F.Projects_PCChange(Sender: TObject);
begin
  Set_Enabled;
end;

procedure TVCS_Client_Project_List_F.Refresh_Fonts;
var i : integer;
begin
  Set_Font;

  for i := 0 to Screen.FormCount - 1 do
    if ( Screen.Forms[ i ] is TProject_Form )
        and
       ( ( Screen.Forms[ i ] as TProject_Form ).Project_ID > 0 )
      then
        begin
          ( Screen.Forms[ i ] as TProject_Form ).Refresh_Font;
        end;
end;

procedure TVCS_Client_Project_List_F.Refresh_Toolbars;
var i : integer;
begin
  Set_ToolBar( ToolBar1 );

  for i := 0 to Screen.FormCount - 1 do
    if ( Screen.Forms[ i ] is TProject_Form )
        and
       ( ( Screen.Forms[ i ] as TProject_Form ).Project_ID > 0 )
      then
        begin
          ( Screen.Forms[ i ] as TProject_Form ).Refresh_Toolbar;
        end;
end;

procedure TVCS_Client_Project_List_F.Set_Catalog;
begin
  if Arg_Refresh
    then
      begin
        f_Catalog.Set_URL_and_Refresh;
        do_Refresh_List;
      end;
      
  if Ini_Strore.Str[ ii_Catalog_URL ] <> ''
    then
      begin
        Catalog_TS.TabVisible := True;
        //Catalog_TS.Visible := True;
      end
    else
      begin
        Catalog_TS.TabVisible := False;
        //Catalog_TS.Visible := False;
      end;  
end;

procedure TVCS_Client_Project_List_F.Set_Enabled;
var add_enabled, view_enabled : boolean;
    i : integer;
    s : string;
    data : pointer;
begin
  view_enabled := False;

  if Projects_PC.ActivePageIndex = 0
    then
      begin
        view_enabled := Projects_LV.Selected <> nil;
        add_enabled  := True;
      end
    else
      begin
        i := Catalog_LV.ItemIndex;
        if i >= 0
          then
            begin
              data := Catalog_LV.Items[i].Data; (* ��������� Assigned(Data) ������������, �.�. 0 ��� � ���� Nil *)
              s := f_Catalog.Get_Server_URL( integer( Data ) );
              view_enabled := fProjects_List.Get_Local_Path_By_Server_Url( s ) <> '';
              add_enabled := not view_enabled;
           end
         else
           add_enabled := True;
      end;

  View_Project_A.Enabled := view_enabled;
  Add_Project_A.Enabled  := add_enabled;
end;

procedure TVCS_Client_Project_List_F.Set_Font;
begin
  Ini_Strore.Set_Font( Font );
end;

procedure TVCS_Client_Project_List_F.Set_Timer;
var interval_ms : Cardinal;
begin
  interval_ms := Ini_Strore.Monitor_Interval * 60 * 1000;
  if Timer1.Interval <> interval_ms
    then
      begin
        Timer1.Interval := interval_ms;
      end;
end;

procedure TVCS_Client_Project_List_F.Timer1Timer(Sender: TObject);
var i : integer;
begin
  for i := 0 to Screen.FormCount - 1 do
    if ( Screen.Forms[ i ] is TProject_Form )
        and
       ( ( Screen.Forms[ i ] as TProject_Form ).Project_ID > 0 )
      then
        begin
          ( Screen.Forms[ i ] as TProject_Form ).Check_Outdated;
        end;
end;

procedure TVCS_Client_Project_List_F.do_Refresh_List;
var i : Integer;
    prev_selected_project_data : pointer; (* ���������� � ������ ������ *)

    prev_selected_catalog_url : string; (* ���������� � �������� ��� *)
    
    filter_text : string;
    filter_text_empty : boolean;
begin
  (* ��������� ���������� � ������ ������,
     �������� ������ (�������� ������� �����������),
     ������������ ���������� ������ *)
  prev_selected_project_data := nil;

  fList_Updating_Flag := True;

  Projects_LV.Items.BeginUpdate;
  if Projects_LV.Selected <> nil
    then
      prev_selected_project_data := Projects_LV.Selected.Data;

  Projects_LV.Items.Clear;

  filter_text := AnsiLowerCase( Filter_E.Text );
  filter_text_empty := filter_text = '';

  for i := 0 to fProjects_List.Project_Array_Count - 1 do
    with fProjects_List.Get_Project( i ) do
      begin
        if ( filter_text_empty )
           or
           ( AnsiPos( filter_text, AnsiLowerCase( server_url ) ) > 0 )
           or
           ( AnsiPos( filter_text, AnsiLowerCase( local_checkout ) ) > 0 )
          then
            with Projects_LV.Items.Add do
              begin
                Caption := local_checkout;
                SubItems.Add( server_url );
                SubItems.Add( local_checkout );
                Data := pointer( id );
                if ( prev_selected_project_data <> nil ) and ( prev_selected_project_data = data )
                  then
                    Selected := True;
              end;
      end;

  if ( Projects_LV.Items.Count > 0 )
    and
    ( Projects_LV.Selected = nil )
    then
      Projects_LV.ItemIndex := 0;

  Projects_LV.Items.EndUpdate;

  with Catalog_LV.Items do
    begin
      if Catalog_LV.Selected <> nil
        then
          prev_selected_catalog_url := 'Catalog_LV.Selected.SubItems[ Catalog_Server_URL_Column ]';

      BeginUpdate;
      Clear;

      for i := 0 to high( f_Catalog.fCatalog_Items_A ) do
        with f_Catalog.fCatalog_Items_A[i] do
          begin
            if ( filter_text_empty )
                or
               ( AnsiPos( filter_text, f_name ) > 0 )
                or
               ( AnsiPos( filter_text, f_url ) > 0 )
                or
               ( AnsiPos( filter_text, f_desc ) > 0 )
              then
                begin
                  with Add do
                    begin
                      Caption := f_name;
                      Data := pointer( i ); (* ������ �� ������ � fCatalog_Items_A *)
                      with SubItems do
                        begin
                          Add( f_url ); (* ����� �� Catalog_Server_URL_Column ! *)
                          Add( f_desc );
                          Add( fProjects_List.Get_Local_Path_By_Server_Url( f_url ) );
                        end;

                      if ( prev_selected_catalog_url <> '' ) and ( prev_selected_catalog_url = 'SubItems[ Catalog_Server_URL_Column ]' )
                        then
                          Selected := True;
                    end;
                end;
          end;

          if ( Count > 0 ) and ( Catalog_LV.Selected = nil )
            then
              Catalog_LV.ItemIndex := 0;
      EndUpdate;
    end;

  fList_Updating_Flag := False;

  (* �������� ������� *)

  Set_Enabled;
end;

procedure TVCS_Client_Project_List_F.do_View_Project;
begin
  View_Project( fProjects_List, Arg_Project_Rec );
end;

procedure TVCS_Client_Project_List_F.View_Project_AExecute(
  Sender: TObject);
var li : TListItem;
    id : integer;
    s : string;
begin
  if Projects_PC.ActivePageIndex = 0
    then
      begin
        li := Projects_LV.Selected;

        if assigned( li )
          then
            Open_Project( integer( li.data ) );
      end
    else
      begin
        li := Catalog_LV.selected;
        if assigned( li )
          then
            begin
              s := f_Catalog.Get_Server_URL( integer( li.Data ) );

              id := fProjects_List.Get_Project_ID_By_Server_Url( s );
              if id >= 0
                then
                  Open_Project( id );
            end;
      end;  
end;

end.
