unit u_vcs_repo_catalog;

(* ������� ������ *)

interface

uses Classes, XMLDoc;

type T_Catalog_Item = record
       f_url : string;  (* ������ *)
       f_name : string;
       f_desc : string; (* �������� *)
      end;

type T_Catalog_Items_A = array of T_Catalog_Item;

type T_Catalog = class( TComponent )
      private
       fXMLDocument: TXMLDocument;
      public
       fCatalog_Items_A : T_Catalog_Items_A;
       constructor Create; { ( Arg_Catalog_Url ) }reintroduce; virtual;
       procedure Set_URL_and_Refresh;
       function Refresh_List : boolean;
       function Get_Server_URL( Arg_IDX : integer ) : string;
       destructor Destroy; override;
      end;

implementation

uses xmldom, XMLIntf, msxmldom,
     sysutils,
     dialogs,
     u_web_utils, U_Msg, u_vcs_Ini;

{ T_Catalog_Urls }

constructor T_Catalog.Create;
begin
  inherited Create( nil );

  SetLength( fCatalog_Items_A, 0 );

  fXMLDocument := TXMLDocument.Create( Self );
  Set_URL_and_Refresh;
end;

function T_Catalog.Refresh_List;
var StartItemNode : IXMLNode;
    ANode : IXMLNode;
    i : integer;
    f_EDOMParseError : EDOMParseError;
    display_msg, log_msg : string;

    project_name, project_url, project_desk : string;
begin
  try
    fXMLDocument.Active := True;

    (* ���� � XML ��� �����. ��������� ���� ���� ���� �����-�� ������ - ��������� ����� ������ ���� *)
    StartItemNode := fXMLDocument.ChildNodes.FindNode('catalog');
    ANode := StartItemNode;

    if ANode <> nil
      then
        begin
          ANode := ANode.ChildNodes.FindNode('repository');

          i := 0;
          while ( ANode <> nil ) do
            begin
              project_name := ANode.ChildNodes['name'].Text;
              project_url  := ANode.ChildNodes['url'].Text;
              project_desk := ANode.ChildNodes['description'].Text;

              if ( project_url <> '' )
                  and
                 ( Check_URL( project_url ) ) (* ��� ����� ���� ������ ����� - �� ���� ��� ���������. �������� ���� �� ��������, ������� ��������� ������ ��������� ���� *)
                then
                  begin
                    SetLength( fCatalog_Items_A, i + 1 );
                    fCatalog_Items_A[i].f_name := project_name;
                    fCatalog_Items_A[i].f_url := project_url;
                    fCatalog_Items_A[i].f_desc := project_desk;
                    inc( i );
                  end;
              ANode := ANode.NextSibling;
            end;
        end;
  (* �������������� ���������� ����������� � ��������. ������ ������ � ������������ ������� EDOMParseError,
     �� ��� � EOleException, � ��������� ��� �����-�� *)
  except on E: Exception do
    begin
      if E is EDOMParseError
        then
          begin
                begin
                  f_EDOMParseError := E as EDOMParseError;
                  (* http://stackoverflow.com/questions/36552156/edomparseerror-errorcode-list *)
                  if f_EDOMParseError.ErrorCode = -2146697210 //$00800C0006
                    then
                      display_msg := cap( cap_cant_Connect_to_Catalog )
                    else
                      display_msg := cap( cap_cant_Parse_Catalog );

                  display_msg := display_msg + ret + ret + fXMLDocument.FileName;
                  log_msg := display_msg + ret +
                             'Parse error: ' + e.ClassName + ret +
                             'Document: ' + fXMLDocument.FileName + ret +
                             'Line: ' + IntToStr( f_EDOMParseError.Line ) + ret +
                             'Message: ' + e.Message + ret +
                             'Error code: ' + inttostr( f_EDOMParseError.ErrorCode ) + ' hex ' + IntToHex( f_EDOMParseError.ErrorCode, 8 ) + ret +
                             'Reason:  ' + f_EDOMParseError.Reason;
                end
          end
        else
          begin
            display_msg := cap( cap_cant_Parse_Catalog ) + ret + ret + fXMLDocument.FileName;
            log_msg := display_msg + ret + E.ClassName + ret + e.Message;
          end;

      SetLength( fCatalog_Items_A, 0 );
      Result := False;
      Write_Log( log_msg, true );
      Show_Err( display_msg );
    end;
  end;
  if fXMLDocument.Active
    then
      fXMLDocument.Active := False;
end;

procedure T_Catalog.Set_URL_and_Refresh;
var url : string;
begin
  (* ������ �� ������� ������ �� ����� *)
  url := Ini_Strore.Str[ ii_Catalog_URL ];
  if url <> ''
    then
      begin
        if Check_URL( url )
          then
            begin
              fXMLDocument.FileName := url;
              Refresh_List;
            end
          else
            Show_Err( cap( cap_Wrong_Catalog_URL ) + ret + cap( cap_Use_Options ) + ret + ret + url );
      end
    else
      SetLength( fCatalog_Items_A, 0 );
end;

destructor T_Catalog.Destroy;
begin
  SetLength( fCatalog_Items_A, 0 );
  fXMLDocument.Free;

  inherited;
end;

function T_Catalog.Get_Server_URL;
begin
  Result := '';
  if Arg_IDX in [ 0 .. high( fCatalog_Items_A ) ]
    then
      Result := fCatalog_Items_A[ Arg_IDX ].f_url;
end;

end.
