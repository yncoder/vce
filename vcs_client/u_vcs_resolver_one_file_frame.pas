unit u_vcs_resolver_one_file_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  u_file_manager_VCS_const, vcs_resolver_const;

type
  Tvcs_resolver_one_file_frame = class(TFrame)
    File_L: TLabel;
    File_E: TEdit;
    Execute_B: TButton;
    Show_in_Explorer_B: TButton;
    Use_This_File_B: TButton;
    procedure Show_in_Explorer_BClick(Sender: TObject);
    procedure Use_This_File_BClick(Sender: TObject);
    procedure File_EEnter(Sender: TObject);
  private
    { Private declarations }
    fResolve_Decision : type_Resolve_Decision;
    fResolve_Decision_callback : T_Resolve_Decision_callback;
    frevision_id : t_revision_id_type;
    fHighlight_Revision_In_Log_callback : T_Highlight_Revision_In_Log_callback;
    fFile_Name : string;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent;
                        Arg_N : string;
                        Arg_revision_id : t_revision_id_type;
                        Arg_Caption : string;
                        Arg_Filename : string;
                        Arg_Renamed : boolean;
                        Arg_Resolve_Decision_callback : T_Resolve_Decision_callback;
                        Arg_Resolve_Decision : type_Resolve_Decision;
                        Arg_Highlight_Revision_In_Log_callback : T_Highlight_Revision_In_Log_callback
                         ); reintroduce; virtual;
  end;

implementation

uses U_ShellOp, U_Msg;

{$R *.dfm}

{ Tvcs_resolver_one_file_frame }

constructor Tvcs_resolver_one_file_frame.Create;
var caption : string;
begin
  frevision_id := Arg_revision_id;
  fHighlight_Revision_In_Log_callback := Arg_Highlight_Revision_In_Log_callback;

  inherited Create( AOwner );

  Name := Name + Arg_N;
  fResolve_Decision := Arg_Resolve_Decision;

  Parent := AOwner as TWinControl;
  caption := Arg_Caption;
  (* � ��������� ���������� �������, ��� ���������� *)
  if Arg_revision_id > revision_unknown
    then
      caption := caption + ' (revision #' + IntToStr( Arg_revision_id ) + ')';

  (* ������������, ��� ����������� � ����������� ����� ����� � ����� ����� *)
  fFile_Name := Arg_Filename;
  File_E.Text    := ExtractFileName( fFile_Name );
  (* ����� ����� � �� ���� �������� *)
  if FileExists( fFile_Name )
    then
      Use_This_File_B.Visible := fResolve_Decision <> trd_None
    else
      begin
        File_E.Font.Color := clGray;
        caption := caption + ' - you delete it locally';
        Use_This_File_B.Visible := True;
        Use_This_File_B.Enabled := False;
      end;

  File_L.Caption := caption;

  {if Arg_Renamed
    then
      Execute_B.Caption := 'Execute from temp'
    else
      Execute_B.Caption := 'Execute';}

  Execute_B.Visible := False; {$message warn '���� �� ����' }

  fResolve_Decision_callback := Arg_Resolve_Decision_callback;
end;

procedure Tvcs_resolver_one_file_frame.File_EEnter(Sender: TObject);
begin
  if ( Assigned( fHighlight_Revision_In_Log_callback ) )
     and
     ( frevision_id > revision_unknown )
    then
      fHighlight_Revision_In_Log_callback( frevision_id );
end;

procedure Tvcs_resolver_one_file_frame.Show_in_Explorer_BClick;
var s : string;
begin
  s := fFile_Name;
  (* ���� ����� ���� ������ - ����� ������� ����� *)
  if not FileExists( s )
    then
      s := ExtractFilePath( s );

  Show_in_Explorer( s );
end;

procedure Tvcs_resolver_one_file_frame.Use_This_File_BClick;
begin
  if fResolve_Decision <> trd_None
    then
      begin
        if Show_Q( Resolve_Decision_Params[fResolve_Decision].msg_warning, False )
          then
            fResolve_Decision_callback( fResolve_Decision )
          else
            ( Owner as tform ).ModalResult := mrNone;
      end;
end;

end.
