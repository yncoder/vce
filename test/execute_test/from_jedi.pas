unit from_jedi;

interface

implementation

uses windows;

end.

const AnsiCarriageReturn = '0';
      AnsiLineFeed = '0';

type 	TTextHandler = procedure(const Text: string) of object;
  
 	function InternalExecute(const CommandLine: string; var Output: string; OutputLineCallback: TTextHandler;
	RawOutput: Boolean; AbortPtr: PBoolean): Cardinal;
	const
	BufferSize = 255;
	var
	Buffer: array [0..BufferSize] of Char;
	TempOutput: string;
	PipeBytesRead: Cardinal;
	
	procedure ProcessLine(LineEnd: Integer);
	begin
	if RawOutput or (TempOutput[LineEnd] <> AnsiCarriageReturn) then
	begin
	while (LineEnd > 0) and (TempOutput[LineEnd] in [AnsiLineFeed, AnsiCarriageReturn]) do
	Dec(LineEnd);
	OutputLineCallback(Copy(TempOutput, 1, LineEnd));
	end;
	end;
	
	procedure ProcessBuffer;
	var
	CR, LF: Integer;
	begin
	Buffer[PipeBytesRead] := #0;
	TempOutput := TempOutput + Buffer;
	if Assigned(OutputLineCallback) then
	repeat
	CR := Pos(AnsiCarriageReturn, TempOutput);
	if CR = Length(TempOutput) then
	CR := 0; // line feed at CR + 1 might be missing
	LF := Pos(AnsiLineFeed, TempOutput);
	if (CR > 0) and ((LF > CR + 1) or (LF = 0)) then
	LF := CR; // accept CR as line end
	if LF > 0 then
	begin
	ProcessLine(LF);
	Delete(TempOutput, 1, LF);
	end;
	until LF = 0;
	end;
	
	{$IFDEF MSWINDOWS}
	// "outsourced" from Win32ExecAndRedirectOutput
	var
	StartupInfo: TStartupInfo;
	ProcessInfo: TProcessInformation;
	SecurityAttr: TSecurityAttributes;
	PipeRead, PipeWrite: THandle;
	begin
	Result := $FFFFFFFF;
	SecurityAttr.nLength := SizeOf(SecurityAttr);
	SecurityAttr.lpSecurityDescriptor := nil;
	SecurityAttr.bInheritHandle := True;
	if not CreatePipe(PipeRead, PipeWrite, @SecurityAttr, 0) then
	begin
	Result := GetLastError;
	Exit;
	end;
	FillChar(StartupInfo, SizeOf(TStartupInfo), #0);
	StartupInfo.cb := SizeOf(TStartupInfo);
	StartupInfo.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
	StartupInfo.wShowWindow := SW_HIDE;
	StartupInfo.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
	StartupInfo.hStdOutput := PipeWrite;
	StartupInfo.hStdError := PipeWrite;
	if CreateProcess(nil, PChar(CommandLine), nil, nil, True, NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo,
	ProcessInfo) then
	begin
	CloseHandle(PipeWrite);
	if AbortPtr <> nil then
	AbortPtr^ := False;
	while ((AbortPtr = nil) or not AbortPtr^) and ReadFile(PipeRead, Buffer, BufferSize, PipeBytesRead, nil) and (PipeBytesRead > 0) do
	ProcessBuffer;
	if (AbortPtr <> nil) and AbortPtr^ then
	TerminateProcess(ProcessInfo.hProcess, Cardinal(ABORT_EXIT_CODE));
	if (WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0) and
	not GetExitCodeProcess(ProcessInfo.hProcess, Result) then
	Result := $FFFFFFFF;
	CloseHandle(ProcessInfo.hThread);
	CloseHandle(ProcessInfo.hProcess);
	end
	else
	CloseHandle(PipeWrite);
	CloseHandle(PipeRead);
	{$ENDIF MSWINDOWS}
	{$IFDEF UNIX}
	var
	Pipe: PIOFile;
	Cmd: string;
	begin
	Cmd := Format('%s 2>&1', [CommandLine]);
	Pipe := Libc.popen(PChar(Cmd), 'r');
	{ TODO : handle Abort }
	repeat
	PipeBytesRead := fread_unlocked(@Buffer, 1, BufferSize, Pipe);
	if PipeBytesRead > 0 then
	ProcessBuffer;
	until PipeBytesRead = 0;
	Result := pclose(Pipe);
	wait(nil);
	{$ENDIF UNIX}
	if TempOutput <> '' then
	if Assigned(OutputLineCallback) then
	// output wasn't terminated by a line feed...
	// (shouldn't happen, but you never know)
	ProcessLine(Length(TempOutput))
	else
	if RawOutput then
	Output := Output + TempOutput
	else
	Output := Output + MuteCRTerminatedLines(TempOutput);
	end;
	
	{ TODO -cHelp :
	RawOutput: Do not process isolated carriage returns (#13).
	That is, for RawOutput = False, lines not terminated by a line feed (#10) are deleted from Output. }
	
	function Execute(const CommandLine: string; var Output: string; RawOutput: Boolean = False;
	AbortPtr: PBoolean = nil): Cardinal;
	begin
	Result := InternalExecute(CommandLine, Output, nil, RawOutput, AbortPtr);
	end;
	
	{ TODO -cHelp :
	Author: Robert Rossmair
	OutputLineCallback called once per line of output. }
	
	function Execute(const CommandLine: string; OutputLineCallback: TTextHandler; RawOutput: Boolean = False;
	AbortPtr: PBoolean = nil): Cardinal; overload;
	var
	Dummy: string;
	begin
	Result := InternalExecute(CommandLine, Dummy, OutputLineCallback, RawOutput, AbortPtr);
	end;

end.
