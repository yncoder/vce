unit u_execute_test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    Label1: TLabel;
    Cmd_E: TEdit;
    Run_B: TButton;
    Run2_B: TButton;
    Button1: TButton;
    procedure Run_BClick(Sender: TObject);
    procedure Run2_BClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses U_ShellOp;

{$R *.dfm}

procedure CaptureConsoleOutput(const ACommand, AParameters: String; AMemo: TMemo);
 const
   CReadBuffer = 2400;
 var
   saSecurity: TSecurityAttributes;
   hRead: THandle;
   hWrite: THandle;
   suiStartup: TStartupInfo;
   piProcess: TProcessInformation;
   pBuffer: array[0..CReadBuffer] of Char;
   dRead: DWord;
   dRunning: DWord;
 begin
   saSecurity.nLength := SizeOf(TSecurityAttributes);
   saSecurity.bInheritHandle := True;  
   saSecurity.lpSecurityDescriptor := nil; 
 
   if CreatePipe(hRead, hWrite, @saSecurity, 0) then
   begin    
     FillChar(suiStartup, SizeOf(TStartupInfo), #0);
     suiStartup.cb := SizeOf(TStartupInfo);
     suiStartup.hStdInput := hRead;
     suiStartup.hStdOutput := hWrite;
     suiStartup.hStdError := hWrite;
     suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;    
     suiStartup.wShowWindow := SW_HIDE; 
 
     if CreateProcess(nil, PChar(ACommand + ' ' + AParameters), @saSecurity,
       @saSecurity, True, NORMAL_PRIORITY_CLASS, nil, nil, suiStartup, piProcess)
       then
     begin
       repeat
         dRunning  := WaitForSingleObject(piProcess.hProcess, 100);        
         Application.ProcessMessages(); 
         repeat
           dRead := 0;
           ReadFile(hRead, pBuffer[0], CReadBuffer, dRead, nil);          
           pBuffer[dRead] := #0; 
 
           OemToAnsi(pBuffer, pBuffer);
           AMemo.Lines.Add(String(pBuffer));
         until (dRead < CReadBuffer);      
       until (dRunning <> WAIT_TIMEOUT);
     end;

       CloseHandle(piProcess.hProcess);
       CloseHandle(piProcess.hThread);    
     CloseHandle(hRead);
     CloseHandle(hWrite);
   end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Memo1.Lines.Text := '';
  if Cmd_E.Text <> ''
    then
      begin
        CaptureConsoleOutput( Cmd_E.Text, '', Memo1 );
      end;
end;

procedure TForm1.Run2_BClick(Sender: TObject);
var STD_Out : string;
begin
  Memo1.Lines.Text := '';
  if Cmd_E.Text <> ''
    then
      begin
        STD_Out := GetDosOutput( Cmd_E.Text );

        Memo1.Lines.Text := STD_Out;
      end;
end;

procedure TForm1.Run_BClick(Sender: TObject);
var STD_Out : string;
begin
  Memo1.Lines.Text := '';
  if Cmd_E.Text <> ''
    then
      begin
        Execute_To_String( Cmd_E.Text, '', STD_Out );
        Memo1.Lines.Text := STD_Out;
      end;
end;

end.
