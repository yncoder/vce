unit u_git_wrapper_test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  U_API_Wrapper_Const;

type
  TProject_List_F = class(TForm)
    Projects_LV: TListView;
    Panel1: TPanel;
    Close_B: TButton;
    View_Project_B: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure View_Project_BClick(Sender: TObject);
    procedure Close_BClick(Sender: TObject);
  private
    { Private declarations }
    fProjects_List : T_Projects_List;
    procedure Refresh_List;
  public
    { Public declarations }
  end;

var
  Project_List_F: TProject_List_F;

implementation

uses U_ProjectView;

{$R *.dfm}

procedure TProject_List_F.View_Project_BClick(Sender: TObject);
var i, idx : integer;
begin
  i := Projects_LV.ItemIndex;
  if i >= 0
    then
      begin
        View_Project( fProjects_List.Get_Progect( i ) );
      end;
end;

procedure TProject_List_F.Close_BClick(Sender: TObject);
begin
  Close;
end;

procedure TProject_List_F.FormCreate(Sender: TObject);
begin
  fProjects_List := T_Projects_List.Create;
  with fProjects_List do
    begin
      Add2List( 'user_copy_1', 'file:///d:/test/repo', 'd:\test\user_copy_1', 158 );
      Add2List( 'user_copy_2', 'file:///d:/test/repo', 'd:\test\user_copy_2', 199 );
      Add2List( 'user_copy_3', 'file:///d:/test/repo', 'd:\test\user_copy_3', 559 );
    end;

  Refresh_List;
end;

procedure TProject_List_F.FormDestroy(Sender: TObject);
begin
  fProjects_List.Free;
end;

procedure TProject_List_F.Refresh_List;
var i : Integer;
begin
  Projects_LV.Items.Clear;

  for i := 0 to fProjects_List.Project_Array_Count - 1 do
    with fProjects_List.Get_Progect( i ), Projects_LV.Items.Add do
      begin
        Caption := fullname;
        SubItems.Add( server_url );
        SubItems.Add( local_checkout );
        SubItems.Add( IntToStr( id ) );
      end;
end;

end.
