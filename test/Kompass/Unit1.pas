unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ListBox1: TListBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses ActiveX, ComObj, KsTLB;

(* ������� �� CreateOleObject ��� �������� ��������� �������� "������ com ������� ���". ������������ CreateOleObject ������ ������ ����������, ������� ����� ������ *)
function Try_CreateOleObject(const ClassName: string; var Arg_Dispatch : IDispatch; out Out_Except_MSG : string ) : boolean;
var ClassID: TCLSID;
begin
  Result := False;
  Out_Except_MSG := '';

  {$ifdef testing_msg}showmessage( ClassName );{$endif}

  try
    ClassID := ProgIDToClassID(ClassName);
    OleCheck(CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or CLSCTX_LOCAL_SERVER, IDispatch, Arg_Dispatch));
    Result := True;
  except
    (* ��. ComObj OleError *)
    on E: EOleSysError do
      begin
        Out_Except_MSG := E.Message;
      end;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var Arg_Dispatch : IDispatch;
    Out_Except_MSG : string;
    v : variant;
begin
  // http://src-code.net/podklyuchenie-k-kompasu-iz-delphi/
  // https://m.geektimes.ru/post/91390/?mobile=yes
  Try_CreateOleObject( 'KOMPAS.Application.7' {'KOMPASLT.Application.5'}, Arg_Dispatch , Out_Except_MSG );
  v := Arg_Dispatch;
  v.Visible := True;
  v.OpenDocument( 'd:\test\������\������.cdw' );
end;

procedure TForm1.Button2Click(Sender: TObject);
var kompas:KompasObject;
    doc:ksDocument2D;
    doc_spec : ksSpcDocument;
    doc_txt : ksDocumentTxt;
    RasterFormatParam : ksRasterFormatParam;

    fn : string;


type T_Kompass_Files = ( tk_2d, tk_2d_frw, tk_SPW, tk_doc );


var pc, i : integer;

    kompas_doc_type : T_Kompass_Files;
    ext : string;
begin
  fn := ListBox1.Items[ ListBox1.ItemIndex ];

  memo1.Lines.Add( fn );
  //exit;

      if Kompas = nil then begin
         {$IFDEF __LIGHT_VERSION__}
         Kompas:= KompasObject( CreateOleObject('KompasLT.Application.5') );
         {$ELSE}
         Kompas:= KompasObject( CreateOleObject('Kompas.Application.5') );
         {$ENDIF}
         if Kompas <> nil
           then
             //Kompas.Visible := true
           else
             Exit;
      end;
    { TODO : ����� �������� - ����� ������� ���}

      ext := AnsiLowerCase( ExtractFileExt( fn ) );

      memo1.lines.Add( ext );
      
        if ext = '.spw'
          then
            begin
              doc_spec := ksSpcDocument( kompas.SpcDocument );
              kompas_doc_type := tk_SPW;
            end
          else
            begin
              if ext = '.kdw'
                then
                  begin
                    kompas_doc_type := tk_doc;
                    doc_txt := ksDocumentTxt( kompas.DocumentTxt );
                  end
            else
            begin
              doc:= ksDocument2D( kompas.Document2D );
              if ext = '.frw'
                then
                  kompas_doc_type := tk_2d_frw
                else
                  kompas_doc_type := tk_2d;
            end;
            end;

      if FileExists(fn) then // FileName - ��� �������+����
        case         { TODO : ����� ������ ���������� ������ ��� ��������. ������ ������� ������� �� �������. ���������� raster ������ �� ����������. ������� ������� }
          kompas_doc_type of
            tk_2d, tk_2d_frw  : doc.ksOpenDocument(fn,true);
            tk_SPW : doc_spec.ksOpenDocument(fn,1);
            tk_doc : doc_txt.ksOpenDocument(fn,1);
        end
          else
        MessageBox(self.Handle,Pchar(fn),'���� �� ������',MB_OK);

      if ( doc <> nil ) or ( doc_spec <> nil ) or ( doc_txt <> nil )
        then begin

      // https://github.com/den-gts/batchPrint/blob/master/rasterer.py
      // ����� �������

     //��� �������� � ����������

        //doc2.ksSaveDocument('d:\test\������\������2.png');//FileName1 - ��� �������� ���������+����

        case kompas_doc_type of
            tk_2d, tk_2d_frw : RasterFormatParam := ksRasterFormatParam( doc.RasterFormatParam() );
            tk_SPW : RasterFormatParam := ksRasterFormatParam( doc_spec.RasterFormatParam() );
            tk_doc : RasterFormatParam := ksRasterFormatParam( doc_txt.RasterFormatParam() );
        end;

        case kompas_doc_type of
            tk_2d     : RasterFormatParam.extResolution := 96;
            tk_2d_frw : RasterFormatParam.extResolution := 16;
            tk_SPW    : RasterFormatParam.extResolution := 96;
            tk_doc    : RasterFormatParam.extResolution := 96;
        end;
        //RasterFormatParam.extResolution := 96; // frw ���������� ����� ������� ����
        RasterFormatParam.colorType := 4;  // 2 ��� 0 ��� ����� ��; 4 - ������������� ��� �������
        RasterFormatParam.ColorBPP := 8;
        RasterFormatParam.MultiPageOutput := False;
        RasterFormatParam.RangeIndex;
        RasterFormatParam.greyScale := False;

        case kompas_doc_type of
            tk_2d, tk_2d_frw : pc := doc.ksGetDocumentPagesCount();
            tk_SPW : pc := doc_spec.ksGetSpcDocumentPagesCount();
            tk_doc : pc := doc_txt.ksGetDocumentPagesCount();
        end;

        memo1.Lines.Add( IntToStr( pc ) );

        if pc > 1
          then
            for i := 1 to pc do // ��������� ������ �� 1 �� count
              begin
                RasterFormatParam.pages := inttostr(i);
                case kompas_doc_type of
                  tk_2d, tk_2d_frw  : doc.SaveAsToRasterFormat( format( fn + '_%d.png', [ i ] ), RasterFormatParam );
                  tk_SPW : doc_spec.SaveAsToRasterFormat( format( fn + '_%d.png', [ i ] ), RasterFormatParam );
                  tk_doc : doc_txt.SaveAsToRasterFormat( format( fn + '_%d.png', [ i ] ), RasterFormatParam );
                end;

              end
          else
                case kompas_doc_type of
                  tk_2d, tk_2d_frw : doc.SaveAsToRasterFormat( fn + '_.png', RasterFormatParam );
                  tk_SPW : doc_spec.SaveAsToRasterFormat( fn + '_.png', RasterFormatParam );
                  tk_doc : doc_txt.SaveAsToRasterFormat( fn + '_.png', RasterFormatParam );
                end;

        { TODO : ������ ��� ������ �������� - ����� ��� ���-�� �������� � ����������� �����? }


      // function SaveAsToRasterFormat(const fileName: WideString; const rasterPar: IDispatch): WordBool; dispid 190;

           if Kompas <> nil then begin
        //������������� �������
        Kompas.Quit;
        Kompas := nil;
           end;

      end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ListBox1.ItemIndex := 0;
end;

end.
