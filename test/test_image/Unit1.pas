unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses GraphicEx, vcs_diff_frame, u_image_utils;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var s : string;
    List: TStringList;
begin
  Image_Compare_exe := 'd:\p\delphi\Projects\Desktop\bin\imagemagick\compare.exe';


  s := 'd:\Diff\pics\pics_visio.'
  +
  'bmp'
  ;
  
  with TView_diff_frame.Create( self,
   s,'', s + '.r12.bmp', 'mod'
  ) do Align := alClient;

  
end;

end.
