program test_multipage_converter;

uses
  Forms,
  u_test_multipage_converter_form in 'u_test_multipage_converter_form.pas' {Form1},
  u_multipage_converter in '..\..\vcs_client\u_multipage_converter.pas',
  visio_converter in '..\..\formats\visio\visio_converter.pas',
  u_ms_ole_common in '..\..\formats\ms_common\u_ms_ole_common.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
