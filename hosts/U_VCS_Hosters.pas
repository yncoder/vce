(* ������ � API ��������� ��������� *)
unit U_VCS_Hosters;

interface

type T_Hosted_Repo_Rec = record
       fName,
       fSVN_Url : string;
       fFork : boolean;
      end;

type T_Hosted_Repo_A = array of T_Hosted_Repo_Rec;

(* ���������� ��������� � GitHub *)
type T_GitHub_Res_Type = ( ghrt_Unknown, ghrt_No_Connection, ghrt_NoSuchUser, ghrt_Ok );

function Get_GitHub_User_Repos( Arg_UserName : string; var Arg_Hosted_Repo_A : T_Hosted_Repo_A ) : T_GitHub_Res_Type;

implementation

uses UrlMon, superobject, Classes, sysutils, U_Const, u_web_utils;

function Get_GitHub_User_Repos;
const cGitHubUserRepos = 'https://api.github.com/users/' + '%s' + '/repos';
      cGitHubAPIRoot = 'https://api.github.com';

var obj: ISuperObject;
    i : integer;
    url, json_str : string;

begin
  Result := ghrt_Unknown;

  SetLength( Arg_Hosted_Repo_A, 0 );

  url := format( cGitHubUserRepos, [ Arg_UserName ] );

  if DownloadUrl2String( url, json_str )
    then
      try
        (* ParseFile *)
        obj := SO( json_str );
        SetLength( Arg_Hosted_Repo_A, obj.AsArray.Length );

        for i := 0 to obj.AsArray.Length - 1 do
          with Arg_Hosted_Repo_A[ i ] do
            begin
              fName    := obj.AsArray[i].S['name'];
              fSVN_Url := obj.AsArray[i].S['svn_url'];
              fFork    := obj.AsArray[i].B['fork'];
            end;

        Result := ghrt_Ok;
      except
      end;

  if Result <> ghrt_Ok
    then
      begin
        (* ��� 2 �������� - ��� ����� � github (��������, ��� ���������, ��� ������������ ������ ���� ����),
           ��� ��� ������ ����� *)
        (* ���������, ���� �� ������ ����� � API *)
        if DownloadUrl2String( cGitHubAPIRoot, json_str )
          then
            Result := ghrt_NoSuchUser (* ����� ���� - ������ �����, ��� ���� � ���������� ����� *)
          else
            Result := ghrt_No_Connection; (* ��� ����� ������ � API *)
      end;
end;

end.
