unit U_ProjectView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  u_common_forms, U_API_Wrapper_Const, u_file_manager_frame, u_file_manager_VCS_const,
  ComCtrls, ToolWin, ActnList;

type T_Refresh_List_Callback = procedure of object;

(*
   ����� ����������
   ����� ������ � ��������� ������
 *)
type T_Form_Mode = ( fm_Add, fm_Checkout );

type
  TProject_Form = class(T_My_Form)
    File_Manager_Frame1: TFile_Manager_Frame;
    Project_Cfg_P: TPanel;
    Calculated_Checkout_path_E: TEdit;
    ToolBar1: TToolBar;
    Checkout_B: TToolButton;
    ToolButton2: TToolButton;
    Close_B: TToolButton;
    Info_P: TPanel;
    Server_Url_L: TLabel;
    Server_Url_E: TComboBox;
    URL_Examples_C_L: TLabel;
    URL_Examples_L: TEdit;
    Username_L: TLabel;
    Username_E: TEdit;
    Password_L: TLabel;
    Password_E: TEdit;
    Repo_Test_View_B: TButton;
    Local_path_CB: TCheckBox;
    Working_Copy_Path_E: TEdit;
    Select_B: TButton;
    New_working_copy_path_L: TLabel;
    ActionList1: TActionList;
    Checkout_A: TAction;
    Close_A: TAction;
    Label1: TLabel;
    IKnowUrl_B: TButton;
    IWantGitHub_B: TButton;
    IHaveLocalRepo_B: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Server_Url_EChange(Sender: TObject);
    procedure Working_Copy_Path_EChange(Sender: TObject);
    procedure Checkout_BClick(Sender: TObject);
    procedure Select_BClick(Sender: TObject);
    procedure Local_path_CBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Repo_Test_View_BClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Username_EChange(Sender: TObject);
    procedure Password_EChange(Sender: TObject);
    procedure Close_AExecute(Sender: TObject);
    procedure IKnowUrl_BClick(Sender: TObject);
    procedure IHaveLocalRepo_BClick(Sender: TObject);
    procedure IWantGitHub_BClick(Sender: TObject);
  private
    { Private declarations }
    fForm_Mode : T_Form_Mode;
    fProject_ID : integer;
    fProjects_List : T_Projects_List;
    fRefresh_List_Callback : T_Refresh_List_Callback;
    fCalculated_Checkout_Path : string;
    fServer_Url_Tested : string;
    fAuth_Params_Tested : T_Auth_Params;
    (* ���� ����� *)
    {$message warn '���� ���� ����� � �������, ���� �������� ������ ������� ������  #138' }
    fBusy_Flag : boolean;
    procedure Calculate_Checkout_Path;
    (* Arg_New - ����� �������������� *)
    procedure Set_Mode( Arg_Mode : T_Form_Mode );
    (* ���������� guid ����������� ��� ����� *)
    function Test_VCS_Link : string;

    function Check_Auth_Params( var Arg_Auth_Params : T_Auth_Params ) : boolean;

    function Type_Serv_Url( Arg_URL : string ) : string;
  public
    { Public declarations }
    procedure Set_Commands_Enabled( Arg_Disable_All : boolean = False );

    property Project_ID : integer read fProject_ID;
    procedure Set_Project( Arg_Project_Rec : T_Project_Record; Arg_New_Checkout : boolean );
    function Get_Current_Folder : string;

    (* https://bitbucket.org/yncoder/desktopclient/issues/55
       ������� ����� ��� ���������� ���� ��������
       - �������� �������� � ��������� �����
       - ������������� VCS
    *)
    procedure Prepare_Show( Arg_Project_ID : integer );
    procedure Check_Outdated;
    procedure Prepare_Close;
    procedure Refresh_Toolbar;
    procedure Refresh_Font;
  end;

procedure View_Project( Arg_Projects_List : T_Projects_List; Arg_Project_Rec : T_Project_Record );

procedure Add_New_Project( Arg_Projects_List : T_Projects_List; Arg_Refresh_List_Callback : T_Refresh_List_Callback; Arg_Url : string = '' );

implementation

uses StrUtils, U_Msg, U_ShellOp, u_vcs_Ini, u_file_manager_VCS, U_Const,
  u_vcs_ico_const, u_web_utils, u_vcs_hosting_form;

{$R *.dfm}

procedure View_Project;
var i : integer;
    found_flag : boolean;
begin
  if Arg_Project_Rec.id <= 0
    then
      begin
        Show_Err( 'Wrong project ID, can''t open!' );
        Exit;
      end;

  (* ���������, ����� ������ ��� ������� *)
  found_flag := False;
  for i := 0 to Screen.FormCount - 1 do
    if ( Screen.Forms[ i ] is TProject_Form )
        and
       ( ( Screen.Forms[ i ] as TProject_Form ).Project_ID = Arg_Project_Rec.id )
      then
        begin
          ( Screen.Forms[ i ] as TProject_Form ).FocusIt;
          found_flag := True;
          break;
        end;

  if not found_flag
    then
      with TProject_Form.Create( Application ) do
        begin
          Set_Mode( fm_Checkout );
          Prepare_Show( Arg_Project_Rec.id );
          fProjects_List := Arg_Projects_List;
          Show;
          Set_Project( Arg_Project_Rec, False );
        end;
end;

procedure Add_New_Project;
var i : integer;
begin
  with TProject_Form.Create( Application ) do
    begin
      fBusy_Flag := False;
      fRefresh_List_Callback := Arg_Refresh_List_Callback;

      (* ������ *)
      if Interface_Image_List_Done
        then
          begin
            Checkout_A.ImageIndex := integer( iii_Update );
            Close_A.ImageIndex := integer( iii_Close );
          end;
          
      Set_ToolBar( ToolBar1, True );

      (* ���������� ����� ��� ������ *)
      for i := 0 to ActionList1.ActionCount - 1 do
        with ( ActionList1.Actions[i] as TAction ) do
          Hint := Caption; 

      Set_Mode( fm_Add );
      fProjects_List := Arg_Projects_List;

      (* ��������� ������ ����� *)
      fProjects_List.Fill_Strings_with_URLs( Server_Url_E.Items );

      Working_Copy_Path_E.Text := Calculate_Work_checkouts_Root;

      if Arg_Url <> ''
        then
          Type_Serv_Url( Arg_Url );
      Show;
    end;
end;

{ TProject_Form }

procedure TProject_Form.Select_BClick(Sender: TObject);
var Directory: String;
begin
  if Select_Directory( cap_Select_Work_Checkout_Folder,
                   Working_Copy_Path_E.Text,
                   false,
                   Directory)
    then
      Working_Copy_Path_E.Text := Directory;
end;

procedure TProject_Form.Calculate_Checkout_Path;
var s, repo_name : string;
begin
  s := '';
  (* ���� ����� �������� - ����� ��������� ������������� ���. ���� ��� - �� ����� *)
  if Local_path_CB.Checked
    then
      s:= Working_Copy_Path_E.Text
    else
      s := Calculate_Work_checkouts_Root; (* ��������� ������ ���� ����� ���� ����� *)

  if s[length(s)] <> Slash
    then
      s := s + slash;

  repo_name := Server_Url_E.Text;
  repo_name := Extact_SVN_FileName( repo_name );

  fCalculated_Checkout_Path := s + repo_name;
  Calculated_Checkout_path_E.Text := fCalculated_Checkout_Path;
end;

procedure TProject_Form.Checkout_BClick(Sender: TObject);
var server_url : string;
    local_path : string;
    related_path : boolean;
    tested_path : string;
    
    username, password : string;

    repo_guid : string;

    idx : integer;
    Project_Record : T_Project_Record;

procedure Show_Path_Wrong( Arg_MSG : string );
begin
  if Working_Copy_Path_E.Enabled
    then
      Working_Copy_Path_E.SetFocus;
  ShowMessage( Arg_MSG );
end;

begin
  server_url := Server_Url_E.Text;
  local_path := fCalculated_Checkout_Path;
  tested_path := local_path;
  (* ���� �� ������ ���� �������, � ����� ��� ���� - ����� Data �������� ������� *)
  if ( not Local_path_CB.Checked ) and ( Ini_Strore.Boo[ ii_Work_Checkouts_ExeData_Root ] )
    then
      begin
        delete( local_path, 1, length( Working_Copy_Path_E.Text ) - 1 );
        related_path := true;
      end
    else
      related_path := false;

  {$i test.inc}
  {$ifdef testing_msg}
  ShowMsg( local_path );
  {$endif}

  username := Username_E.Text;
  password := Password_E.Text;

  (* ������� ��������� *)
  case Check_Dest_For_VCS_Working_Copy( tested_path ) of
    tcpr_None : ;
    tcpr_Ok :
      begin
        fBusy_Flag := True;
        Set_Commands_Enabled( True );

        repo_guid := Test_VCS_Link;   

        if ( server_url = fServer_Url_Tested )
            and
           ( fAuth_Params_Tested.f_username = username )
            and
           ( fAuth_Params_Tested.f_password = password )
          then
            begin
              if ( related_path ) or ( ForceDirectories( local_path ) )
                then
                  begin
                    (* ������� ������� *)
                    idx := fProjects_List.Save2List( '', server_url, local_path, username, password, repo_guid );
                    Project_Record := fProjects_List.Get_Project( idx );
                    Prepare_Show( Project_Record.id );
                    Set_Project( Project_Record, True );
                    fRefresh_List_Callback;
                    Set_Mode( fm_Checkout );
                  end
                else
                  Show_Path_Wrong( local_path + ret + ' could not be created!' );;
            end;
        fBusy_Flag := False;
        Set_Commands_Enabled( False );
      end;
    tcpr_Drive_Doesnt_Exist : Show_Path_Wrong( cap( cap_Error_Drive_doesnt_exist ) );
    tcpr_Path_Exist : Show_Path_Wrong( tested_path + ret + ' is not empty!' );
    tcpr_Path_Busy : Show_Path_Wrong( tested_path + ret + ' is not empty!' );
    tcpr_Nested : Show_Path_Wrong( tested_path + ret + ' is nested (owned by another working copy)!' );
    tcpr_ForbiddenChars : Show_Path_Wrong( tested_path + ret + ' contain illegal characters!' );
  end;
end;

function TProject_Form.Check_Auth_Params;
begin
  Result := True;

  Arg_Auth_Params.f_username := Username_E.Text;
  Arg_Auth_Params.f_password := Password_E.Text;
end;

procedure TProject_Form.Check_Outdated;
begin
  if fForm_Mode = fm_Checkout
    then
      begin
        File_Manager_Frame1.Check_Outdated;
      end;
end;

procedure TProject_Form.Close_AExecute(Sender: TObject);
begin
  Close;
end;

procedure TProject_Form.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ( fForm_Mode = fm_Checkout )
     and (* �������� - ������� ����� *)
     ( File_Manager_Frame1.Busy_Flag )
    then
      begin
        ShowMessage( cap( cap_Manager_Busy ) );
        Action := caNone;
      end
    else
      begin
        (* �������� - ������� ����� *)
        if fBusy_Flag
          then
            begin
              ShowMessage( cap( cap_Manager_Busy ) );
              Action := caNone;
            end
          else
            begin
              Action := caFree;
              Prepare_Close;
            end;
      end;
end;

procedure TProject_Form.FormCreate(Sender: TObject);
begin
  fServer_Url_Tested := '';
  fAuth_Params_Tested.f_username := '';
  fAuth_Params_Tested.f_password := '';
  fCalculated_Checkout_Path := '';
end;

procedure TProject_Form.FormDestroy(Sender: TObject);
begin
  File_Manager_Frame1.Close_FM;
end;

function TProject_Form.Get_Current_Folder;
begin
  if ( fForm_Mode = fm_Checkout ) and ( fProject_ID > 0 ) 
    then
      begin
        Result := File_Manager_Frame1.Get_Current_Folder;
      end
    else
      Result := '';
end;

procedure TProject_Form.IHaveLocalRepo_BClick(Sender: TObject);
var Directory: String;
begin
  if ( Select_Directory( cap_Select_Local_Repo_Path, '', True, Directory ) )
     and
     ( Directory <> '' )
    then
      begin
        (* ��������� ���� *)
        if Check_Path_Is_Local_SVN_Repo( Directory )
          then
            Type_Serv_Url( Convert_Path2URL( Directory ) )
          else
            ShowMsg( cap( cap_Folder_is_Not_Local_Repo_Path ) );
      end;
end;

procedure TProject_Form.IKnowUrl_BClick(Sender: TObject);
begin
  Server_Url_E.SetFocus;
end;

procedure TProject_Form.IWantGitHub_BClick(Sender: TObject);
var url : string;
begin
  url := Show_Hosted_Repo;
  if url <> ''
    then
      Type_Serv_Url( url );
end;

procedure TProject_Form.Local_path_CBClick(Sender: TObject);
begin
  Working_Copy_Path_E.Enabled := Local_path_CB.Checked;
  Select_B.Enabled := Local_path_CB.Checked;
  Calculate_Checkout_Path;
end;

procedure TProject_Form.Password_EChange(Sender: TObject);
begin
  Set_Commands_Enabled;
end;

procedure TProject_Form.Prepare_Close;
begin
  if fProject_ID > 0
    then
      begin
        Ini_Strore.Save_Form_Sizes( Self, True, Get_Current_Folder );
      end;
end;

procedure TProject_Form.Prepare_Show;
begin
  if Arg_Project_ID > 0
    then
      begin
        Name := Name + '_p' + IntToStr( Arg_Project_ID );

        Ini_Strore.Load_Form_Sizes( Self );
      end;
end;

procedure TProject_Form.Refresh_Font;
begin
  if fForm_Mode = fm_Checkout
    then
      Ini_Strore.Set_Font( File_Manager_Frame1.Font );
end;

procedure TProject_Form.Refresh_Toolbar;
begin
  if fForm_Mode = fm_Checkout
    then
      File_Manager_Frame1.Refresh_Toolbar;
end;

procedure TProject_Form.Repo_Test_View_BClick(Sender: TObject);
begin
  Set_Commands_Enabled( True );

  Test_VCS_Link;

  Set_Commands_Enabled( False );
end;

procedure TProject_Form.Server_Url_EChange(Sender: TObject);
begin
  Set_Commands_Enabled;
  Calculate_Checkout_Path;
end;

procedure TProject_Form.Set_Commands_Enabled;

procedure Set_Checkout_B_Enabled( Arg_Enabled : boolean );
begin
  Checkout_A.Enabled := Arg_Enabled;
{  if Arg_Enabled
    then
      Checkout_B.Font.Style := [ fsBold ]
    else
      Checkout_B.Font.Style := [ ];}
end;

var user_name_correct : boolean;
begin
  if Arg_Disable_All
    then
      begin
        Repo_Test_View_B.Enabled := False;
        Set_Checkout_B_Enabled( False );
        Close_A.Enabled := not fBusy_Flag;
        
        Username_E.Enabled := False;
        Password_E.Enabled := False;

        Local_path_CB.Enabled := False;
        Working_Copy_Path_E.Enabled := False;
        Select_B.Enabled := False;
      end
    else
      begin
        Username_E.Enabled := True;
        Password_E.Enabled := True;

        (* ���� �������� ����, ������ ������ ���������, �� ������ ���� ������ ���� ���� *)
        if ( Password_E.Text <> '' ) and ( Username_E.Text = '' )
          then
            user_name_correct := False
          else
            user_name_correct := True;

        Repo_Test_View_B.Enabled := ( Server_Url_E.Text <> '' ) and user_name_correct;

        Set_Checkout_B_Enabled(
          ( Server_Url_E.Text <> '' )
          and
          user_name_correct
          and
          ( Server_Url_E.Text = fServer_Url_Tested )
          and
          ( Username_E.Text = fAuth_Params_Tested.f_username )
          and
          ( Password_E.Text = fAuth_Params_Tested.f_password )
          and
          ( Working_Copy_Path_E.Text <> '' )
          );


        Local_path_CB.Enabled := True;
        Local_path_CBClick( Self );
      end;
end;

procedure TProject_Form.Set_Mode;
begin
  fForm_Mode := Arg_Mode;
  
  case fForm_Mode of
     fm_Add :
       begin
         Project_Cfg_P.Visible := True;
         File_Manager_Frame1.Visible := False;

         {$message warn '�������� https://bitbucket.org/yncoder/desktopclient/issues/138 '}
         URL_Examples_L.Text := ReplaceStr( Repo_Url_Short_Examples{Repo_Url_Examples}, ';', '     ' );

         Set_Commands_Enabled;
         Caption := cap( cap_Add_New_Project );
       end;
     fm_Checkout :
       begin
         Project_Cfg_P.Visible := False;
         File_Manager_Frame1.Visible := True;
         Refresh_Font;
       end;
   end;
end;

procedure TProject_Form.Set_Project;
var last_saved_path : string;
    Auth_Params : T_Auth_Params;
begin
  fProject_ID := Arg_Project_Rec.id;

  Caption := Arg_Project_Rec.local_checkout;

  last_saved_path := Ini_Strore.Get_Project_Saved_Current_Folder( Name );
                                
  if Arg_New_Checkout
    then
      Check_Auth_Params( Auth_Params )
    else
      begin
        Auth_Params.f_username := Arg_Project_Rec.user_name;
        Auth_Params.f_password := Arg_Project_Rec.pw;
      end;

  File_Manager_Frame1.Init_FM( fm_View_Checkout, fProjects_List,
                               Arg_Project_Rec.server_url, Auth_Params, fProject_ID, Arg_Project_Rec.local_checkout, last_saved_path );
end;

function TProject_Form.Test_VCS_Link;
var url : string;
    Connect_Results : T_Connect_Results;
    Auth_Params : T_Auth_Params;
    Repo_Info : T_Repo_Info;
begin
  Result := '';

  url := Server_Url_E.Text;
  if Check_Auth_Params( Auth_Params )
    then
      File_Manager_Frame1.Init_FM( fm_Testing_Server, fProjects_List, url, Auth_Params, 0 )
    else
      Exit;

  Connect_Results := File_Manager_Frame1.Test_VCS_Link( url, Repo_Info );

  if Connect_Results = tcr_Ok
    then
      begin
        fServer_Url_Tested := url;
        fAuth_Params_Tested := Auth_Params;
        Result := Repo_Info.UUID;
      end
    else
      begin
        Server_Url_E.SetFocus;
        fServer_Url_Tested := '';
        fAuth_Params_Tested.f_username := '';
        fAuth_Params_Tested.f_password := '';

        case Connect_Results of
          tcr_WrongURL : ShowMessage( cap( cap_Wrong_Repo_Url ) + ret + ret + ReplaceStr( Repo_Url_Examples, ';', ret + ret ) );
          tcr_Fail     : ShowMessage( cap( cap_Error_Repo_Connect ) );
        end;
      end;
end;

procedure TProject_Form.Username_EChange(Sender: TObject);
begin
  Set_Commands_Enabled;
end;

procedure TProject_Form.Working_Copy_Path_EChange(Sender: TObject);
begin
  Set_Commands_Enabled;
  Calculate_Checkout_Path;
end;

function TProject_Form.Type_Serv_Url;
begin
  Server_Url_E.Text := Arg_URL;
  (* ������� ���������� *)
  Server_Url_E.OnChange( nil );
end;

end.
