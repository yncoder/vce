unit U_API_Wrapper_Const;

interface

uses Classes;

type T_Project_Record = record
       fullname : string;
       server_url, local_checkout      : string;
       user_name, pw : string;
       repo_guid : string;
       id       : integer;
      end;
     T_Project_Array = array of T_Project_Record;

type T_Projects_List = class
       private
         fProject_Array : T_Project_Array;
         procedure Clear_List;
         procedure Save_Project2Ini( Arg_IDX : integer );
         function Add2List( Arg_fullname : string; Arg_server_url, Arg_local_checkout, Arg_User_Name, Arg_PW : string; Arg_Repo_Guid : string; Arg_id : integer ) : integer;
       public
         function Project_Array_Count : integer;
         function Get_Project_IDX_by_ID( Arg_ID : integer ) : integer;           // ������ �� ���
         function Get_Project( Arg_IDX : integer ) : T_Project_Record;           // ������ �� �������
         function Get_Project_by_ID( Arg_ID : integer ) : T_Project_Record;      // ������ �� ���
         function Get_Project_IDX_By_Server_Url( Arg_Url : string ) : integer;   // ������ �� ���������� ���� (������)
         function Get_Project_ID_By_Server_Url( Arg_Url : string ) : integer;    // �� �� ���������� ���� (������)
         function Get_Local_Path_By_Server_Url( Arg_Url : string ) : string;     // ���� � ������� �� ���������� ���� (������)

         constructor Create;
         function Save2List( Arg_fullname : string; Arg_server_url, Arg_local_checkout, Arg_User_Name, Arg_PW : string; Arg_Repo_Guid : string; Arg_id : integer = 0 ) : integer;
         procedure Save_Credentials( Arg_id : integer; Arg_User_Name, Arg_PW : string );

         (* ��������� Arg_Strings ������� ����� �� ��������� �������� *)
         procedure Fill_Strings_with_URLs( Arg_Strings : TStrings );

         procedure Load;
         destructor Done;
       end;

implementation

uses SysUtils,
     IniFiles, U_Msg

     {$include directives.inc}
     {$ifdef VCS_Client}
       ,u_vcs_Ini
     {$endif}
     , U_Const;

function T_Projects_List.Save2List;

function Calc_New_ID : integer;
var i : integer;
begin
  repeat
    i:= 1 + Get_Big_Rnd;
  until Get_Project_IDX_by_ID( i ) < 1;

  Result := i;
end;

var id, i : integer;
begin
  (* id ��� *)
  Randomize;

  id := Arg_id;
  if id = 0
    then
      id := Calc_New_ID;

  i := Add2List( Arg_fullname, Arg_server_url, Arg_local_checkout, Arg_User_Name, Arg_PW, Arg_Repo_Guid, id );
  Save_Project2Ini( i );
  Result := i;
end;

function T_Projects_List.Add2List;
var i : integer;
begin
  i := length( fProject_Array );
  SetLength( fProject_Array, i + 1 );
  with fProject_Array[ i ] do
    begin
      fullname        := Arg_fullname;
      server_url      := Arg_server_url;
      local_checkout  := Arg_local_checkout;

      user_name       := Arg_User_Name;
      pw              := Arg_PW;

      repo_guid       := Arg_Repo_Guid;

      id              := Arg_id;
    end;
  Result := i;
end;

procedure T_Projects_List.Clear_List;
begin
  SetLength( fProject_Array, 0 );
end;

constructor T_Projects_List.Create;
begin
  Clear_List;
end;

destructor T_Projects_List.Done;
begin
  Clear_List;
end;

procedure T_Projects_List.Fill_Strings_with_URLs;
var i : integer;
    url : string;
begin
  for i := 0 to high( fProject_Array ) do
    begin
      (* ��������� ������������ *)
      url := fProject_Array[ i ].server_url;
      if Arg_Strings.IndexOf( url ) = -1
        then
          Arg_Strings.Add( url );
    end;
end;

function T_Projects_List.Get_Local_Path_By_Server_Url(Arg_Url: string): string;
var i : integer;
begin
  Result := '';

  i := Get_Project_IDX_By_Server_Url( Arg_Url );
  if i >= 0
    then
      begin
        Result := fProject_Array[ i ].local_checkout;
      end;
end;

function T_Projects_List.Get_Project;
begin
  Result := fProject_Array[ Arg_IDX ];
end;

function T_Projects_List.Get_Project_ID_By_Server_Url;
var i : integer;
begin
  Result := -1;
  i := Get_Project_IDX_By_Server_Url( Arg_Url );
  if i >= 0
    then
      Result := fProject_Array[ i ].id;
end;

function T_Projects_List.Get_Project_by_ID;
var i : integer;
begin
  i := Get_Project_IDX_by_ID( Arg_ID );
  if i >= 0
    then
      Result := fProject_Array[ i ];
end;

function T_Projects_List.Get_Project_IDX_by_ID;
var i : integer;
begin
  result := -1;

  for i := 0 to High( fProject_Array ) do
    if fProject_Array[i].id = Arg_ID
      then
        begin
          Result := i;
          break;
        end;
end;

function T_Projects_List.Get_Project_IDX_By_Server_Url;
var i : integer;
    low_url : string;
begin
  Result := -1;

  low_url := LowerCase( Arg_Url );
  for i := 0 to High( fProject_Array ) do
    if LowerCase( fProject_Array[i].server_url ) = low_url
      then
        begin
          Result := i;
          break;
        end;
end;

procedure T_Projects_List.Load;
var sl : tstringlist;
    i : integer;

    fullname : string;
    server_url, local_checkout, user_name, pw : string;
    repo_guid : string;
    id : integer;
    sec_name : string;
begin
  sl := tstringlist.Create;
  with Ini_Strore.Get_Ini_File do
    begin
      (* ��������� ��� ������ *)
      ReadSections( sl );
      for i := 0 to sl.Count - 1 do
        begin
          sec_name := sl[i];
          if Pos( Project_Section_Name, sec_name ) > 0
            then
              begin
                fullname := ReadString( sec_name, 'fullname', '' );
                server_url := ReadString( sec_name, 'server_url', '' );
                local_checkout := ReadString( sec_name, 'local_checkout', '' );
                user_name := ReadString( sec_name, 'username', '' );

                pw := Decrypt_B64( ReadString( sec_name, 'pw', '' ) );

                repo_guid := ReadString( sec_name, 'repo_guid', '' );

                id := ReadInteger( sec_name, 'id', 0 );

                if ( id = 0 ) or ( server_url = '' ) or ( local_checkout = '' )
                  then
                    Ini_Strore.Del_Section_By_Name( sec_name )
                  else
                    Add2List( fullname, server_url, local_checkout, user_name, pw, repo_guid, id );
              end;
        end;

    end;
  sl.Free;
end;

function T_Projects_List.Project_Array_Count;
begin
  Result := Length( fProject_Array );
end;

procedure T_Projects_List.Save_Credentials;
var sec_name : string;
begin
  sec_name := Project_Section_Name + inttostr( Arg_id );

  with Ini_Strore do
    begin
      with Get_Ini_File do
        begin
          WriteString( sec_name, 'username', Arg_User_Name );
          WriteString( sec_name, 'pw', Encrypt_B64( Arg_PW ) );
        end;
    end;
end;

procedure T_Projects_List.Save_Project2Ini;
var sec_name : string;
begin
  sec_name := Project_Section_Name + inttostr( fProject_Array[ Arg_IDX ].id );

  with Ini_Strore do
    begin
      Del_Section_By_Name( sec_name );
      with Get_Ini_File do
        begin
          WriteInteger( sec_name, 'id', fProject_Array[ Arg_IDX ].id );
          WriteString( sec_name, 'server_url', fProject_Array[ Arg_IDX ].server_url );
          WriteString( sec_name, 'local_checkout', fProject_Array[ Arg_IDX ].local_checkout );
          WriteString( sec_name, 'fullname', fProject_Array[ Arg_IDX ].fullname );
          WriteString( sec_name, 'username', fProject_Array[ Arg_IDX ].user_name );
          WriteString( sec_name, 'repo_guid', fProject_Array[ Arg_IDX ].repo_guid );
          WriteString( sec_name, 'pw', Encrypt_B64( fProject_Array[ Arg_IDX ].pw ) );
        end;
    end;
end;

end.
