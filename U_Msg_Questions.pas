unit U_Msg_Questions;

interface

uses u_vcs_ico_const,
     Controls;

type T_Button_Data = record
       fCaption : string;
       fResult : TModalResult;
       fIntarface_Img_Ico : T_Intarface_Img_Ico;
      end;

function Show_Form_With_Buttons( Arg_Caption, Arg_Text : string; const Arg_Button_List : array of T_Button_Data ) : TModalResult;

implementation

uses StdCtrls, ExtCtrls, ComCtrls, Windows, Forms, Graphics;

type t_b_form = class(TForm)
       procedure toolbutton_click( Sender: TObject );
      end;

procedure t_b_form.toolbutton_click( Sender: TObject );
begin
  ModalResult := TModalResult( ( sender as TToolButton ).tag );
end;

function Show_Form_With_Buttons;
var f : t_b_form;
    bd : T_Button_Data;
    toolbar : TToolBar;
begin
  Result := -1;
  f := t_b_form.CreateNew( Application );
  with f do
    begin
      BorderStyle := bsSizeable;
      Caption := Arg_Caption;
      BorderIcons := [ biSystemMenu ];
      Position := poDesktopCenter;
      Width := Width * 2;
      Height := Height * 2;
    end;

  toolbar := TToolBar.Create( f );
  with toolbar do
    begin
      Parent := f;
      Align := alBottom;
      Visible := true;
      ShowCaptions := true;
      Flat := false;
      AutoSize := true;
    end;

  for bd in Arg_Button_List do
    begin
      with TToolButton.Create( toolbar ) do
        begin
          Tag := bd.fResult;
          Parent := toolbar;
          Caption := bd.fCaption;
          AutoSize := true;
          OnClick := f.toolbutton_click;
          visible := true;
          style := tbsButton;
          if Interface_Image_List_Done
            then
              ImageIndex := integer( bd.fIntarface_Img_Ico );
        end;
    end;

  Set_ToolBar( ToolBar );

  with TPanel.Create( f ) do
    begin
      Parent := f;
      Align := alBottom;
      Caption := Arg_Caption;
    end;

  with tmemo.Create( f ) do
    begin
      Align := alClient;
      ReadOnly := true;
      Color := clBtnFace;
      ScrollBars := ssVertical;
      Parent := f;
      Lines.Text := Arg_Text;
    end;

  Result := f.ShowModal;
  
  f.Free;
end;

end.
