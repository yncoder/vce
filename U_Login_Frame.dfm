object Login_Frame: TLogin_Frame
  Left = 0
  Top = 0
  Width = 560
  Height = 128
  TabOrder = 0
  DesignSize = (
    560
    128)
  object Label1: TLabel
    Left = 3
    Top = 35
    Width = 55
    Height = 13
    Caption = 'User name:'
  end
  object Label2: TLabel
    Left = 3
    Top = 64
    Width = 50
    Height = 13
    Caption = 'Password:'
  end
  object Warning_L: TLabel
    Left = 3
    Top = 3
    Width = 40
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Warning'
  end
  object Wrong_Login_L: TLabel
    Left = 10
    Top = 89
    Width = 351
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Caption = 'The username and/or password you specified are not correct. '
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
    WordWrap = True
  end
  object Username_E: TEdit
    Left = 80
    Top = 32
    Width = 209
    Height = 21
    TabOrder = 0
    OnChange = Username_EChange
  end
  object Password_E: TEdit
    Left = 80
    Top = 59
    Width = 209
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnChange = Password_EChange
  end
  object Login_B: TButton
    Left = 304
    Top = 58
    Width = 75
    Height = 25
    Caption = 'Login'
    Default = True
    TabOrder = 2
  end
  object Reg_B: TButton
    Left = 399
    Top = 58
    Width = 150
    Height = 25
    Caption = 'Free online sign up'
    TabOrder = 3
    OnClick = Reg_BClick
  end
end
