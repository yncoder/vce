unit U_Net_Const;

interface

(* ���������� ���� ����������� http-������� *)
type t_http_result = ( http_res_ok, http_res_bad_auth, http_res_no_connection );

(* ��������� ������ ��������� �������. *)
type T_Net_Call_Results = ( nc_res_Fail,             // ��� �����
                            nc_res_Wrong_API_Verion, // �� �� ������ API
                            nc_res_Auth_Empty,       // �����-������ �� �������
                            nc_res_Auth_Fail,        // ��������� �����-������ �� ��������
                            nc_res_NewData,          // �������� ������, ������� ���� ��������
                            nc_res_DataNoChange,     // �������� ������, �� ��� �� ��������
                            nc_res_Fail_but_Cash     // �������� ������, ������� ���� ��������

                            );

implementation

end.
