object VCS_Log_F: TVCS_Log_F
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'VCS_Log_F'
  ClientHeight = 373
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inline VCS_Log_Frame1: TVCS_Log_Frame
    Left = 0
    Top = 0
    Width = 592
    Height = 373
    Align = alClient
    Constraints.MinHeight = 240
    Constraints.MinWidth = 320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 592
    ExplicitHeight = 373
    inherited Splitter1: TSplitter
      Top = 255
      Width = 592
      ExplicitTop = 255
      ExplicitWidth = 592
    end
    inherited LV: TListView
      Width = 592
      Height = 255
      ExplicitWidth = 592
      ExplicitHeight = 255
    end
    inherited Panel1: TPanel
      Top = 258
      Width = 592
      ExplicitTop = 258
      ExplicitWidth = 592
      inherited Splitter2: TSplitter
        Left = 289
        ExplicitLeft = 289
      end
      inherited Panel2: TPanel
        Left = 484
        ExplicitLeft = 484
      end
      inherited File_List_LB: TListBox
        Left = 292
        ExplicitLeft = 292
      end
      inherited Comment_M: TMemo
        Width = 288
        ExplicitWidth = 288
      end
    end
  end
end
