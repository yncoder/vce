program visio2image;

{$APPTYPE CONSOLE}

uses SysUtils, activex, Windows,
     visio_converter;

function ANSI_2_OEM( Arg_S : string ) : string;
begin
  ansitooem( pchar( Arg_S ), pchar( Arg_S ) );
  Result := Arg_S;
end;

const ret = #13#10;

procedure Print_Error( Arg_Err : string );
begin
  Writeln( 'Error: ' + Arg_Err );
end;

procedure Print_Msg( Arg_Msg : string );
begin
  Writeln( Arg_Msg );
end;

type T_Converter = class(TObject)
  private
    { Private declarations }
    procedure fOne_Page_Complete_NotifyEvent( Arg_Page_Num : integer; Arg_Page_Caption, Arg_Page_Res_File : string );
    procedure fOLE_Exception_Handler( Arg_Err_MSG : string );
  public
    { Public declarations }
    procedure Convert( Arg_FN : string );
  end;

{ T_Converter }

procedure T_Converter.Convert;
begin
  (* http://www.delphigroups.info/2/27/265071.html *)
  CoInitialize(nil);

  case Convert_Visio2Image( ExpandFileName( Arg_FN ), fOne_Page_Complete_NotifyEvent, fOLE_Exception_Handler ) of
    tcr_None                : ;
    tcr_Ok                  : Print_Msg( ret + 'Done.' );
    tcr_File_Not_Found      : Print_Error( 'File not found!' );
    tcr_Ole_Not_Found       : Print_Error( 'MS Visio not found! You need the MS Visio installed on your computer to use the converter.' );
    tcr_Ole_Error_Open_Doc  : Print_Error( 'Can''t open the doc!' );
    tcr_Ole_Error_Export    : Print_Error( 'Can''t export!');
    tcr_Some_Ole_Error      : Print_Error( 'ole error' );
  end;
  CoUninitialize;
end;

procedure T_Converter.fOLE_Exception_Handler;
begin
  Print_Error( ANSI_2_OEM( Arg_Err_MSG ) );
end;

procedure T_Converter.fOne_Page_Complete_NotifyEvent;
begin
  Print_Msg( IntToStr( Arg_Page_Num ) + ' ' + ANSI_2_OEM( Arg_Page_Caption ) + ' ' + ANSI_2_OEM( Arg_Page_Res_File ) );
end;

var Converter : T_Converter;
    fn : string;

const ln_Usage = 'Usage:' + ret +
                 'visio2image.exe visio_document.ext' + ret + ret +
                 'Usage example:'+ ret+
                 'visio2image.exe my_draft.vsd';

begin
  Print_Msg( 'MS Visio to image batch converter version 1.0' + ret + 'You need the MS Visio installed on your computer to use the converter.' +
             ret + ret +
             ln_Usage + ret + ret +
             'Resulting PNG files will be exported to original Visio document folder as "originalname_page_N.png"' +
             ret + ret +
             'PostPDM, copyright 2016.' + ret +
             'http://soft.postpdm.com/' + ret );

  (* ������ ��������� ������ *)

  if ParamCount > 0
    then
      begin
        fn := ParamStr( 1 );

        if FileExists( fn )
          then
            begin
              (* ����������� *)
              Converter := T_Converter.Create;

              Converter.Convert( fn );
              Converter.Free;
            end
          else
            Print_Error( 'File ' + fn + ' not found!' );
      end
    else
      begin
        Print_Error( 'You need to specify the visio file to convert!' + ret + ln_Usage );
      end;  
end.
