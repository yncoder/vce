unit u_ms_word_compare;

interface

type T_MSWord_Compare_Results = (
       tmswcr_None,
       tmswcr_OK,
       tmswcr_LF_Not_Found, tmswcr_RF_Not_Found,
       tmswcr_LF_CantOpen, tmswcr_RF_CantOpen,
       tmswcr_OLE_MSWord_Not_Found,
       tmswcr_OLE_MSWord_Compare_Fail
        );

function MS_Word_Compare( Arg_Left_Doc, Arg_Right_Doc : string; out Out_OLE_Error : string ) : T_MSWord_Compare_Results;

implementation

uses SysUtils, comobj, u_ms_ole_common;

function MS_Word_Compare;

var Word_Dispatch : IDispatch;
    Word_App, Word_L_Doc, Word_R_Doc : Variant;
    ole_error_msg : string;

function Open_Hidded( Arg_FN : string; var Arg_Doc : Variant ) : boolean;
begin
  Result := False;
  (* https://msdn.microsoft.com/EN-US/library/ff839499.aspx

     OpenNoRepairDialog(FileName, ConfirmConversions, ReadOnly, AddToRecentFiles, PasswordDocument, PasswordTemplate, Revert, WritePasswordDocument, WritePasswordTemplate,
     Format, Encoding, Visible, OpenAndRepair, DocumentDirection, NoEncodingDialog, XMLTransform)
  *)
  try
    Arg_Doc := Word_App.Documents.OpenNoRepairDialog(
                Arg_FN,
                {ConfirmConversions} False,
                ReadOnly := True,
                (* ��������� ����������� ���������, ���������� ���� ����� *)
                Visible := False );
    Result := True;
  except
    on E: EOleError do
      Out_OLE_Error := E.Message;
  end;
end;

begin
  Result := tmswcr_None;
  Out_OLE_Error := '';

  if not FileExists( Arg_Left_Doc )
    then
      begin
        Result := tmswcr_LF_Not_Found;
        Exit;
      end;
  if not FileExists( Arg_Right_Doc )
    then
      begin
        Result := tmswcr_RF_Not_Found;
        Exit;
      end;

  if Try_CreateOleObject( Word_App_Classname, Word_Dispatch, ole_error_msg )
    then
      begin
        Word_App := Word_Dispatch;

        if not Open_Hidded( Arg_Left_Doc, Word_L_Doc )
          then
            begin
              Result := tmswcr_LF_CantOpen;
              Word_App.Quit;
              Exit;
            end;

        if not Open_Hidded( Arg_Right_Doc, Word_R_Doc )
          then
            begin
              Result := tmswcr_RF_CantOpen;
              Word_L_Doc.Close;
              Word_App.Quit;
              Exit;
            end;

        (* Word ����� ��� ��������� � ���������� ��������� ������ ������� (doc � docx) � ������������ ������������,
           ��� � ��������� ������ ���������� � ��������� ��������

           https://msdn.microsoft.com/EN-US/library/ff195665.aspx
           CompareDocuments( OriginalDocument, RevisedDocument, Destination,
                     Granularity, CompareFormatting,
                     CompareCaseChanges, CompareWhitespace,
                     CompareTables, CompareHeaders,
                     CompareFootnotes, CompareTextboxes,
                     CompareFields, CompareComments,
                     RevisedAuthor, IgnoreAllComparisonWarnings )

                     �� ������� ��� ����� ���� CompareFormatting = True *)

        Word_App.Visible := True;
        try
          Word_App.CompareDocuments( Word_L_Doc, Word_R_Doc
                                     {$i test.inc}
                                     {$ifdef test_wrong_word_compare} , Fake_Argument := 'fake' {$endif}
                                   );
          Word_L_Doc.Close;
          Word_R_Doc.Close;
          Word_App.Activate;
          (* ������� ����� �� ����, ������� ����� *)
          Result := tmswcr_OK;
          //Word_App.Quit; ������� ���� ����������
        except
          (* ���� ��������� ����� ����������� test_wrong_word_compare *)
          on E: EOleError do
            begin
              Out_OLE_Error := E.Message;
              Result := tmswcr_OLE_MSWord_Compare_Fail;
              Word_App.Quit;
            end;
        end;
      end
    else
      begin
        Result := tmswcr_OLE_MSWord_Not_Found;
      end;
end;

end.
