unit Kompas2image_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ListBox1: TListBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure fOne_Page_Complete_NotifyEvent( Arg_Page_Num : integer; Arg_Page_Caption, Arg_Page_Res_File : string );
    procedure fOLE_Exception_Handler( Arg_Err_MSG : string );
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses kompas_converter;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var fn : string;
    Kompas_Wrapper : T_Kompas_Multipage_Wrapper;
    doc_idx : integer;
    Out_MSG : string;
begin
  fn := ListBox1.Items[ ListBox1.itemindex ];

  memo1.Lines.Add( fn );

  ( sender as tbutton ).Enabled := False;

  Kompas_Wrapper := T_Kompas_Multipage_Wrapper.Create;
  with Kompas_Wrapper do
    begin
      Init_Wrapper( fOne_Page_Complete_NotifyEvent, fOLE_Exception_Handler );
      Open_Invisible_File( fn, doc_idx, Out_MSG );

      Export_File( doc_idx, fn+'ssxsxs.png', True, Out_MSG, 0 );

      Free;
    end;

  ( sender as tbutton ).Enabled := True;
end;

procedure TForm1.fOLE_Exception_Handler(Arg_Err_MSG: string);
begin
//
end;

procedure TForm1.fOne_Page_Complete_NotifyEvent(Arg_Page_Num: integer;
  Arg_Page_Caption, Arg_Page_Res_File: string);
begin
//
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ListBox1.itemindex := 0;
end;

end.
