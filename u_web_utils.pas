unit u_web_utils;

interface

function Extract_Protocol( Arg_URL : string ) : string;

function Check_URL( Arg_URL : string; Arg_protocol_list : string = '' ) : boolean;

(* ������������� �������� ���� � URL *)
function Convert_Path2URL( Arg_Path : string ) : string;

function DownloadUrl2String( const url: string; out out_result: string): boolean;

implementation

uses SysUtils, StrUtils, ShLwApi, IdURI, WinInet, Classes, Windows, U_Msg;

function Extract_Protocol;
var IdURI : TIdURI;
begin
  IdURI := TIdURI.Create( Arg_URL );
  Result := LowerCase( IdURI.Protocol );
  IdURI.Free;
end;

(* ���� ������ �������, �� ���������. ���������� �������� �� ������. *)
function Occurrences(const Text: string; const Substring : Char ): integer;
var i : integer;
begin
  Result := 0;
  for i := 1 to Length( Text ) do
    if ( Text[ i ] = Substring )
      then
        Inc( Result );    
end;

function Check_URL;
var protocol : string;
begin
  Result := False;

  if Pos( '\', Arg_URL ) > 0
    then
      Exit;

  if Pos( '"', Arg_URL ) > 0
    then
      Exit;

  if not( Occurrences( Arg_URL, ':' ) in [1,2] ) (* ���� : ������������ ����� � ��� ��������� �����, ��� ��� : ����� ���� 1 ��� 2 ���� *)
    then
      Exit;

  if Length( Arg_URL ) < 2
    then
      Exit;

  if PathIsURL( PChar( Arg_URL ) )
    then
      begin
        protocol := Extract_Protocol( Arg_URL );

        if Arg_protocol_list = ''
          then
            Result := protocol <> ''
          else
            begin
              if pos( protocol, Arg_protocol_list ) > 0
                then
                  Result := True;
            end;
    end;
end;

function Convert_Path2URL;
begin
  Result := 'file:///' + ReplaceText( Arg_Path, '\', '/' );
end;

function DownloadUrl2String;
var hInet: HINTERNET;
    hFile: HINTERNET;
    buffer: array[1..1024] of byte;
    bytesRead: DWORD;

    ss : tstringstream;
begin
  result := False;
  out_result := '';

  hInet := InternetOpen( program_title, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0 );
  hFile := InternetOpenURL( hInet, PChar(url), nil, 0, 0, 0 );
  if Assigned( hFile)
    then
      begin
        ss := tstringstream.Create( '' );
        repeat
          InternetReadFile( hFile, @buffer, SizeOf( buffer ), bytesRead );
          ss.Write( buffer, bytesRead );
        until bytesRead = 0;
        result := true;
        InternetCloseHandle( hFile );
        out_result := ss.DataString;
        ss.Free;
      end;
  InternetCloseHandle( hInet );
end;

end.
