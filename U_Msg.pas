unit U_Msg;

interface

{$include directives.inc}
      {$ifdef Naiveshark_agent}
const program_title = 'Naiveshark agent';

      {$endif}
      {$ifdef VCS_Client}
const program_title = 'Version Control for engineers';
      {$endif}

      {$ifdef CAD_Diff_App}
const program_title = 'CAD diff';
      {$endif}

const ret = #13#10;
      ret2 = ret + ret;
      slash_n = '\n';

const msg_New_Feed_Data = 'New data!';
      msg_New_Notification_Data = 'New notification!';
      msg_ConnectFail = 'Failed to connect to site!';

      {$ifdef VCS_Client or Naiveshark_agent}
      msg_NewVersion_detected_fmt = 'New version %u.%u of ' + program_title + ' available! Click to open the download page in browser!';
      msg_Version_UpToDate = 'Version is up-to-date';
      {$endif}

{$ifdef CAD_Diff_App}
const CMD_MSG = 'Usage:' + ret +
                'cad_diff.exe OldFile NewFile' + ret2 +
                'Support for common image format (BMP, GIF, JPG, PNG, PSD, SVG etc.),' + ret +
                'MS Visio drawings (need MS Visio to be installed)' + ret +
                'KOMPAS Graphic CDW drawings (need KOMPAS to be installed)'
                ;
{$endif}

const Repo_Url_Short_Examples =
         'file:///d:/svn_repos/repo1' + ';' +

         'svn://svnserver/reponame/' + ';' +

         'https://server/svn/repo_name/';

const Repo_Url_Examples =
         'file:///d:/svn_repos/repo1' + ';' +
         'file:///d|/svn_repos/repos/repo78' + ';' +

         'svn://svnserver/reponame/' + ';' +

         'http://server/svn/repo_name/' + ';' +
         'https://server/svn/repo_name/' + ';' +
         'https://github.com/user/repo'
         ;

type cap_Type = ( cap_Login,
                  cap_Logout,
                  cap_Confirm_Caption,
                  cap_Select_Work_Checkout_Folder,
                  cap_Select_Work_Checkout_Default_Folder,
                  cap_Select_Work_Checkout_Default_Folder_Description,

                  cap_Add_New_Project,

                  cap_Select_Local_Repo_Path,
                  cap_Folder_is_Not_Local_Repo_Path,

                  cap_Work_Checkout_Default_Folder,
                  cap_Work_Checkout_Exe_Data_Folder,
                  cap_Error_Drive_doesnt_exist,
                  cap_Update_Conflicts,
                  cap_Conflict_Resolved,
                  cap_Cant_Resolve_Conflict,
                  cap_Wrong_Repo_Url,
                  cap_Error_Repo_Connect,
                  cap_Confirm_Revert,
                  cap_Nothing_to_Commit,
                  cap_No_Diff_Tool_Configured,
                  cap_No_Diff3_Tool_Configured,
                  cap_Open_Diff_Options_Confirm,
                  cap_Delete_Diff_Tool,
                  cap_Diff_Tool_Not_Found,
                  cap_Cant_Enter_Unversioned_Folder,
                  cap_Manager_Busy,
                  cap_Image_Compare_Fail,
                  cap_Image_Compare,
                  cap_Image_Compare_Size_Is_Different,
                  cap_Image_Convert_Fail,

                  cap_Use_Options,

                  cap_MSWord_Not_Found,
                  cap_MSVisio_Not_Found,
                  cap_MSVisio_Export_Fail,
                  cap_Kompas_Not_Found,

                  cap_File_Not_Found,
                  cap_Cant_Open_File,

                  cap_Error,
                  cap_Cant_Write_On_Disk,
                  cap_Cant_Write_On_Exe_Data,
                  cap_Cant_Write_To_Portable_INI,
                  cap_Save_Changes_q,
                  cap_cant_Connect_to_Catalog,
                  cap_cant_Parse_Catalog,
                  cap_Wrong_Catalog_URL,

                  cap_No_Internet_Connection,
                  cap_No_GitHubUser

                   );


function Cap( Arg_Code : cap_Type ) : string;

procedure ShowMsg( Arg_MSG : string );

procedure Show_Err( Arg_Err : string );

procedure ShowSInt( Arg_S : string; Arg_Int : integer );

procedure ShowInt( Arg_Int : integer );

(* ������ ������ Arg_Q. Arg_Default_Ok ������ ����� �� ��������� �� *)
function Show_Q( Arg_Q : string;
                 Arg_Default_Yes : boolean ) : boolean;

function Show_YN( Arg_Title : string;
                  Arg_Q : string;
                  Arg_Default_Yes : boolean ) : boolean;

function Show_YNC( Arg_Q : string{;
                   Arg_Default_Yes : boolean} ) : integer;

procedure Write_Log( s : string; Arg_Add_DT : boolean );

function Get_Log_FN : string;

(* ������������� ������� '\n' � ������� ������ *)
procedure Convert_N_To_Ret( var Arg_S : string );

implementation

uses Dialogs, SysUtils, Windows, Forms,
     U_Const;

function Cap( Arg_Code : cap_Type ) : string;

(* ������, ����������� ������� � ������� � ��������� ������� *)
type TTxtRes_Record = record
        Ident : string;    (* ��������� �������������, �� �������� �������� ������� ��������� � ����� *)
        Def_Text : string; (* �������� �� ��������� (�� ������, ���� ������� � ����� ���) *)
       end;

const Msg_Texts : array[cap_Type] of TTxtRes_Record =
       ( ( Ident : 'cap_Login'; Def_Text : 'Login : ' ),
         ( Ident : 'cap_Logout'; Def_Text : 'Logout? ' ),
         ( Ident : 'cap_Confirm_Caption'; Def_Text : 'Confirm required!' ),
         ( Ident : 'cap_Select_Work_Checkout_Folder'; Def_Text : 'Select path to work checkout folder' ),
         ( Ident : 'cap_Select_Work_Checkout_Default_Folder'; Def_Text : 'Select path to work checkouts root folder' ),
         ( Ident : 'cap_Select_Work_Checkout_Default_Folder_Description'; Def_Text : 'Select the root folder for all new projects. If leave field empty then the user Document folder will be used.' ),

         ( Ident : 'cap_Add_New_Project'; Def_Text : 'Add new project' ),

         ( Ident : 'cap_Select_Local_Repo_Path'; Def_Text : 'Select local SVN repository folder' ),
         ( Ident : 'cap_Folder_is_Not_Local_Repo_Path'; Def_Text : 'Selected folder doesn''t contain a local repository!' ),

         ( Ident : 'cap_Work_Checkout_Default_Folder'; Def_Text : 'Empty path. Documents folder will be used' ),

         ( Ident : 'cap_Work_Checkout_Exe_Data_Folder'; Def_Text : 'Project checkouts will placed to Data folder in executable path.' ),

         ( Ident : 'cap_Error_Drive_doesnt_exist'; Def_Text : 'Drive doesn''t exist!' ),
         ( Ident : 'cap_Update_Conflicts'; Def_Text : 'Some file are in update conflicts!' ),
         ( Ident : 'cap_Conflict_Resolved'; Def_Text : 'Conflict resolved locally, you need to commit it to server!' ),
         ( Ident : 'cap_Cant_Resolve_Conflict'; Def_Text : 'Can''t resolve conflict! Conflicted files are missed!' ), {$message '� ��� ������ �����?'}

         ( Ident : 'cap_Wrong_Repo_Url'; Def_Text : 'Wrong server URL!' + ret + ret + 'URL can be in format:' ),

         ( Ident : 'cap_Error_Repo_Connect'; Def_Text : 'Error connect to server repository!' ),
         ( Ident : 'cap_Confirm_Revert'; Def_Text : 'Confirm file reverting to server version?\nYour local editions will be sent to Windows trashbin.' ),
         ( Ident : 'cap_Nothing_to_Commit'; Def_Text : 'Nothing to commit!' ),

         ( Ident : 'cap_No_Diff_Tool_Configured'; Def_Text : 'No diff tool is configured.' ),
         ( Ident : 'cap_No_Diff3_Tool_Configured'; Def_Text : 'No diff3 tool is configured.' ),
         ( Ident : 'cap_Open_Diff_Options_Confirm'; Def_Text : 'Open the options dialog?' ),
         ( Ident : 'cap_Delete_Diff_Tool'; Def_Text : 'Delete diff tool from list?' ),
         ( Ident : 'cap_Diff_Tool_Not_Found'; Def_Text : 'Diff tool not found' ),

         ( Ident : 'cap_Cant_Enter_Unversioned_Folder'; Def_Text : 'Can''t enter into unversioned folder. Open it in Explorer?' ),
         ( Ident : 'cap_Manager_Busy'; Def_Text : 'Manager is busy. Please wait!' ),

         ( Ident : 'cap_Image_Compare_Fail'; Def_Text : 'Image compare fail!' ),
         ( Ident : 'cap_Image_Compare'; Def_Text : 'Compare images' ),
         ( Ident : 'cap_Image_Compare_Size_Is_Different'; Def_Text : 'Compare images with different sizes' ),
         ( Ident : 'cap_Image_Convert_Fail'; Def_Text : 'Image convert fail!\nFile may be corrupted.' ),

         ( Ident : 'cap_Use_Options'; Def_Text : 'Use the Options dialog!' ),

         ( Ident : 'cap_MSWord_Not_Found'; Def_Text : 'MS Word is not found! You need the MS Word installed to use this function for MS Word documents.' ),
         ( Ident : 'cap_MSVisio_Not_Found'; Def_Text : 'MS Visio is not found! You need the MS Visio installed to use this function for this type of documents.' ),

         ( Ident : 'cap_MSVisio_Export_Fail'; Def_Text : 'MS Visio export fail.' ),

         ( Ident : 'cap_Kompas_Not_Found'; Def_Text : 'Kompas is not found! You need the Kompas installed to use this function for this type of documents.' ),

         ( Ident : 'cap_File_Not_Found'; Def_Text : 'File not found!' ),
         ( Ident : 'cap_Cant_Open_File'; Def_Text : 'Can''t open file!\nFile may be corrupted or locked.' ),
         ( Ident : 'cap_Error'; Def_Text : 'Error!' ),

         ( Ident : 'cap_Cant_Write_On_Disk'; Def_Text : 'Can''t write on disk!' ),

         ( Ident : 'cap_Cant_Write_On_Exe_Data'; Def_Text : 'Can''t write on executable Data folder!\nSystem application data folder will be used.' ),

         ( Ident : 'cap_Cant_Write_To_Portable_INI'; Def_Text : 'Can''t write on portable\n %s\n configuration file!\nSystem configuration file will be used.' ),

         ( Ident : 'cap_Save_Changes_q'; Def_Text : 'Save changes before exit?' ),

         ( Ident : 'cap_cant_Connect_to_Catalog'; Def_Text : 'Can''t connect to catalog!' ),
         ( Ident : 'cap_cant_Parse_Catalog'; Def_Text : 'Can''t parse the catalog data!\nRead the application log for more information!' ),
         ( Ident : 'cap_Wrong_Catalog_URL'; Def_Text : 'Wrong catalog URL!' ),

         ( Ident : 'cap_No_Internet_Connection'; Def_Text : 'No Internet connection' ),
         ( Ident : 'cap_No_GitHubUser'; Def_Text : 'No info about user "%s" on GitHub!' )

       );

begin
  Result := Msg_Texts[Arg_Code].Def_Text;
  Convert_N_To_Ret( Result );
end;

procedure ShowMsg( Arg_MSG : string );
begin
  ShowMessage( Arg_MSG );
end;

procedure Show_Err( Arg_Err : string );
begin
  Application.MessageBox( PChar( Arg_Err ),
                          PChar( Cap( cap_Error ) ),
                          MB_ICONEXCLAMATION );
end;


procedure ShowSInt( Arg_S : string; Arg_Int : integer );
begin
  ShowMessage( Arg_S + ret + inttostr( Arg_Int ) );
end;

procedure ShowInt( Arg_Int : integer );
begin
  ShowMessage( inttostr( Arg_Int ) );
end;

function Show_Q;
begin
  Result := Show_YN( Cap( Cap_Confirm_Caption ),
                     Arg_Q,
                     Arg_Default_Yes );
end;

function Show_YN;
var DefB : integer;
begin
  if Arg_Default_Yes
    then
      DefB := MB_DEFBUTTON1
    else
      DefB := MB_DEFBUTTON2;

  Result := ( Application.
                MessageBox( PChar( Arg_Q ),
                            PChar( Arg_Title ),
                            MB_YESNO + MB_ICONQUESTION + DefB ) = id_Yes );
end;

function Show_YNC;
begin
  Result := Application.
              MessageBox(
                PChar( Arg_Q ),
                PChar( Cap( cap_Confirm_Caption ) ),
                MB_YESNOCancel + MB_ICONQUESTION + MB_DEFBUTTON1 );
end;

procedure Write_Text( arg_file_name, s : string );
var f : textfile;
begin
  assignfile( f, arg_file_name );
  {$i-}
  Append( f );
  {$i+}
  if ioresult <> 0
    then
      rewrite( f );

  writeln( f, s);
  closefile( f );
end;

function Get_Log_FN;
begin
  Result := AppData_Path + 'log.txt';
end;

procedure Write_Log;
var fn : string;
begin
{$ifndef CAD_Diff_App}

  fn := Get_Log_FN;
  if Arg_Add_DT
    then
      Write_Text( fn, DateTimeToStr( now ) );

  Write_Text( fn, s  + ret );
{$endif}
end;

(* ������������� ������� '\n' � ������� ������ *)
procedure Convert_N_To_Ret( var Arg_S : string );
var i : integer;
begin
  repeat
    i := Pos( slash_n, Arg_S );
    if i > 0
      then
       begin
         Arg_S[i] := #13;
         Arg_S[i+1] := #10;
       end;
  until i = 0;
end;

end.
