# README #

"Version Control for engineers" (http://soft.postpdm.com/index.html) and "CAD Diff" (http://soft.postpdm.com/cad_diff.html) software.

## How to get binaries ##

Ready to install Windows binaries are available here:

https://bitbucket.org/yncoder/naiveshark/downloads/

## How to build ##

Software developed with Turbo Delphi IDE. All used libraries are included to repo.

Windows installer builded with NSIS http://nsis.sourceforge.net

## How to contribute ##

1. Post a task in issue list and discuss your idea or report a bug
2. Create a Fork and send a pull-request

## Have a questions? ##

1. For user experience questions - ask on https://groups.google.com/forum/#!forum/postpdm
2. For programmer questions about sourcecode, build process and contributions - ask in repo Issues https://bitbucket.org/yncoder/vce/issues

## License ##

Apache License, Version 2.0, January 2004

http://www.apache.org/licenses/LICENSE-2.0