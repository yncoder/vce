MS Visio to image batch converter version 1.0
You need the MS Visio installed on your computer to use the converter.

Usage:
visio2image.exe visio_document.ext

Usage example:
visio2image.exe my_draft.vsd

Resulting PNG files will be exported to original Visio document folder as "originalname_page_N.png"

PostPDM, copyright 2016.
http://soft.postpdm.com/