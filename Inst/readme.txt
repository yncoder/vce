ABOUT NAIVESHARK AGENT
======================

Naiveshark Agent is a small freeware Windows program, which keep you informed about data changes on main www.naiveshark.com site.

NaiveShark site is an engineering community portal and open database of world-wide industrial products. You are free to add your (or any) organization or products to catalog, or extend the existing information in community-driven site.

Naiveshark Agent running in the system tray (notification area) of your Windows desktop and waits for new and modified product information on site, when informs you about.

You need the active Internet connection (with minimal bandwidth) for get the program works.

REQUIREMENTS
============

1. Microsoft Windows 95 or later.
2. Internet connection (with minimal bandwidth).
3. No local administrator rights are required!!!

CONTACT INFORMATION
===================
Naiveshark web-site:     http://www.naiveshark.com

INSTALLATION
============

Run nsasetup.exe, select the destination folder and click "Install".
Software will be installed on your Windows desktop.

For uninstall - select "Programs|Naiveshark|Naiveshark agent|Uninstall".

HISTORY
=======

02 July 2015 - v0.5

* Read and display user notifications from server (need user login).

Numbers 0.2, 0.3 and 0.4 are skipped to satisfy server version counting.

29 January 2015 - v0.1

* Auto-check new version from site
* Auto-start when Windows run

20 January 2015 - v0.0.5

* First release.


LICENSE
=======

All copyrights to Naiveshark agent are exclusively owned by
Naiveshark Software - www.naiveshark.com.

Naiveshark agent is distributed as FREEWARE.
It means what you can:
1) Use this software free
2) Freely distribute the software, provided the 
   distribution package is not modified. No person or company 
   may charge a fee for the distribution of Naiveshark agent.

- NAIVESHARK AGENT IS DISTRIBUTED "AS IS".
  NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED. 
  YOU USE AT YOUR OWN RISK. THE AUTHOR WILL NOT BE LIABLE 
  FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND 
  OF LOSS WHILE USING OR MISISING THIS SOFTWARE.

- You may not use, copy, emulate, clone, rent, lease, sell, modify,
  decompile, disassemble, otherwise reverse engineer, or transfer the
  licensed program, or any subset of the licensed program, except as 
  provided for in this agreement.  Any such unauthorized use shall 
  result in immediate and automatic termination of this license and
  may result in criminal and/or civil prosecution.

  All rights not expressly granted here are reserved by
  Author.

- Installing and using Naiveshark agent signifies acceptance of 
  these terms and conditions of the license.

- If you do not agree with the terms of this license you must remove
  Naiveshark agent files from your storage devices 
  and cease to use the product.
