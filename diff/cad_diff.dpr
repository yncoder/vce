program cad_diff;

{%File '..\vcs_client\test.inc'}

uses
  Forms,
  sysutils,
  u_vcs_diff_wrapper in '..\vcs_client\u_vcs_diff_wrapper.pas',
  U_ShellOp in '..\U_ShellOp.pas',
  U_Msg in '..\U_Msg.pas',
  U_Const in '..\U_Const.pas',
  u_vcs_portable_const in '..\vcs_client\u_vcs_portable_const.pas',
  u_vcs_Ini in '..\vcs_client\u_vcs_Ini.pas',
  u_vcs_ico_const in '..\vcs_client\u_vcs_ico_const.pas',
  u_file_manager_VCS_const in '..\u_file_manager_VCS_const.pas',
  u_ms_word_compare in '..\formats\word\u_ms_word_compare.pas',
  u_ms_ole_common in '..\formats\ms_common\u_ms_ole_common.pas',
  vcs_diff_form in '..\vcs_client\vcs_diff_form.pas' {Diff_F},
  u_multi_page_const in '..\formats\multi_page\u_multi_page_const.pas',
  visio_converter in '..\formats\visio\visio_converter.pas',
  u_image_utils in '..\vcs_client\u_image_utils.pas',
  U_About_CAD_Diff in 'U_About_CAD_Diff.pas' {About_F},
  U_URLs in '..\U_URLs.pas',
  u_vcs_diff_options_const in '..\vcs_client\u_vcs_diff_options_const.pas',
  U_Cad_Diff_File_Select in 'U_Cad_Diff_File_Select.pas' {File_Select_F},
  vcs_diff_frame_const in '..\vcs_client\vcs_diff_frame_const.pas',
  kompas_converter in '..\formats\kompas\kompas_converter.pas',
  KsTLB in '..\formats\kompas\KsTLB.pas';

{$R *.res}

function Check_File_Path( const Arg_FileName : string ) : boolean;
begin
  if FileExists( Arg_FileName )
    then
      Result := True
    else
      begin
        Result := False;
        Show_Err( cap( cap_File_Not_Found ) + ret + Arg_FileName );
      end;
end;

procedure Run_Diff( Arg_FN1, Arg_FN2 : string );
var left_file, right_file : string;
    Convertable_Formats : T_Convertable_Formats;
    Multipage_Formats : T_Multipage_Formats;
    Diff_Action_desc : T_Diff_Action_desc;
begin
  if not( Check_File_Path( Arg_FN1 ) and Check_File_Path( Arg_FN2 ) )
    then
      Exit;

  (* �������� ����� � ������� ������ *)
  left_file  := ExpandFileName( Arg_FN1 );
  right_file := ExpandFileName( Arg_FN2 );
  
  case Check_Diff_Viewer_Kind( right_file, Convertable_Formats, Multipage_Formats, Diff_Action_desc ) of
    dvk_MSWord : Run_MSWord_Compare( left_file, right_file );
    dvk_Multipage_Convert : begin
                              (* �������� ����� ����, �.�. �� �������� ��������� ��������������� ������� ���������� *)
                              Protect_File( left_file, sysutils.ExtractFileName( right_file ) );
                              Show_Diff( False, left_file, 'Base version', right_file, '', cf_None, Multipage_Formats );
                            end;
    dvk_Internal_Image : Show_Diff( False, left_file, 'Base version', right_file, '' );
    dvk_Internal_Image_Convert : Show_Diff( False, left_file, 'Base version', right_file, '', Convertable_Formats );
    (* ����� - ������� ���� (��������, ���-�� ������� ��������� �� ��������� pristine), ������ - �������  *)
    dvk_External, dvk_None :

    begin
      ShowMsg( 'File format is not supported!' );

      Show_About;
    end;
     else Show_About;
   end;

end;

var Arg_FN1, Arg_FN2 : string;


begin
  Application.Initialize;

  if not ( Prepare_Image_bins )
    then
      begin
        Show_Err( 'Application components not found (imagemagick)!' + ret + 'Please, check for you have unpack application properly!' );
        Application.Terminate;
        Exit;
      end
    else
      begin
        Arg_FN1 := ParamStr(1);
        Arg_FN2 := ParamStr(2);
        //  ShowMsg(  Arg_FN1 + ret + Arg_FN2 );

        if ( Arg_FN1 = '' ) or ( Arg_FN2 = '' )
          then
            begin
              //Show_About;
              if not File_Select( Arg_FN1, Arg_FN2 )
                then
                  Exit;
            end;

        Refresh_Temp_Path( False );
        Application.Run;
        Run_Diff( Arg_FN1, Arg_FN2 );
      end;
end.
