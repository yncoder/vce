unit U_About_CAD_Diff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TAbout_F = class(TForm)
    Panel1: TPanel;
    ProductName_L: TLabel;
    Version_L: TLabel;
    Copyright: TLabel;
    www_main_l: TLabel;
    OK_B: TButton;
    Info_M: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure www_main_lClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_BClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Show_About;

implementation

uses U_Msg, U_URLs;

{$R *.dfm}

procedure Show_About;
begin
  with TAbout_F.Create( Application ) do ShowModal;
end;

procedure TAbout_F.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TAbout_F.FormCreate(Sender: TObject);
begin
  ProductName_L.Caption := program_title;
  www_main_l.Caption := url_main_site_root;
  Version_L.Caption := 'Version 1.4';
  Info_M.Lines.Text := CMD_MSG;
end;

procedure TAbout_F.OK_BClick(Sender: TObject);
begin
  Close;
end;

procedure TAbout_F.www_main_lClick(Sender: TObject);
begin
  OpenMainSite;
end;

end.
