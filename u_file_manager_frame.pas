unit u_file_manager_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, ActnList, Clipbrd,
  u_file_manager_VCS_const, u_file_manager_VCS_Base,
  U_API_Wrapper_Const, 
  fisFileNotification, Menus, ImgList, ToolWin, StdActns;

type T_Frame_Mode = ( fm_Testing_Server, fm_View_Checkout  );

type
  TFile_Manager_Frame = class(TFrame)
    Panel1: TPanel;
    Up_B: TButton;
    Files_ListView: TListView;
    vcs_AL: TActionList;
    Stage_A: TAction;
    Revert_A: TAction;
    Commit_A: TAction;
    Update_A: TAction;
    View_Log_A: TAction;
    Open_in_explorer_A: TAction;
    Up_folder_A: TAction;
    Resolve_A: TAction;
    View_Item_Log_A: TAction;
    PopupMenu1: TPopupMenu;
    Viewitemlog1: TMenuItem;
    Show_Item_In_Explorer_A: TAction;
    Showiteminexplorer1: TMenuItem;
    Drill_Down_Folder_A: TAction;
    DrillDownFolderA1: TMenuItem;
    Diff_A: TAction;
    Diff1: TMenuItem;
    Edit_Project_A: TAction;
    Copy_Path2Clipboard_A: TAction;
    Copypathtoclipboard1: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton5: TToolButton;
    Current_Path_E: TEdit;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    Close_A: TAction;
    procedure Files_ListViewDblClick(Sender: TObject);
    procedure View_Log_Execute(Sender: TObject);
    procedure Stage_AExecute(Sender: TObject);
    procedure Revert_AExecute(Sender: TObject);
    procedure Commit_AExecute(Sender: TObject);
    procedure Update_AExecute(Sender: TObject);
    procedure Up_folder_AExecute(Sender: TObject);
    procedure Files_ListViewChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure Open_in_explorer_AExecute(Sender: TObject);
    procedure Resolve_AExecute(Sender: TObject);
    procedure View_Item_Log_AExecute(Sender: TObject);
    procedure Show_Item_In_Explorer_AExecute(Sender: TObject);
    procedure Drill_Down_Folder_AExecute(Sender: TObject);
    procedure Diff_AExecute(Sender: TObject);
    procedure Edit_Project_AExecute(Sender: TObject);
    procedure Copy_Path2Clipboard_AExecute(Sender: TObject);
    procedure Close_AExecute(Sender: TObject);
  private
    { Private declarations }
    fFrame_Mode : T_Frame_Mode;
    fVCS_Wrapper_Base : T_VCS_Wrapper_Base;
    FileNotification : TfisFileNotification;
    (* ���� ����������, ��� ���� vcs �������. ��. on_VCS_Busy_Notify *)
    fBusy_Flag : boolean;

    (* ������� ��� ���� ������� ���������� � ����������� diff, � ���� ������������� ��������� �����, ��������� �������� ������� *)
    Lock_Diff_Flag : boolean;

    procedure Fill_Files;
    procedure Refresh_Files;

    procedure Set_Command_Enable( Arg_Disable_All : boolean = False );
    procedure on_VCS_Busy_Notify( Arg_Busy : boolean );

    procedure Subscribe_FileNotification;
    procedure FileNotification_DirectoryChanged(Sender: TObject);
    procedure Unsubscribe_FileNotification;
    procedure Set_Update_Status( Arg_Delta : integer = -1 );
    procedure Load_Icons;
  public
    { Public declarations }
    property Busy_Flag : boolean read fBusy_Flag;
    (* Arg_Frame_Mode - �����
       Arg_Server_Url - ����
       Arg_ID - �� �������
       Arg_Current_Folder - ������� ������������� ���� � ������� (�������� � ��� ������ ���������, �.�. �� ������ � ������ ������� )
       Arg_Root - ����� ��� �������
    *)

    procedure Init_FM( Arg_Frame_Mode : T_Frame_Mode;
                       Arg_Projects_List : T_Projects_List;
                       Arg_Server_Url : string; Arg_Auth_Params : T_Auth_Params; Arg_ID : integer; Arg_Root : string = ''; Arg_Current_Folder : string = '' );
    function Test_VCS_Link( Arg_Server_url : string; out Out_Repo_Info : T_Repo_Info ) : T_Connect_Results;
    function Get_Current_Folder : string;
    procedure Check_Outdated;
    procedure Refresh_Toolbar;
    procedure Close_FM;
  end;

implementation

uses U_ShellOp, u_file_manager_VCS_log, u_file_manager_VCS_commit_dialog, U_Msg, u_file_manager_VCS,
  U_Const, vcs_diff_form, u_vcs_diff_wrapper, vcs_password_form, u_vcs_Ini,
  u_vcs_ico_const, vcs_options_form, u_vcs_diff_options_const,
  u_vcs_diff_wrapper_previewer;

{$R *.dfm}

procedure Show_Options_Diff;
begin
  v_Show_Diff_Options_Event;
end;

{ TFrame1 }

procedure TFile_Manager_Frame.FileNotification_DirectoryChanged;
begin
  (* �������� ����������� � �������� ������� � ������������� ����� *)
  if not fBusy_Flag
    then
      Refresh_Files;
end;

procedure TFile_Manager_Frame.Files_ListViewChange;
begin
  if ( not fBusy_Flag ) and ( Change = ctState )
    then
      begin
        Set_Command_Enable;
      end;
end;

procedure TFile_Manager_Frame.Files_ListViewDblClick(Sender: TObject);
var i : integer;
begin
  i := Files_ListView.ItemIndex;
  if i >= 0
    then
      begin
        if fVCS_Wrapper_Base.Folder_Files_Array[ i ].f_folder_flag
          then
            begin
              Drill_Down_Folder_A.Execute;
            end
          else
            Show_Item_In_Explorer_A.Execute;
      end;
end;

procedure TFile_Manager_Frame.Fill_Files;
var i : integer;
    fn : string;
    VCS_State : T_File_VCS_States;
begin
  Current_Path_E.Text := fVCS_Wrapper_Base.Current_Related_Folder;

  Files_ListView.Clear;
  for i := 0 to High( fVCS_Wrapper_Base.Folder_Files_Array ) do
    begin
      fn := fVCS_Wrapper_Base.Folder_Files_Array[ i ].f_file_name;
      if fVCS_Wrapper_Base.Folder_Files_Array[ i ].f_folder_flag
        then
          fn := '[' + fn + ']';
      with Files_ListView.Items.Add do
        begin
          Caption := fn;
          VCS_State := fVCS_Wrapper_Base.Folder_Files_Array[ i ].f_state;
          if Small_FileState_ImageList_Done
            then
              ImageIndex := integer( VCS_State );
              
          SubItems.Add( T_File_VCS_State_Titles[ VCS_State ] );
        end;
    end;
end;

function TFile_Manager_Frame.Get_Current_Folder;
begin
  Result := fVCS_Wrapper_Base.Current_Related_Folder;
end;

procedure TFile_Manager_Frame.Init_FM;
var root_path_len : integer;
    i : integer;
begin
  fBusy_Flag := False;
  fFrame_Mode := Arg_Frame_Mode;
  Lock_Diff_Flag := False;

  Load_Icons;

  if Interface_Image_List_Done
    then
      begin
        Open_in_explorer_A.ImageIndex := integer( iii_Open_in_Explorer );
        
        Edit_Project_A.ImageIndex := integer( iii_Edit_Project );

        View_Log_A.ImageIndex := integer( iii_View_Log );
        Revert_A.ImageIndex := integer( iii_Revert );
        Diff_A.ImageIndex := integer( iii_Diff );
        Update_A.ImageIndex := integer( iii_Update );
        Commit_A.ImageIndex := integer( iii_Commit );
        Stage_A.ImageIndex := integer( iii_Stage );
        Resolve_A.ImageIndex := integer( iii_Resolve );
        Close_A.ImageIndex := integer( iii_Close );
        //ActionList1.Images := Ini_Strore.Interface_Image_List;
      end;

  Refresh_Toolbar;

  (* ���������� ����� ��� ������ *)
  for i := 0 to vcs_AL.ActionCount - 1 do
    with ( vcs_AL.Actions[i] as TAction ) do
      Hint := Caption;  

  root_path_len := Length( Arg_Root );
  if ( root_path_len > 0 ) and ( Arg_Root[ root_path_len ] = '\' )
    then
      Delete( Arg_Root, root_path_len, 1 );

  if not Assigned( fVCS_Wrapper_Base )
    then
      begin
        fVCS_Wrapper_Base := T_VCS_SVN_Wrapper.Create( Self, on_VCS_Busy_Notify );
      end;

  if fVCS_Wrapper_Base.Set_Params( Arg_Projects_List, Arg_Server_Url, Arg_ID, Arg_Auth_Params, Arg_Current_Folder, Arg_Root )
    then
      begin
        case fFrame_Mode of
          fm_Testing_Server : ;
          fm_View_Checkout :
            begin
              Refresh_Files;
              Subscribe_FileNotification;
              Check_Outdated;
            end;
        end;
      end;
end;

procedure TFile_Manager_Frame.Load_Icons;
begin
  Files_ListView.SmallImages := Small_FileState_ImageList;
end;

procedure TFile_Manager_Frame.on_VCS_Busy_Notify;
begin
  (* �������� - ����� �� ��� � ������ "������" *)
  if fBusy_Flag = Arg_Busy
    then
      Exit;
      
  fBusy_Flag := Arg_Busy;
  
  if fBusy_Flag
    then
      Set_Command_Enable( True )
    else
      Set_Command_Enable;
end;

procedure TFile_Manager_Frame.Open_in_explorer_AExecute(Sender: TObject);
begin
  StartExplorer( fVCS_Wrapper_Base.Current_Folder );
  //Show_in_Explorer( fVCS_Wrapper_Base.Current_Folder );
end;

procedure TFile_Manager_Frame.View_Item_Log_AExecute(Sender: TObject);
var i : integer;
    full_file_name,
    canonical_full_file_name
     : string;

    Commits_Log_A : T_Commits_Log_A;
begin
  with Files_ListView do
    begin
      i := ItemIndex;
      if i >= 0
        then
          begin
            SetLength( Commits_Log_A, 0 );

            full_file_name := fVCS_Wrapper_Base.Get_File_FullPath( i );
            canonical_full_file_name := fVCS_Wrapper_Base.Get_File_FullPath( i, True );

            if fVCS_Wrapper_Base.Get_VCS_History( Commits_Log_A, canonical_full_file_name )
              then
                Show_VCS_Log( Commits_Log_A, fVCS_Wrapper_Base, full_file_name );
                
            SetLength( Commits_Log_A, 0 );
          end;
    end;
end;

procedure TFile_Manager_Frame.View_Log_Execute(Sender: TObject);
var Commits_Log_A : T_Commits_Log_A;
    full_file_name : string;
begin
  SetLength( Commits_Log_A, 0 );

  full_file_name := fVCS_Wrapper_Base.VCS_Folder;
  if fVCS_Wrapper_Base.Get_VCS_History( Commits_Log_A, full_file_name )
    then
      Show_VCS_Log( Commits_Log_A, fVCS_Wrapper_Base, full_file_name );

  SetLength( Commits_Log_A, 0 );
end;

procedure TFile_Manager_Frame.Unsubscribe_FileNotification;
begin
  if Assigned( FileNotification )
    then
      begin
        FileNotification.Stop;
        FileNotification.Free;
        FileNotification := nil;
      end;
end;

procedure TFile_Manager_Frame.Update_AExecute(Sender: TObject);
begin
  case fVCS_Wrapper_Base.Update of
    tur_Fail : ;
    tur_Ok : Set_Update_Status;
    tur_Conflict :
      begin
        (* ���� ���� ��������� - �� ��������� ������ ������ *)
        ShowMessage( cap( cap_Update_Conflicts ) );
      end;
  end;
  Refresh_Files;
end;

procedure TFile_Manager_Frame.Up_folder_AExecute(Sender: TObject);
begin
  fVCS_Wrapper_Base.Up2Folder;
  Fill_Files;
  Set_Command_Enable;
end;

procedure TFile_Manager_Frame.Refresh_Files;
begin
  fVCS_Wrapper_Base.Scan_Folder;
  Fill_Files;
  Set_Command_Enable;
end;

procedure TFile_Manager_Frame.Refresh_Toolbar;
begin
  Set_ToolBar( ToolBar1 );
end;

procedure TFile_Manager_Frame.Resolve_AExecute(Sender: TObject);
var i : integer;
begin
  with Files_ListView do
    begin
      i := ItemIndex;
      if i >= 0
        then
          begin
            if fVCS_Wrapper_Base.Resolve( i )
              then
                begin
                  Refresh_Files;
                  ShowMsg( cap( cap_Conflict_Resolved ) );
                end;
          end;
    end;
end;

procedure TFile_Manager_Frame.Revert_AExecute(Sender: TObject);
var i : integer;
    fn : string;
    file_exist : boolean;

procedure Try_Revert;
begin
  if fVCS_Wrapper_Base.Revert( i )
    then
      Refresh_Files;  
end;

begin
  i := Files_ListView.ItemIndex;
  if i >= 0
    then
      begin
        fn := fVCS_Wrapper_Base.Get_File_FullPath( i );

        file_exist := FileExists( fn );

        if file_exist
          then
            begin
              if ( Show_Q( cap( cap_Confirm_Revert ), False ) )
                then
                  begin
                    Delete_File_To_TrashBin( fn );

                    Try_Revert;
                  end;
            end
          else
            Try_Revert;
      end;
end;

procedure TFile_Manager_Frame.Set_Command_Enable;

procedure Disabable_All;
var i : integer;
begin
  for i := 0 to vcs_AL.ActionCount - 1 do
    ( vcs_AL.Actions[ i ] as TAction ).Enabled := False;
end;

var fr : T_FileRec;
    something_selected : boolean;
begin
  (* ������� ����� �������� � ��� �������� ���� ����� - ����� ������������ �� ���� *)
  if ( Arg_Disable_All ) or ( not Assigned( fVCS_Wrapper_Base ) )
    then
      begin
        Disabable_All;
        Exit;
      end;

  something_selected := False;

  with Files_ListView do
    begin
      if ( ItemIndex >=0 ) and ( ItemIndex < Length( fVCS_Wrapper_Base.Folder_Files_Array ) )
        then
          begin
            fr := fVCS_Wrapper_Base.Folder_Files_Array[ ItemIndex ];
            something_selected := True;
          end;
    end;

  Stage_A.Enabled := something_selected and ( fr.f_state in Addable_states );

  Revert_A.Enabled := something_selected and ( fr.f_state in Revertable_states );

  Resolve_A.Enabled := something_selected and ( fr.f_state in Resolvable_states );

  Commit_A.Enabled := True;

  Update_A.Enabled := true;
  View_Log_A.Enabled := true;

  View_Item_Log_A.Enabled := something_selected and ( fr.f_state in Logable_states );
  Show_Item_In_Explorer_A.Enabled := something_selected;
  Copy_Path2Clipboard_A.Enabled := something_selected;
  Drill_Down_Folder_A.Enabled := something_selected and fr.f_folder_flag;
  Diff_A.Enabled := something_selected and ( not fr.f_folder_flag ) and ( fr.f_state in Diffable_states );

  Open_in_explorer_A.Enabled := true;
  Edit_Project_A.Enabled := true;
  Up_folder_A.Enabled := fVCS_Wrapper_Base.Up2Folder_avail;
  Close_A.Enabled := not fBusy_Flag;
end;

procedure TFile_Manager_Frame.Set_Update_Status;
var caption : string;
begin
  if Arg_Delta = -1
    then
      begin
        caption := 'Update';
      end
    else
      begin
        caption := 'Update ' + '(' + IntToStr( Arg_Delta ) + ' new rev)';
      end;
  Update_A.Caption := caption;
  Update_A.Hint := caption;
end;

procedure TFile_Manager_Frame.Show_Item_In_Explorer_AExecute;
var i : integer;
begin
  i := Files_ListView.ItemIndex;
  if i >= 0
    then
      Show_in_Explorer( fVCS_Wrapper_Base.Get_File_FullPath( i ) );
end;

procedure TFile_Manager_Frame.Subscribe_FileNotification;
begin
  FileNotification := TfisFileNotification.Create( Self );
  FileNotification.Directory := fVCS_Wrapper_Base.VCS_Folder;
  FileNotification.OnDirectoryChanged := FileNotification_DirectoryChanged;

  FileNotification.Subtree := True;
  FileNotification.NotificationType := [noFilename, noDirname, noLastWrite, noCreation
  { noAttributes, noSize,  noLastAccess }
  ];

  FileNotification.Start;
end;

function TFile_Manager_Frame.Test_VCS_Link;
begin
  Result := fVCS_Wrapper_Base.Test_Server_Url( Arg_Server_url, Out_Repo_Info );
end;

procedure TFile_Manager_Frame.Stage_AExecute(Sender: TObject);
var i : integer;
begin
  with Files_ListView do
    begin
      i := ItemIndex;
      if i >= 0
        then
          begin
            if fVCS_Wrapper_Base.Add_File( i )
              then
                Refresh_Files;
          end;
    end;
end;

procedure TFile_Manager_Frame.Check_Outdated;
var Server_Rev, Working_Copy_Rev, delta : t_revision_id_type;
begin
  if fBusy_Flag
    then
      Exit;
      
  delta := 0;
  if fVCS_Wrapper_Base.Check_Working_Copy_Outdated( Server_Rev, Working_Copy_Rev ) = tcr_Ok
    then
      begin
        if Server_Rev > Working_Copy_Rev
          then
            delta := Server_Rev - Working_Copy_Rev;
      end;
  if delta = 0
    then
      Set_Update_Status
    else
      Set_Update_Status( delta );
end;

procedure TFile_Manager_Frame.Close_AExecute(Sender: TObject);
begin
  ( Owner as tform ).Close; 
end;

procedure TFile_Manager_Frame.Close_FM;
begin
  Unsubscribe_FileNotification;

  fVCS_Wrapper_Base.Free;
  fVCS_Wrapper_Base := nil;
end;

procedure TFile_Manager_Frame.Commit_AExecute(Sender: TObject);
var commit_msg : string;
    file_list : T_Commited_Files_Array;
    MSG_Changed : boolean;
    do_commit : boolean;
    FileList_BeforeCommit_results : T_Get_FileList_BeforeCommit_results;
begin
  (* ������������ �������� ������ ��� ������ - �������� ����� *)
  FileList_BeforeCommit_results := fVCS_Wrapper_Base.Get_FileList_BeforeCommit( file_list );

  case FileList_BeforeCommit_results of
    tgfl_bc_None : ShowMessage( cap( cap_Nothing_to_Commit ) );
    tgfl_bc_Error : ;
    tgfl_bc_Commit_Yes :
      begin
        if Length( file_list ) = 0
          then
            ShowMessage( cap( cap_Nothing_to_Commit ) )
          else
            begin
              commit_msg := fVCS_Wrapper_Base.Stored_Commit_Msg;

              (* ������ ��������� �� �����������, ����� ��������� *)
              if commit_msg = ''
                then
                  begin
                    commit_msg := Ini_Strore.Str[ ii_Default_Commit_MSG ];
                  end;

              do_commit := Request_Commit_Confirm( file_list, commit_msg, MSG_Changed );

              (* ��������� ���������� ��������� (���� ���� ������������� ���-�� ����, ��� �������������� ����� ��� ������ � ����������).
                 ������������� ����� ��������, ���� ������������ ���� �����, ������� ������ ��������� ���������               
               *)
              if ( MSG_Changed ) and
                 ( ( commit_msg <> Ini_Strore.Str[ ii_Default_Commit_MSG ] )
                    or
                   ( commit_msg <> fVCS_Wrapper_Base.Stored_Commit_Msg )
                 )
                then
                  fVCS_Wrapper_Base.Stored_Commit_Msg := commit_msg;

              (* ������? *)
              if do_commit and ( commit_msg <> '' )
                then
                  begin
                    (* ��������� ����� � ����� ������ � ���� ����� *)
                    fVCS_Wrapper_Base.Stored_Commit_Msg := commit_msg;
                    
                    if fVCS_Wrapper_Base.Commit
                      then
                        Refresh_Files;
                  end;
            end;
      end;
    tgfl_bc_Forbid_Conflict :
      begin
        ShowMessage( 'Some files in conflict! Can''t commit! Please resolve conflicts first!' );
      end;
   end;

  SetLength( file_list, 0 );
end;

(* http://stackoverflow.com/questions/21707127/delphi-7-how-to-copy-non-latin-text-to-clipboard-convert-to-unicode *)
procedure SetClipboardText(const Text: WideString);
var
  Count: Integer;
  Handle: HGLOBAL;
  Ptr: Pointer;
begin
  Count := (Length(Text)+1)*SizeOf(WideChar);
  Handle := GlobalAlloc(GMEM_MOVEABLE, Count);
  Try
    Win32Check(Handle<>0);
    Ptr := GlobalLock(Handle);
    Win32Check(Assigned(Ptr));
    Move(PWideChar(Text)^, Ptr^, Count);
    GlobalUnlock(Handle);
    Clipboard.SetAsHandle(CF_UNICODETEXT, Handle);
  Except
    GlobalFree(Handle);
    raise;
  End;
end;

procedure TFile_Manager_Frame.Copy_Path2Clipboard_AExecute;
var i : integer;
    full_fn : WideString;
begin
  with Files_ListView do
    begin
      i := ItemIndex;
      if i >= 0
        then
          begin
            full_fn :=  fVCS_Wrapper_Base.Get_File_FullPath( i );
            SetClipboardText( full_fn );
          end;
    end;
end;

procedure TFile_Manager_Frame.Diff_AExecute(Sender: TObject);

var i : integer;
    FileRec : T_FileRec;
    left_file, right_file : string;
    Convertable_Formats : T_Convertable_Formats;
    Multipage_Formats : T_Multipage_Formats;
    Diff_Action_desc : T_Diff_Action_desc;

begin
  (* ��������� ��������� ���������� ������ *)
  if Lock_Diff_Flag
    then
      Exit;
      
  Lock_Diff_Flag := True;

  try
    i := Files_ListView.ItemIndex;
    if i >= 0
      then
        begin
          FileRec := fVCS_Wrapper_Base.Folder_Files_Array[ i ];
          if not ( FileRec.f_folder_flag ) and ( FileRec.f_state in Diffable_states )
            then
              begin
                (* �������� ����������� ���� ��� ��������� *)
                left_file := fVCS_Wrapper_Base.Get_Pristine_File_Path( i );
                right_file := fVCS_Wrapper_Base.Get_File_FullPath( i );

                case Check_Diff_Viewer_Kind( right_file, Convertable_Formats, Multipage_Formats, Diff_Action_desc ) of
                  dvk_MSWord : Run_MSWord_Compare( left_file, right_file );
                  dvk_Multipage_Convert : begin
                                            (* �������� ����� ����, �.�. �� �������� ��������� ��������������� ������� ���������� *)
                                            Protect_File( left_file, ExtractFileName( right_file ) );
                                            Show_Diff( True, left_file, 'Base version', right_file, '', cf_None, Multipage_Formats );
                                          end;
                  dvk_Internal_Image : Show_Diff( True, left_file, 'Base version', right_file, '' );
                  dvk_Internal_Image_Convert : Show_Diff( True, left_file, 'Base version', right_file, '', Convertable_Formats );
                  (* ����� - ������� ���� (��������, ���-�� ������� ��������� �� ��������� pristine), ������ - �������  *)
                  dvk_External :
                    case Run_External_Diff( Diff_Action_desc, left_file, ExtractFileName( right_file ), right_file, '' ) of
                      //rediff_Ok
                      rediff_NotConfigured : Prepare_No_Diff_MSG( right_file, fVCS_Wrapper_Base, i ); (* ��� �������������� �����������, ���� ��������� ������������ *)
                      rediff_NotFound : Show_Diff_Not_Found( Diff_Action_desc.f_external_tool_path );
                      rediff_NoDiff3 : ;
                      rediff_Fail : {Show_Err( 'Fail to start ' + Diff_Action_desc.f_external_tool_path ) ��� ��� ���� ��������� } ;
                    end;
                  dvk_None : (* ������ �� ��������� *)
                    Prepare_No_Diff_MSG( right_file, fVCS_Wrapper_Base, i );
                 end;
              end;
        end;
  finally
    Lock_Diff_Flag := False;
  end;
end;

procedure TFile_Manager_Frame.Drill_Down_Folder_AExecute(Sender: TObject);
var i : integer;
    FileRec : T_FileRec;
begin
  i := Files_ListView.ItemIndex;
  if i >= 0
    then
      begin
        FileRec := fVCS_Wrapper_Base.Folder_Files_Array[ i ];
        if ( FileRec.f_folder_flag )
          then
            begin
              if FileRec.f_state in Enter_Folder_states
                then
                  begin
                    fVCS_Wrapper_Base.Drill2Folder( i );
                    Fill_Files;
                  end
                else
                  begin
                    if Show_Q( cap( cap_Cant_Enter_Unversioned_Folder ), True )
                      then
                        Show_Item_In_Explorer_A.Execute;
                  end;
            end;
      end;
end;

procedure TFile_Manager_Frame.Edit_Project_AExecute(Sender: TObject);
var //un, pw : string;
    fAuth_Params : T_Auth_Params;
begin
  on_VCS_Busy_Notify( True );
  fAuth_Params := fVCS_Wrapper_Base.Auth_Params;

  //un := fVCS_Wrapper_Base.Auth_Params.f_username;
  //pw := fVCS_Wrapper_Base.Auth_Params.f_password;
  if Show_Credentials( fVCS_Wrapper_Base.VCS_URL, fAuth_Params.f_username, fAuth_Params.f_password )
    then
      begin
        fVCS_Wrapper_Base.Auth_Params := fAuth_Params;
      end;
  on_VCS_Busy_Notify( False );
end;

end.
