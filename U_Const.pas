unit U_Const;

interface

var Appl_Path : string; (* ���� � ��������� *)
    AppData_Path : string; (* ���� � ����� ������, �������� � ���� (Application Data) *)
    Temp_Path : string; (* ��������� ����� *)

(* �������� ������ � ��������� ����� *)
procedure Refresh_Temp_Path( Arg_Use_Ini : boolean );

(* � ����� ������� ��������� �������� - ������������ ���� � ����� Data � ������������ exe,
   ���� �� ��� - �������, ���� ���� - ��������� ������ �� ������.
   ���� ��� �� - ���������� ����.
   ���� ���� - �� ������ ������
    *)
function Test_Appl_Path_Data_Folder_Write_Access : string;

const internal_Temp_Folder = 'Naiveshark_tmp';
      AppData_SubFolderName = 'Naiveshark';

const Slash = '\';

var readme_full_path : string;
    ico_full_path : string;

(* �������� ������ � ������� �����-�������� � ������� ������ �����������
   �������� �������� ��������
   12-01-2015_11#00#36  *)
function GetNowAsFileName : string;

procedure OpenURL( Arg_Url : string );

(* ������� ���� �������
   http://stackoverflow.com/questions/3690608/simple-string-hashing-function *)
//function FNV1aHash( const Arg_Buff : Pointer; Arg_Size : integer ): LongWord;
function FNV1aHash( Arg_Buff : string ): LongWord;

function Encrypt_B64( Arg_S : string ) : string;

function Decrypt_B64( Arg_S : string ) : string;

function Get_ini_file_name : string;

function Get_Big_Rnd : integer;

(* ���� ��� ��������� - �� ������ ��������� ��� �����. ���� True - �� � temp ����� *)
function Get_Rnd_Temp_File_Name( Arg_With_Path : boolean = False ) : string;

function Prepare_Readme_Path : boolean;

function Prepare_Ico_Path : boolean;

implementation

uses SysUtils, Forms, StrUtils, ShellAPI, Windows, U_ShellOp,
     IdCoderMIME, u_vcs_Ini, u_vcs_portable_const, U_Msg;

const naiveshark_ini_file_name = 'cfg.ini';
      vcs_ini_file_name = 'vcs_cfg.ini';

(* �������� ��� ������ ���������� *)
function Get_ini_file_name;
begin
  {$include directives.inc}

  {$ifdef VCS_Client}
    Result := vcs_ini_file_name;
  {$else}

    {$ifdef Naiveshark_agent}
      Result := naiveshark_ini_file_name;
    {$else}
      {$ifdef CAD_Diff_App}
       Result := '';
      {$endif}
      {$else}
        ��� ���������� ������ ������������, �� ��� ���������� ������ ���� ��� �������� �� VCS_Client �� Naiveshark_agent
    {$endif}

  {$endif}
end;

function GetNowAsFileName : string;
var s : string;
begin
  s := DateTimeToStr( now );
  s := ReplaceText( s, '.', '-' );
  s := ReplaceText( s, ' ', '_' );
  result := ReplaceText( s, ':', '#' );
end;

procedure OpenURL;
begin
  ShellExecute(0, 'OPEN', PChar(Arg_Url), '', '', SW_SHOWNORMAL);
end;

function FNV1aHash;
var i: Integer;
const FNV_offset_basis = 2166136261;
      FNV_prime = 16777619;

begin
  //FNV-1a hash
  Result := FNV_offset_basis;
  for i := 1 to Length( Arg_Buff ) do
    Result := (Result xor Byte( Arg_Buff[i])) * FNV_prime;
end;


const Encrypt_Key = 30872;

(* http://www.codeface.com.br/?p=1679 *)
function EncryptStr(const S: String; Key: Word): String;
var I: Integer;
const C1 = 53761;
      C2 = 32618; 
begin
  Result := S;
  for I := 1 to Length(S) do begin
    Result[I] := char(byte(S[I]) xor (Key shr 8));
    Key := (byte(Result[I]) + Key) * C1 + C2;
  end;
end;
 
function DecryptStr(const S: String; Key: Word): String;
var I: Integer;
const C1 = 53761;
      C2 = 32618;
begin
  Result := S;
  for I := 1 to Length(S) do begin
   Result[I] := char(byte(S[I]) xor (Key shr 8));
   Key := (byte(S[I]) + Key) * C1 + C2;
  end;
end;

function Encrypt_B64( Arg_S : string ) : string;
begin
  Result := TIdEncoderMIME.EncodeString( EncryptStr( Arg_S, Encrypt_Key ) );
end;

function Decrypt_B64( Arg_S : string ) : string;
begin
  Result := DecryptStr( TIdDecoderMIME.DecodeString( Arg_S ), Encrypt_Key );
end;

function Get_Big_Rnd;
begin
  Result := Random( MaxInt - 2 );
end;

function Get_Rnd_Temp_File_Name;
begin
  Result := 'tempfile_#_' + IntToStr( Get_Big_Rnd ) + '_' + IntToStr( GetTickCount );
  if Arg_With_Path
    then
      Result := Temp_Path + Result;
end;

function Prepare_Readme_Path : boolean;
begin
  (* ���� readme.txt *)
  readme_full_path := Appl_Path + 'readme.txt';

  Result := FileExists( readme_full_path );
end;

function Prepare_Ico_Path : boolean;
begin
  ico_full_path := Appl_Path + 'vcs_res\';
  Result := FileExists( ico_full_path + 'icon list.txt' );
end;

procedure Refresh_Temp_Path;
var temp_root : string; 
begin
  {$message warn '����� �������� ����������� ������ � �����.' }

  (* ���� ������� ��������� - ���������� �� *)
  if Arg_Use_Ini
    then
      begin
        if Ini_Strore.Temp_Folder_Flag
          then
            temp_root := Get_System_Temp_Path
          else (* ����� - � ����� ������� *)
            temp_root := Appl_Path;
      end
    else
      temp_root := Get_System_Temp_Path;
      
  if ( DirectoryExists( temp_root + internal_Temp_Folder ) )
     or
     ( CreateDir( temp_root + internal_Temp_Folder ) )
    then
      Temp_Path := temp_root + internal_Temp_Folder + Slash
    else
      begin
        Temp_Path := temp_root;
      end;
end;

function Test_Appl_Path_Data_Folder_Write_Access;

function Test_Write( Arg_path : string ) : boolean;
var temp_file : string;
    f : file;
    error : integer;
begin
  Result := False;
  try
    if DirectoryExists( Arg_path )
      then
        begin
          temp_file := Arg_path + 'test_file_' + IntToStr( Random( maxint - 1  ) ) + '.$$$';
          {$I-}

          AssignFile( f, temp_file );
          Rewrite( f );
          CloseFile( f );
          error := IOResult;
          {$I+}
          if error <> 0
            then
              begin
                Exit;
              end;

          DeleteFile( pansichar( temp_file ) );
        end
      else
        ForceDirectories( Arg_path );

    Result := True;
  except
    //on EInOutError do
    //  result := '';
  end;
end;

var s : string;

begin
  Result := '';
  s := Appl_Path + 'Data' + Slash;
  if Test_Write( s )
    then
      Result := s
    else
      begin
        Result := '';
        Show_Err( cap( cap_Cant_Write_On_Exe_Data ) );
      end;
end;

initialization

  (* ���������� ������� ������� ��������� *)
  Appl_Path := ExtractFilePath(Application.EXEName);

  (* ���������� ����� ������� ���� � ��� ���������. *)
  Detect_Desktop_and_MyDoc_Folder;
end.
