04/08/2015
�� ������ "���������� ������ ������� ���� ��� ����� �������� - naiveshark � vcs"
https://bitbucket.org/yncoder/desktopclient/issues/56/naiveshark-vcs

������� ��������� ������� - ���� ����� include_directives, � ������� 2 ����� 
naiveshark_agent
vcs_client

� ����� ���� � ����� ������ directives.inc

� ���� ���� ����������� {$include directives.inc} � {$ifdef}, ������� ��������� ������� ��� ��� ���� ��������, ������������ ���������, ����������� ��� ������ �������. ����������� include ������������ �.�. ������ �������� ��������� {$ifdef} �������� ������ � ������ ������ ������.