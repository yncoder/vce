object AboutBox: TAboutBox
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 277
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 128
    Top = 246
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKButtonClick
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 6
    Width = 353
    Height = 235
    ActivePage = Credits_TSh
    TabOrder = 1
    object Credits_TSh: TTabSheet
      Caption = 'Credits'
      ExplicitWidth = 281
      ExplicitHeight = 165
      object Panel1: TPanel
        Left = 3
        Top = 8
        Width = 339
        Height = 196
        BevelInner = bvRaised
        BevelOuter = bvLowered
        ParentColor = True
        TabOrder = 0
        object ProgramIcon: TImage
          Left = 8
          Top = 8
          Width = 96
          Height = 96
          Picture.Data = {
            0A544A504547496D6167657E0F0000FFD8FFE000104A46494600010101012C01
            2C0000FFDB0043000302020302020303030304030304050805050404050A0707
            06080C0A0C0C0B0A0B0B0D0E12100D0E110E0B0B1016101113141515150C0F17
            1816141812141514FFDB00430103040405040509050509140D0B0D1414141414
            1414141414141414141414141414141414141414141414141414141414141414
            14141414141414141414141414FFC00011080060006003012200021101031101
            FFC4001C0000020301010101000000000000000000060704050803090201FFC4
            00471000010303030201070708060B0000000001020304050611001221073113
            0822415171D2D3141532426181A52325526584A4B4C1243343B1B5C335444553
            556264747591A1FFC4001B010002030101010000000000000000000004050203
            06070108FFC40035110001030203050506060300000000000001000203041105
            21311241516191063271D1F01314225281B115424482A1C12472E1FFDA000C03
            010002110311003F006EF54BAA95BAA5ED5B5F8A85B8C4F9119BF1DB4BA865B6
            DD53694210B05238485138C92A3CE8459EA757D2E04B8F4323FF001D1BE1EA5D
            ED1775DB702BF4AAB38FEF4E8FE5A14951319E35F2DE253CEFAB95C5E7BC77F3
            5D969628842C6868D026252FA8151904789F22567F57C7F734614CB8DD9291BD
            880A3FF60C7B9A46D3672A3BA12A3A60DBB530ADB9571A4AF9E71F9CF5288744
            CF9426BC096DBD8DF0E02BF6167DCD1353A1C27F1BE9B4F57EC4D7BBA03A5CC4
            361256B09048482A38E49C01F792068F288BDC53A11D5338FCE7A94AE7606E81
            44ACB3162EEF0E9F4F4FEC4CFBBA06ABD75C8DBBC38F0538FF00A063DCD195D2
            FA594B8A5282529049513800694B5AAAB32990F30F21F65C1B90E36A0A4A87AC
            11C1D150CF3BB3DB3D4A953B03B50A3542F7A921CD8D084924E3FD1F1FDCD4DA
            75CB547B6EF10D59FD5F1FDCD0B4160CC9056464678D1AD2298709E345BAA246
            8EF1EA5307B2360B6C8E8AEA054253C06F6611FD818F7357D49ACCBB6AAF48A9
            C2D911EF9C22C67511D01A6DF69E7D0D292B4270938F137038C8291CF7D70A65
            2F84F1A9F5A8059854E5E3B55E99FC731AB30FAD9DB5D096BCF786FE693D4063
            98E69093F7744DD70D6158FA5529E7F7C7B42F2A0E73C698B70C1F16A9505E3B
            D46A1FC748D0F49A61E78D1F5C01AA90733F74653BED1B7C100CC8452720608D
            4CA2563E48BC3AA0809E4A947000F5EBB5EDBE8B6BD66A28042E2437A424800F
            28415763C1EDACB37C75766DFB42A5D1586D5484CB6D3F3BCB49C2482705B6F0
            49DAAEEAFB38E72750A5C2A5AE20B326DEC4F0DFEB9AB65AC6422CECCEE449D5
            2F28B9579DF3445518EFB56DBA8B339A4EE2915392CB814164FF00BB491848F4
            9CABD401541F2ACEB3F546E3816E5A8E522DE995077C08E983192E3A73FA4EBE
            54818009C840EDA47DD546A2D26E4148B6AABF3FB0865AFCA32D01B5C5150F08
            04A9409F341C027E901DF3A7274DFC8FBA9970BF1AA0CBD12D07D043ACBD3E5A
            D992839E084B4952D0A1DC6EDA75B67D2E1F042C2E6B4068F876B9E7720EA4EA
            72F0B059B73A491CE24E675B7ADC955D4B72EC3784EA3F506B156AFCFA6C92D4
            A893EA4B79B0410484804A0020820A47620E9F56EF94A5AB578B129F2612ED44
            3284B2D327F2915B42404A5095A46400903E9252063BE833A83E485D42B3DF99
            316EC5BA9E75C53EFBD0A638FC9756A3CA9497825C7147B9C6E3A5F74D6D0A25
            D5733D4EB9AE366D186D30E38A99292900389206C3BD4900F24F247D123B9D4E
            78692BE01775DADF9741E005C7F0A7049253BB69A333C56EEB3D88B5784C4B83
            2199B15C1943F1D61C4287D8A1C1D3268F45236F9BAF3F3C93294C5CFD53629F
            1AF5A8DA82424B91DCA72427E70524F08217E6729CA86F42FB6319D7A691D866
            98C252565D5A460AD58C9FB4E0019F60D73AC628FF000F9445B77BF2B11FD23F
            DECCE2F65F10696965214AE350EEC5A0D3E0A13FF16A61FDF98D779354FB7035
            435999E3A212739FCE94D3FBF31A5F860DAAE86FF30FBAA9ED7169250FCA81E3
            BB2D78CE6A351FE3E46AB9FA3E73E6E8CE9D03E511A4AB1FED2A97F8848D7755
            1B77D5D31C4A4D9AD94732A71496600B3E75E209A77476F69012ADC28D2D2929
            E082A6949073F6120EB03252129007000C0D6D0F2C6EA84DB5E8F5DB1665ACFB
            71EB5093F36D698989287C6E478A148291B4A4EE49014A38293801635982E97A
            C56EC4B5DBA10AA397838B70D5CBE93F266D001C149C6324ED00024E3715638C
            EE70363E3A505E3BE6E37E5606E6DA6FD77E5AA06A240F7DF879A2FB5D0DF477
            A74C5F0B8A255C95477E4F476143904E40239C8CED5289EE10060F9DA6E74D3C
            9C15D64704DEAC5D758AA541CC8F99603E966343CF64ED2149CE3B809CE71951
            391AE1D42B6A28AEF9329702052A63190A38F0FC4D908359CF1F595DFEDD3F25
            D9EF74E678AF225C66E3B0773F1D6F252A7123B84827957A87A7B693E21893E1
            8DB2C06D23F6B33A801C5A00E1A5CF1BA2A18E39AF1BCE4341C4DB53F648BBCB
            C9B1DE9D54A49E94D62AEDCB8A9DEAA2CA712EB52523BA40484A4A881C254939
            E70A07008AF556DAA67583A20D756290CB716B74D7930ABD1107256094A42FD6
            4A4A90738E50B393E66B57377A512D2A84D72525D72AEEBAA53AA7585A50DF3C
            25391CE07A7593EDABD6026DEF28E0DA928A2541738C7000D9B8A64EFD807000
            2A4818EC0271A850E213D45E57F7985B9FCC1C6C41EB70A5341B0031993483D4
            0BDD679A5D525D12A516A102439126C5752F30FB2ADAB6D69394A81F41046BD2
            AE8F75AD8EACD890EAE1486AA281E04F8E93FD5BC0738FF955C287D871DC1D79
            E34D9967C7E9C55CD4A3D49EBC9C79029C23E7C108F373BB8C0FAF92A23EAE39
            CEB44F93E59C8B36994FB8E257E423E73880CDA4BD1816D7DCA149702F823823
            293C150CE9976820867A6F68FF0085CD3604EFCAE478703C478AAA901326C817
            FE96AF7EAF9CF9DA8E899F297E22739C5469C7F7F8FA0155D4143856AE2D2AB7
            CE154651BB3B65D3D5F88C51FCF587C299FE743FEC139A88C36171E49BD6C784
            29720B879F9CEA7FE2123562E4C8CD7A33F7E835153F9144751BB19A8548FE23
            2755736E2C67CED578B83EFF00378A530C45CD0555794874E29DD68E9DC9A4A5
            2D35598A4CAA64A5000A1E03E8151EC858F34FA079AAC1291AF3025C47E04A7A
            2C96571E4B0B534EB2EA76ADB5A4E14923D0410411AF4B2AF74A9085ED25C50C
            E1208049F564903FF671AC2BD70A949A9F52DEAACDB3A75B6DBC105C6EA0424C
            D524905C011948DC90904A54AC904F7275ADECBD44AE63E9DD9B4660F0BEEFAE
            BD55753008ACEE28E2CEBCA0755BA430FA775A982056288F193449E4729FA470
            71CE00529240E71B54028A31AE954EB15C7457E145BFA9D3D6582089D1807512
            40ECACE40576EE92727D03498BC6E966E7B9973E9D448B6E446D96DA6D88671B
            9692A25C3800038291C7E8E4F249D12D99D68BDEDD7530E952135253EA0811DF
            69C716E93C6DF31492ACF6E4289D3C9B0E6C80ED32E0E7626C5A4EB622E33D48
            E2BD8A7111BB4D8F1B647E89FF007875DEB3D78A646B7BA556555DFAC2D90C4B
            B86A2CA1A8F0D3C02B1851E71E95948071F4BB6933D66A752FA4B6842E95D266
            26A55870A6456E5A39F0D2485107D214B212003CEC05440DC356175795D7572A
            14A76DF1528B6B3291E0BC29D0D489400E36EF754AF0C8EDE6A5247AC697DD2F
            BAA8B6257E654EB76DB57736FC775263CD7028A9F5904BAA52C2B72B1B864E48
            DD918201D5B4945EECC003006B4DC341B924684936196E032BE7E22191CE26E7
            95F803AD87342DAD6F67417E2D9F434A77364416329CE707C34E7FFB9D2EFA73
            E4C753BCFA7F26BD5270522A339BF12931093E184E7216E9233B543CD4E39FAD
            DB00BFE9F6EAE99468311D404B8C476DA5242B70052900F23BF6EFA518ECB1CC
            C1131C0969CFA26987DD8E2E2350867C594DFD6CE8EBA40FBAFDC4B4B9D92E40
            57E270C7F3D523F4DC13C68A7A5917C0AF3AAC7F694F1F8A43D6770D8ED5B11E
            69A553EF039185CF50F923A519C665D44FE252F41551AE9E7CED59752A618F53
            4201EEFD40FE27334BC97349C9273A07156DEBE5F151A38C7B1692A4D42AAA73
            3956872A75752A2B919C0DC88AB395479084BAD28FACA140827EDC6BF26CCEFC
            E872A128A8903D3A853B1CC70734D8A63B21C2C46484AE1E9D5A7576E4C9085D
            B45A429C5C88C7C48E900649536B3903D24A5581E84E963D3C83734CACC4AA5A
            14DA84F92C28B8CBCC4452F0391950C100107D3EBD680A5C542C80E34D3E83DD
            B79B4B8857B52A041FBC69814A99264782875F71C6D380941579A91EA03B0FBB
            5A866386089CC946D9FE2DCF8A53361ED73C3A33B2126683E4A1D49EA4D665D5
            AE19F4DA5CA9EE990F3B35F0A5E4E004A5B642F6E001C288EDC9CEAE7C9CBC96
            1FBCAF6AC4EB8985AAD8B76A522028293B45424B0EA90A40E4F980A72A2339FA
            39E491A8ED07CA3C3E74DAA02184C02C32D36CA0A94E6C690129DCA51529581E
            95294A513DC9249E4E96C9DA5A8746F60686DEC05B771FF8954B4C23233B8411
            56A52464048007000180342550A5E09E34D5AAC1E4F1A14A8401CF1A411CA4E6
            5171BD2DE553B04F1AB7B0A37835759C63F2F4F1F89C3D4F9B0393C6BADAF1BC
            1A8138FF0059A70FC4A26B41863F6AAE3F15399D789CA9AE9A13F7C54EA6DD2D
            6D2AA34CA9D4223B15F752D6F4198F3A95A54A213FDA2810483C0EF9D0BCAE8E
            DE073F9BE30F6D4A28FF00334DDB83A71715897E572547A34AADD3EA329C98DA
            E1146F415A8A9495256A48C024E083DB5F329EB85FCEDB2ABC3DA98FF1B4F6BB
            01AE75548E34EE399CC68786E28483138DB1B43246DADBCE7F709172FA35782B
            3FD0618F6D5620FF00375547A2976F8BB9C8B4F481EBAC43F8DA78CAA6DD0FE7
            6D9B5B1ED11FE2EA9E55AD77BF9DB6855C7B7C0F8BA1860B5ED1614AFF005F44
            637138CEB2B3A8F34B987D29AFC7C7889A627DB5887F174414EB1AA519692E3D
            4A4E3F5C44F8BA9F27A7F7B3D9DB6954C7B4B3F13556FF004BAFF73E85AB501E
            D535EFEA87E015CFD699FEBE8AD1884075999D479A6050A9E6104F8B3E949C7E
            B58C7FB9CD30A895785176F8955A627D95060FF72F59E99E967501072BB567FD
            CB6BDFD59C6E9E5F0CE375A7533EC2CFC4D0E7B33587F4D27AFDA85927A57EB3
            B7A8F35A1E654A97287E4EAB4E513E8131AF7B54D2A233209F0E6425FB2635EF
            695116D2BC18C6EB42AC7D9E07C4D5C45A4DD2C63759B5A3EC0C7C5D4DBD9AAC
            6FE9A4F5FB509EDA9DBA4CDEA3CD15BF6D487F3E1AA2AFD92DAF7B5512BC0B6A
            7D2233EF34E4EA955A9F1DB65858736253319714A528703FAB000CE79FB35D22
            AAE2631BACBAF1F6263FC6D74B7BA6F71DF57ED0E548A2CAA2D3E9D29B98E393
            4B7BD650A0A4A5294295E9032491C69AE1F815747531914EE198CCE838EE0AA9
            2AE00C76D48DB5B711E6BFFFD9}
          Stretch = True
          IsControl = True
        end
        object ProductName_L: TLabel
          Left = 120
          Top = 21
          Width = 68
          Height = 13
          Caption = 'Product Name'
          IsControl = True
        end
        object Version_L: TLabel
          Left = 120
          Top = 40
          Width = 47
          Height = 13
          Caption = 'Version_L'
          IsControl = True
        end
        object Copyright: TLabel
          Left = 8
          Top = 110
          Width = 161
          Height = 13
          Caption = 'Copyright: Naiveshark, 2014-2015'
          IsControl = True
        end
        object www_main_l: TLabel
          Left = 8
          Top = 126
          Width = 60
          Height = 13
          Cursor = crHandPoint
          Caption = 'www_main_l'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          OnClick = www_main_lClick
        end
        object www_agent_l: TLabel
          Left = 8
          Top = 149
          Width = 32
          Height = 13
          Cursor = crHandPoint
          Caption = 'www_l'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          OnClick = www_agent_lClick
        end
        object www_doc_l: TLabel
          Left = 8
          Top = 172
          Width = 32
          Height = 13
          Cursor = crHandPoint
          Caption = 'www_l'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          OnClick = www_doc_lClick
        end
        object New_Version_L: TLabel
          Left = 120
          Top = 59
          Width = 89
          Height = 13
          Cursor = crHandPoint
          Caption = 'New_Version_L'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          Visible = False
          WordWrap = True
          OnClick = New_Version_LClick
        end
        object Check_Status_L: TLabel
          Left = 121
          Top = 59
          Width = 94
          Height = 13
          Caption = 'Check_Status_L'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
      end
    end
    object Inforrmation_TSh: TTabSheet
      Caption = 'Inforrmation'
      ImageIndex = 1
      ExplicitTop = 23
      ExplicitWidth = 281
      ExplicitHeight = 165
      object Ini_Path_L: TLabel
        Left = 3
        Top = 3
        Width = 117
        Height = 13
        Caption = 'Path to configuration file:'
      end
      object Ini_Path_E: TEdit
        Left = 3
        Top = 22
        Width = 322
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
      end
    end
  end
end
