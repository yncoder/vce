unit U_Settings;

interface

uses U_Online_Mode;

var Debug_Mode : Boolean;

function Get_Refresh_Interval : integer;

var Online_Mode : T_Online_Mode;

implementation

(* � ������������� *)

const sec = 1000;

const Std_Refresh_Interval_if_online = 60 * sec; (* � ������ ������ *)
      Std_Refresh_Interval_if_offline = 120 * sec; (* � ������ ������ *)

const Debug_Refresh_Interval_if_online = 5 * sec; (* � ������ ������ *)
      Debug_Refresh_Interval_if_offline = 15 * sec; (* � ������ ������ *)

function Get_Refresh_Interval;
begin
  if Debug_Mode
    then
      begin
        if Online_Mode.Online_Flag
          then
            Result := Debug_Refresh_Interval_if_online
          else
            Result := Debug_Refresh_Interval_if_offline;
      end
    else
      begin
        if Online_Mode.Online_Flag
          then
            Result := Std_Refresh_Interval_if_online
          else
            Result := Std_Refresh_Interval_if_offline;
      end;
end;

initialization

  Online_Mode := T_Online_Mode.Create;

finalization

  Online_Mode.Free;

end.
