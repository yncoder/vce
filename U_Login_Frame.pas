unit U_Login_Frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TLogin_Frame = class(TFrame)
    Label1: TLabel;
    Username_E: TEdit;
    Label2: TLabel;
    Password_E: TEdit;
    Warning_L: TLabel;
    Login_B: TButton;
    Wrong_Login_L: TLabel;
    Reg_B: TButton;
    procedure Username_EChange(Sender: TObject);
    procedure Password_EChange(Sender: TObject);
    procedure Reg_BClick(Sender: TObject);
  private
    { Private declarations }
    prev_login, prev_pw : string;
    
    procedure SetLoginBtnEnable;
  public
    { Public declarations }
    procedure Set_Labels;
    procedure Switch_Wrong_Login_MSG( Arg_Show : boolean );
    procedure Save_Prev;
    procedure Clear_Login;
  end;

implementation

uses U_URLs;

{$R *.dfm}

{ TFrame1 }

procedure TLogin_Frame.Clear_Login;
begin
  Username_E.Clear;
  Password_E.Clear;
  Save_Prev;
  Set_Labels;
end;

procedure TLogin_Frame.Password_EChange(Sender: TObject);
begin
  if prev_pw <> Password_E.Text
    then
      Switch_Wrong_Login_MSG( False );

  SetLoginBtnEnable;
end;

procedure TLogin_Frame.Reg_BClick(Sender: TObject);
begin
  OpenOnlineRegPage;
end;

procedure TLogin_Frame.Save_Prev;
begin
  prev_login := Username_E.Text;
  prev_pw    := Password_E.Text;
end;

procedure TLogin_Frame.SetLoginBtnEnable;
begin
  Login_B.Enabled := ( Username_E.Text <> '' ) and ( Password_E.Text <> '' );
end;

procedure TLogin_Frame.Set_Labels;
begin
  Warning_L.Caption := 'You need to login with your site cretentials to view your notifications';
  SetLoginBtnEnable;
end;

procedure TLogin_Frame.Switch_Wrong_Login_MSG;
begin
  Wrong_Login_L.Visible := Arg_Show;
end;

procedure TLogin_Frame.Username_EChange(Sender: TObject);
begin
  if prev_login <> Username_E.Text
    then
      Switch_Wrong_Login_MSG( False );

  SetLoginBtnEnable;
end;

end.
