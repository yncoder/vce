unit U_API_Wrapper;

interface

uses
  SysUtils, Classes, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP,
  U_Net_Const, IdCookieManager
  {$IFDEF NEED_SQLITE}
  , ZConnection, DB, ZAbstractRODataset,
  ZAbstractDataset, ZDataset
  {$ENDIF }
  //,  U_API_Wrapper_Const
  ;

type
     T_Notification_Record = record
       url      : string;
       id       : integer;
       created_at : string;
       sender_user : string;
       msg_txt  : string;
       absolute_url : string;
      end;
     T_Notification_Array = array of T_Notification_Record;

type
  TAPI_Wrapper_DataModule = class(TDataModule)
    IdHTTP1: TIdHTTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }

    (* �������, ��������� ������������ *)
    //MyProjects : T_Project_Array;

    (* ����������� *)
    Notifications : T_Notification_Array;

    (* ����� *)
    fLogin : string;
    (* ������ *)
    fPassword : string;
    (* �����-������ ��������� � ���������� *)
    fLogin_PW_Saved : boolean;
    (* ��� ������������ - ��� *)
    fNickname : string;
    (* ����� ������������ *)
    fUser_EMail : string;

    (* ��� ���������� ������ �������� *)
    fMyProjectsLastHash : LongWord;

    (* ��� ���������� ������ ����������� *)
    fNotificationLastHash : LongWord;

    {$IFDEF NEED_SQLITE}
    fZConnection: TZConnection;
    WebRequest_Q: TZQuery;
    {$ENDIF}

    procedure SaveLogin2Ini;

    (* ��������� ������ web api *)
    function CheckApiVersion( out Out_Version_Ok : boolean ) : t_http_result;

    (* �������� ������ ������� *)
    function Get_User_Profile : boolean;

    {$IFDEF NEED_SQLITE}
    function ConnectDB : boolean;
    procedure DisconnectDB;
    {$ENDIF}

    procedure ParseJSON2MyProjects( arg_json_str : string );

    procedure ParseJSON2Notifications( arg_json_str : string );

    function MyProjectsCashFile : string;

    function NotificationsCashFile : string;

    {$IFDEF NEED_SQLITE}
    procedure CreateDB_Struct;
    procedure Add_Response_Record2DB( Arg_URL, Arg_Response_Text : string; Arg_ResponseCode : integer );
    {$ENDIF}

    procedure Clear_Arrays; 
  protected
  public
    { Public declarations }
    procedure SetLogin_PW( Arg_Login, Arg_Password : string );
    (* ��������. ��������� ������. *)
    function Refresh : T_Net_Call_Results;

    //function Get_My_Projects : T_Project_Array;
    function Get_Notifications : T_Notification_Array;

    function Get_Login : string;
    procedure Logout;
  end;

implementation

{$R *.dfm}

uses superobject, U_Msg, U_URLs, U_Ini, U_Const, U_Ver;

{ TAPI_Wrapper_DataModule }

{$IFDEF NEED_SQLITE}
procedure TAPI_Wrapper_DataModule.Add_Response_Record2DB;
begin
  if WebRequest_Q.Active
    then
      with WebRequest_Q do
        begin
          Append;

          FieldByName( 'DT' ).AsDateTime := now;
          FieldByName( 'Url' ).AsString := Arg_URL;
          FieldByName( 'ResponseCode' ).AsInteger := Arg_ResponseCode;
          FieldByName( 'ResponseText' ).AsString := Arg_Response_Text;

          Post;
      end;
end;
{$ENDIF}

function TAPI_Wrapper_DataModule.CheckApiVersion;
var s : string;
    obj: ISuperObject;
    site_api_version : integer;
    Version_Ok : boolean;
    response_code : integer;
begin
  Version_Ok := False;

  Result := http_res_no_connection;

  s := IdHTTP1.Get( Get_Api_Version_URL );

  response_code := Idhttp1.response.ResponseCode;
  case response_code of
    200 : begin
            obj := SO(s);
            site_api_version := obj.I['api_version'];
            Result := http_res_ok;
            
            if Web_Api_Version <> site_api_version
              then
                ShowMsg( 'Wrong API version.' + #13#10 + ' Local version is ' + IntToStr( Web_Api_Version ) + ', and site version is ' + IntToStr( site_api_version ) + '.' )
              else
                Version_Ok := True;

          end;
    401 : Result := http_res_bad_auth;
   end;
   Out_Version_Ok := Version_Ok;
end;

procedure TAPI_Wrapper_DataModule.Clear_Arrays;
begin
  //SetLength( MyProjects, 0 );
  SetLength( Notifications, 0 );
  
  fMyProjectsLastHash := 0;
  fNotificationLastHash := 0;
end;

{$IFDEF NEED_SQLITE}
function TAPI_Wrapper_DataModule.ConnectDB;
var db_name : string;
    create_db : boolean;
begin
  Result := False;
  if fZConnection.Connected
    then
      Exit;

  if fLogin <> ''
    then
      begin
        db_name := AppData_Path + fLogin + '.sqlite';

        create_db := not FileExists( db_name );
        fZConnection.Database := db_name;
        fZConnection.Connect;
        if create_db
          then
            CreateDB_Struct;
        WebRequest_Q.Open;
      end;
end;
{$ENDIF}

{$IFDEF NEED_SQLITE}
procedure TAPI_Wrapper_DataModule.CreateDB_Struct;
begin
  fZConnection.ExecuteDirect( ' CREATE TABLE WebRequest ' +
                                ' ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ' +
                                '   DT DATETIME, ' +
                                '   Url CHAR (255) NOT NULL, ' +
                                '   ResponseCode INTEGER, ' +
                                '   ResponseText TEXT );    '
                                );
end;
{$ENDIF}

procedure TAPI_Wrapper_DataModule.DataModuleCreate(Sender: TObject);
var login, password : string;
begin
  {$IFDEF NEED_SQLITE}
  fZConnection := TZConnection.Create( Self );
  fZConnection.Protocol := 'sqlite-3';
  WebRequest_Q := TZQuery.Create( Self );
  WebRequest_Q.Connection := fZConnection;
  WebRequest_Q.SQL.Text := 'select * from WebRequest';
  {$ENDIF}
  
  Clear_Arrays;

  fLogin_PW_Saved := False;
  fLogin := '';
  fPassword := '';
  fNickname := '';
  fUser_EMail := '';

  {$IFDEF NEED_SQLITE}
  ConnectDB;
  {$ENDIF}
  
  login := Ini_Strore.Str[ ii_Login ];
  if login <> ''
    then
      begin
        password := Ini_Strore.Str[ ii_PW ];
        if password <> ''
          then
            begin
              fLogin := login;
              fPassword := password;
              fLogin_PW_Saved := True;
            end;
        fNickname := Ini_Strore.Str[ ii_Nickname ];
        fUser_EMail := Ini_Strore.Str[ ii_email ];
      end;
end;

procedure TAPI_Wrapper_DataModule.DataModuleDestroy(Sender: TObject);
begin
  Clear_Arrays;

  {$IFDEF NEED_SQLITE}
  DisconnectDB;
  WebRequest_Q.Free;
  fZConnection.Free;
  {$ENDIF}
end;

{$IFDEF NEED_SQLITE}
procedure TAPI_Wrapper_DataModule.DisconnectDB;
begin
  if WebRequest_Q.Active
    then
      WebRequest_Q.Close;

  if fZConnection.Connected
    then
      fZConnection.Disconnect;
end;
{$ENDIF}

function TAPI_Wrapper_DataModule.Get_Notifications;
begin
  Result := Notifications;
end;

function TAPI_Wrapper_DataModule.Get_Login: string;
begin
  Result := fLogin;
end;

{function TAPI_Wrapper_DataModule.Get_My_Projects;
begin
  Result := MyProjects;
end;}

function TAPI_Wrapper_DataModule.Get_User_Profile;
var s : string;
    obj: ISuperObject;
begin
  s := IdHTTP1.Get( Get_My_Profile_URL );
  obj := SO(s);

  fNickname := UTF8Decode( WideString( obj.s['nickname'] ) );

  fUser_EMail := obj.s['email'];

  //ShowMsg(  fNickname  + '  ' + fUser_EMail );
end;

procedure TAPI_Wrapper_DataModule.Logout;
begin
  {$IFDEF NEED_SQLITE}
  DisconnectDB;
  {$ENDIF}
  
  fLogin    := '';
  fPassword := '';
  fNickname := '';
  fUser_EMail := '';
  fLogin_PW_Saved := False;
  SaveLogin2Ini;

  Clear_Arrays;
end;

function TAPI_Wrapper_DataModule.NotificationsCashFile: string;
begin
  if fLogin <> ''
    then
      result := AppData_Path + fLogin + '_notifications.txt';
end;

function TAPI_Wrapper_DataModule.MyProjectsCashFile: string;
begin
  if fLogin <> ''
    then
      result := AppData_Path + fLogin + '_my_projects.txt';
end;

procedure TAPI_Wrapper_DataModule.ParseJSON2Notifications;
var obj: ISuperObject;
    i : integer;
begin
  obj := SO(arg_json_str);
  SetLength( Notifications, obj.AsArray.Length );

  for i := 0 to obj.AsArray.Length - 1 do
    begin
      Notifications[i].id       := obj.AsArray[i].I['id'];
      Notifications[i].url      := obj.AsArray[i].S['url'];
      Notifications[i].created_at := UTF8Decode( WideString( obj.AsArray[i].S['created_at'] ) );
      Notifications[i].sender_user := UTF8Decode( WideString( obj.AsArray[i].S['sender_user.username'] ) );

      Notifications[i].msg_txt := UTF8Decode( WideString( obj.AsArray[i].S['msg_txt'] ) );
      Notifications[i].absolute_url := UTF8Decode( WideString( obj.AsArray[i].S['absolute_url'] ) );
    end;
end;

procedure TAPI_Wrapper_DataModule.ParseJSON2MyProjects;
var obj: ISuperObject;
    i : integer;
begin
  obj := SO(arg_json_str);
{  SetLength( MyProjects, obj.AsArray.Length );

  for i := 0 to obj.AsArray.Length - 1 do
    begin
      MyProjects[i].fullname := UTF8Decode( WideString( obj.AsArray[i].S['fullname'] ) );
      MyProjects[i].url      := obj.AsArray[i].S['url'];
      MyProjects[i].id       := obj.AsArray[i].I['id'];
    end;}
end;

function TAPI_Wrapper_DataModule.Refresh;
var s : string;
    curr_hash : longword;
    ft : textfile;
    response_code : integer;
    Version_Ok : boolean;
    http_result : t_http_result;
begin
  Result := nc_res_Fail;

  if ( fLogin <> '' ) and ( fPassword <> '' )
    then
      begin
        (* ������� �������������� *)
        if ( IdHTTP1.Request.Username <> fLogin )
            or
           ( IdHTTP1.Request.Password <> fPassword )
          then
            begin
              (* ���������, http://stackoverflow.com/a/30078000/3578861 *)
              IdHTTP1.Request.Authentication.Free;
              IdHTTP1.Request.Authentication:=nil;

              (* ����� *)
              IdHTTP1.Request.BasicAuthentication := True;
              IdHTTP1.Request.Username := fLogin;
              IdHTTP1.Request.Password := fPassword;
            end;

        try
          (* ??? ��� ��� ��������� ���.
          ��� �������� ������ ������ � ������������ ����������, �� ��� ������ ������ �� ������������? *)

          http_result := CheckApiVersion( Version_Ok );
          case http_result of
            http_res_ok : if not Version_Ok
                            then
                              begin
                                Result := nc_res_Wrong_API_Verion;
                                Exit;
                              end;
            http_res_bad_auth : begin
                                  Result := nc_res_Auth_Fail;
                                  Exit;
                                end;
            http_res_no_connection : begin
                                       Result := nc_res_Fail;
                                       Exit;
                                     end;
          end;

          //s := IdHTTP1.Get( Get_MyProjects_URL );
          s := IdHTTP1.Get( Get_Notifications_URL );

          response_code := Idhttp1.response.ResponseCode;

          (* ��������� ��� ������ *)
          case response_code of
            200:
              begin
                (* ���������, ���������� �� ��� *)
                curr_hash := FNV1aHash( s );
                //if fMyProjectsLastHash = curr_hash
                if fNotificationLastHash = curr_hash
                  then
                    begin
                      (* ������ �� ����������? *)
                      Result := nc_res_DataNoChange;
                    end
                  else
                    begin
                      //ParseJSON2MyProjects( s );
                      ParseJSON2Notifications( s );

                      //fMyProjectsLastHash := curr_hash;
                      fNotificationLastHash := curr_hash;

                      (* ������ ���� �������� *)
                      SaveLogin2Ini;

                      (* ������ ���� �������� *)
                      {$IFDEF NEED_SQLITE}
                      ConnectDB;
                      {$ENDIF}
                      
                      Result := nc_res_NewData;

                      (* ��������� ������ �������� � ��� *)
                      //Add_Response_Record2DB( Get_MyProjects_URL, s, response_code );
                      {$IFDEF NEED_SQLITE}
                      Add_Response_Record2DB( Get_Notifications_URL, s, response_code );
                      {$ENDIF}
                      
                      //AssignFile( ft, MyProjectsCashFile );
                      AssignFile( ft, NotificationsCashFile );
                      ReWrite( ft );
                      WriteLn( ft, s );
                      CloseFile( ft );

                    end;
              end;
            401 : Result := nc_res_Auth_Fail;
            else Result := nc_res_Fail;
           end;

        except
           (* fail *)
           if fLogin <> ''
             then
               begin
                 (* ����������� �������� ������ �� ���� *)
                 //if FileExists( MyProjectsCashFile )
                 if FileExists( NotificationsCashFile )
                   then
                     begin
                       //AssignFile( ft, MyProjectsCashFile );
                       AssignFile( ft, NotificationsCashFile );

                       Reset( ft );
                       Readln( ft, s );
                       CloseFile( ft );

                       //ParseJSON2MyProjects( s );
                       ParseJSON2Notifications( s );

                       //fMyProjectsLastHash := FNV1aHash( s );
                       fNotificationLastHash := FNV1aHash( s );

                       Result := nc_res_Fail_but_Cash;
                     end;                 
               end;           
        end;
      end
    else
      begin
        (* ������ ��� - ����� ����� *)
        Result := nc_res_Auth_Empty;
      end;
end;

procedure TAPI_Wrapper_DataModule.SaveLogin2Ini;
begin
  (* ���� ��� �� �������� *)
  if not fLogin_PW_Saved
    then
      begin
        if fLogin = ''
          then
            (* ������� ��� ������ *)
            Ini_Strore.Del_Section( is_Curr_user_Section )
          else
            begin
              Ini_Strore.Str[ ii_Login ] := fLogin;
              Ini_Strore.Str[ ii_PW ]    := fPassword;
              fLogin_PW_Saved := True;

              if Get_User_Profile
                then
                  begin
                    Ini_Strore.Str[ ii_Nickname ] := fNickname;
                    Ini_Strore.Str[ ii_email ]    := fUser_EMail;
                  end;
            end;
      end;
end;

procedure TAPI_Wrapper_DataModule.SetLogin_PW;
begin
  fLogin    := Arg_Login;
  fPassword := Arg_Password;
  fLogin_PW_Saved := False;
end;

end.
